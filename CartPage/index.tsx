import styles from "./styles.module.scss";
import { GetStaticProps } from "next";
import { useState } from "react";
import withHeader from "../../utils/withHeader";
import { PageTitle } from "../../component-nextv3/PageTitle";
import { THEME_SETTING, TOP_CART_PAGE_SETTING } from "../../setting/setting";
import CartProductsTable from "../../component-nextv3/CartProductsTable";
import { getDocument } from "../../core-nextv3/document/document.api";
import { CartMessage } from "../../component-nextv3/CartMessage";

const CartPage = ({ cartMessage }: any) => 
{	
    const [ loadingCart, setLoadingCart ] = useState(true);

    return (
        <>
            <div className={styles.cartPage}>
                <div className={styles.content}>
                    {cartMessage?.published === true && (<CartMessage cartMessage={cartMessage} />)}
                    <PageTitle name="Carrinho" noTitle={true} />
                    <CartProductsTable loadingCart={loadingCart} 
									   			   setLoadingCart={setLoadingCart}/>
                </div>
            </div>
        </>
    );
}

const getStaticProps: GetStaticProps = () => withHeader(async (props: any) => 
{		
    const cartMessageResult = await getDocument(TOP_CART_PAGE_SETTING)	

    return {
        revalidate : THEME_SETTING.revalidate,
        props      : {
            seo         : props.seo.merge({ title : "Carrinho" }),
            cartMessage : cartMessageResult?.data || [],
        },
    };
});

export { getStaticProps as GetStaticProps, CartPage }