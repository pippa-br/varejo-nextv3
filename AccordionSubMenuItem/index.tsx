"use client"

import styles from "./styles.module.scss";
import { useState } from "react";
import { AnimatePresence, motion } from "framer-motion";
import { ImageSet } from "@/component-nextv3/ImageSet";

export const AccordionSubMenuItem = ({ children, title, number }: any) => 
{
    const [ toggle, setToggle ] = useState(false)

    return (
        <AnimatePresence
            initial={false}
            mode='wait'
            onExitComplete={() => null}
        >
            <motion.div
                layout
                className={styles.accordionSubMenuItem}
                onClick={() => setToggle(!toggle)}
            >
                <motion.p className={styles.accordionTitle} layout>
                    {number ? <span>{number}</span> : null}
                    {title}                
                    <ImageSet
                        className={styles.accordionSvg} 
                        width={20}
                        height={20}
                        src="/assets/icons/IoIosArrowForward.svg"
                        responsive={false}
                        alt=""
                    />
                </motion.p>
                {toggle ? children : ""}
            </motion.div>
        </AnimatePresence>
    );
}