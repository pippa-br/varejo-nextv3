import styles from "./styles.module.scss";
//import { useRouter } from "next/router";
import { useState } from "react";
import { AddAddressModal } from "../AddAddressModal";
import { PageTitle } from "../../component-nextv3/PageTitle";
import Image from "next/image";
import { cloudflareLoader } from "@/core-nextv3/util/util";
import Link from "next/link";

function AddressPage({ user }: any) 
{
    const [ openModal, setOpenModal ] = useState(false);
    const [ openEditModal, setEditOpenModal ] = useState(false);
    const [ addressToEdit, setAddressToEdit ] = useState();

    //const router = useRouter();

    const logout = async () => 
    {
    // await api.post("/api/authV2/logout");
    // dispatch(getUserData());
    };

    const toggleEditModal = async (address: any) => 
    {
        setEditOpenModal(true);
        setAddressToEdit(address);
    };

    const deleteAddress = async (addressReference: any) => 
    {
        addressReference;
    // await dispatch(deleteUserAddress(addressReference));
    // toast.success("Endereço deletado com sucesso!");
    // await dispatch(getUserData());
    };

    return (
        <>
            <div className={styles.addressPage}>
                <div className={styles.content}>
                    <PageTitle name="Endereços" />
                    <div className={styles.perfilData}>
                        <div className={styles.perfilMenu}>
                            <Link href="/perfil/" prefetch={true}>Meus Pedidos</Link>
                            <Link href="/perfil/detalhes-da-conta/" prefetch={true}>Detalhes da conta</Link>
                            <Link href="/perfil/enderecos/" prefetch={true} className={styles.active}>
                Endereços
                            </Link>
                            <Link href="/perfil/cartoes/" prefetch={true}>Cartões</Link>
                            <Link href="/" prefetch={true} onClick={() => logout()}>
                Sair
                            </Link>
                        </div>
                        <div className={styles.perfilContent}>
                            {user?.address?.length >= 1 ? (
                                <div className={styles.addressGrid}>
                                    {user?.address?.map((address: any) => (
                                        <div
                                            key={address.referencePath}
                                            className={styles.addressItem}
                                        >
                                            <div className={styles.addressItemActions}>
                                                <Image
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/BiEditAlt.svg"
                                                    loader={cloudflareLoader}
                                                    alt=""
                                                    onClick={() => toggleEditModal(address)}
                                                    title="Editar endereço"
                                                />
                                                <Image
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/BsTrash.svg"
                                                    loader={cloudflareLoader}
                                                    alt=""
                                                    onClick={() => deleteAddress(address.referencePath)}
                                                    title="Deletar endereço"
                                                />
                                            </div>

                                            <span>{address.name}</span>
                                            <span>
                                                {address.address.street},{address.address.housenumber}
                                            </span>
                                            <span>{address.address.complement}</span>
                                            <span>
                                                {address.address.city}, {address.address.state}
                                            </span>
                                        </div>
                                    ))}
                                </div>
                            ) : (
                                <span className={styles.noAddress}>
                  Você não possui endereços cadastrados.
                                </span>
                            )}
                            <button
                                type="button"
                                className={styles.addAddressButton}
                                onClick={() => setOpenModal(true)}
                            >
                Cadastrar endereço
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {openModal && <AddAddressModal setOpenModal={setOpenModal} />}
            {openEditModal && (
                <AddAddressModal
                    setOpenModal={setEditOpenModal}
                    edit={true}
                    addressData={addressToEdit}
                />
            )}
        </>
    );
}

export default AddressPage;
