import React from "react";
import { ProductProvider } from "../Providers/product.providers";
import { getDocument } from "@/core-nextv3/document/document.api";
import { calls } from "@/core-nextv3/util/call.api";
import { FEATURES_PRODUCTS_SETTING, MEASUREMENT_TABLE_SETTING, TROCA_PAGE_SETTING } from "@/setting/setting";

const getProductLayoutServer = async () => 
{
    const [ exchangeResult, featuresProductResult, measurementTableResult  ] : any = await calls(
        getDocument(TROCA_PAGE_SETTING.merge({ cache : "force-cache" })),
        getDocument(FEATURES_PRODUCTS_SETTING.merge({ cache : "force-cache" })),
        getDocument(MEASUREMENT_TABLE_SETTING.merge({ cache : "force-cache" })),
    );

    return {
        exchangeData         : exchangeResult?.data || {},
        featuresProductData  : featuresProductResult?.data || {},
        measurementTableData : measurementTableResult?.data || {},
    };
}

export default async function ProductLayout({
    children,
}: Readonly<{
  children: React.ReactNode;
}>) 
{
    const { exchangeData, featuresProductData, measurementTableData } = await getProductLayoutServer();

    return (
        <ProductProvider exchangeData={exchangeData} featuresProductData={featuresProductData} measurementTableData={measurementTableData}>
            {children}
        </ProductProvider>	
    );
}