import React from "react";
import StoreServer from "../ServerComponents/store.server";

export default async function StoreLayout({
    children,
}: Readonly<{
  children: React.ReactNode;
}>) 
{
    return (
        <StoreServer>
            {children}
        </StoreServer>	
    );
}