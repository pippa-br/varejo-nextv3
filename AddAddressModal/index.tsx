import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import InputMask from "react-input-mask";
import { useForm } from "react-hook-form";
import { useState } from "react";
import { toast } from "react-hot-toast";
import { buscaCep, cloudflareLoader } from "../../core-nextv3/util/util";
import { AnimatedLoading } from "../../component-nextv3/AnimatedLoading";
import ErrorMessage from "../../component-nextv3/ErrorMessage";
import Image from "next/image";

type AddressProps = {
  bairro: string;
  cep: string;
  complemento: string;
  localidade: string;
  logradouro: string;
  uf: string;
};

export const AddAddressModal = ({
    setOpenModal,
    edit = false,
    addressData = null,
}: any) => 
{
    const [ address, setAddress ] = useState<AddressProps>();
    const [ loadingUserAddress, setLoadingUserAddress ] = useState(false);
    const router = useRouter();

    const {
        register,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm();

    const onSubmit = async (data: any) => 
    {
        data;

        toast.success("Endereço salvo com sucesso!");
        // const newData = {
        //     data : {
        //         name   : data.name,
        //         client : {
        //             referencePath : userData.referencePath,
        //         },
        //         address : {
        //             zipcode     : data.cep,
        //             street      : data.street,
        //             housenumber : data.housenumber,
        //             complement  : data.complement,
        //             district    : data.district,
        //             city        : data.city,
        //             state       : data.state,
        //             country     : { id : "br", label : "Brasil", value : "br" },
        //         },
        //     },
        // };

        await setLoadingUserAddress(true);
        // await addressPlugin.addDocumentFront(newData);
        await setLoadingUserAddress(false);
        router.reload();
    };

    const onSubmitEdit = async (data: any) => 
    {
        data;
        toast.success("Endereço alterado com sucesso!");
        // const newData = {
        //     document : {
        //         referencePath : addressData.referencePath,
        //     },
        //     data : {
        //         name   : data.name,
        //         client : {
        //             referencePath : userData.referencePath,
        //         },
        //         address : {
        //             zipcode     : data.cep,
        //             street      : data.street,
        //             housenumber : data.housenumber,
        //             complement  : data.complement,
        //             district    : data.district,
        //             city        : data.city,
        //             state       : data.state,
        //             country     : { id : "br", label : "Brasil", value : "br" },
        //         },
        //     },
        // };
        await setLoadingUserAddress(true);
        // const result = await addressPlugin.setDocumentFront(newData);
        router.reload();
    };

    return (
        <div className={styles.addAddressModal} onClick={() => setOpenModal(false)}>
            <div className={styles.content} onClick={(e) => e.stopPropagation()}>
                <Image
                    onClick={() => setOpenModal(false)}
                    width={20}
                    height={20}
                    src="/assets/icons/GrFormClose.svg"
                    loader={cloudflareLoader}
                    alt=""
                />
                <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                    <div className={styles.formItem}>
                        <label>Nome do endereço</label>
                        <input
                            defaultValue={edit ? addressData?.name : null}
                            autoComplete="new-off"
                            placeholder="Ex: Casa, Apartamento, Empresa"
                            {...register("name", { required : "Este campo é obrigatório!" })}
                        />
                        {errors.name && <ErrorMessage message={errors.name?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>CEP</label>
                        <InputMask
                            mask="99999-999"
                            placeholder="Ex: 12345-678"
                            autoComplete="new-password"
                            maskChar=""
                            defaultValue={edit ? addressData?.address?.zipcode : null}
                            onKeyUp={(e: any) =>
                                e.target.value.length === 9 &&
                buscaCep(e.target.value, setAddress, setValue)
                            }
                            {...register("cep", { required : "Este campo é obrigatório!" })}
                        />
                        {errors.cep && <ErrorMessage message={errors.cep?.message}/>}
                    </div>

                    <div className={styles.formItem}>
                        <label>Rua</label>
                        <input
                            defaultValue={
                                edit ? addressData?.address?.street : address?.logradouro
                            }
                            type="text"
                            placeholder="Ex: Rua Guaranabara"
                            autoComplete="new-password"
                            {...register("street", { required : "Este campo é obrigatório!" })}
                        />
                        {errors.street && <ErrorMessage message={errors.street?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>Número</label>
                        <input
                            type="text"
                            defaultValue={edit ? addressData?.address?.housenumber : null}
                            autoComplete="new-password"
                            placeholder="Ex: 17"
                            {...register("housenumber", {
                                required : "Este campo é obrigatório!",
                            })}
                        />
                        {errors.housenumber && <ErrorMessage message={errors.housenumber?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>Complemento</label>
                        <input
                            type="text"
                            defaultValue={edit ? addressData?.address?.complement : null}
                            autoComplete="new-password"
                            placeholder="Ex: Ap 15B"
                            {...register("complement")}
                        />
                        {errors.complement && <ErrorMessage message={errors.complement?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>Bairro</label>
                        <input
                            defaultValue={
                                edit ? addressData?.address?.district : address?.bairro
                            }
                            type="text"
                            autoComplete="new-password"
                            placeholder="Ex: Dom Bosco"
                            {...register("district", {
                                required : "Este campo é obrigatório!",
                            })}
                        />
                        {errors.district && <ErrorMessage message={errors.district?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>Cidade</label>
                        <input
                            defaultValue={
                                edit ? addressData?.address?.city : address?.localidade
                            }
                            type="text"
                            placeholder="Ex: São Paulo"
                            autoComplete="new-password"
                            {...register("city", { required : "Este campo é obrigatório!" })}
                        />
                        {errors.city && <ErrorMessage message={errors.city?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>Estado</label>
                        <input
                            defaultValue={edit ? addressData?.address?.state : address?.uf}
                            type="text"
                            autoComplete="new-password"
                            placeholder="Ex: São Paulo"
                            {...register("state", { required : "Este campo é obrigatório!" })}
                        />
                        {errors.state && <ErrorMessage message={errors.state?.message}/>}
                    </div>

                    <button
                        className={styles.submitPerfilButton}
                        type="button"
                        disabled={loadingUserAddress && true}
                        onClick={edit ? handleSubmit(onSubmitEdit) : handleSubmit(onSubmit)}
                    >
                        {loadingUserAddress ? <AnimatedLoading /> : "Salvar"}
                    </button>
                </form>
            </div>
        </div>
    );
};
