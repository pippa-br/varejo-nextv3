"use client"

import { Suspense, useLayoutEffect, useState } from "react";
import { usePathname, useSearchParams } from "next/navigation";
import { THEME_SETTING } from "../../setting/setting";
import { findVariantByValue } from "../../core-nextv3/product/product.util";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { FilterFrontCategory } from "../FiltersFrontCategory";
import { useCategoryClickAnalytics } from "@/core-nextv3/analytics/analytics.use";
import { useStoreContext } from "../Providers/store.providers";
import { useLoadProducts } from "../Hooks/useLoadProducts";
import { productsVariantViewsAnalytics } from "@/core-nextv3/analytics/analytics.api";
import { useCore } from "@/core-nextv3/core/core";
import { ProductList } from "../ProductList";
import useProductFilter from "@/core-nextv3/product/product.filter";

const ProductsClient = ({ category, where = [], orderBy = "order", asc = false, perPage = THEME_SETTING.perPage || 24 }:any) =>
{
    const searchParams               							  = useSearchParams();
    const pathname               								  = usePathname();
    const { user } 								                  = useCore();    
    const { colorVariantsData, sizeVariantsData, colorTableData, installmentRuleData } = useStoreContext();
    const orderOptions = [
        { value : "salesAmount|false",   label : "Mais Vendidos" },
        { value : orderBy + "|false",    label : "Mais Recentes" },
        { value : orderBy + "|true",     label : "Mais Antigos" },
        { value : "indexes.price|false", label : "Maior Preço" },
        { value : "indexes.price|true",  label : "Menor Preço" }
    ]
    const [ levelVariant1 ] = useState(() => 
    {
        return findVariantByValue(colorVariantsData, searchParams.get("color")) || null
    });
    const { setFilter, registerCallback, resetFilters } 		    = useProductFilter();
    const { loading, items, hasNextPage, loadProducts, totalItems } = useLoadProducts({ 
        colorVariantsData, 
        sizeVariantsData, 
        category,
        levelVariant1,
        onLoadProducts : (items:any) =>
        {
            // GTM
            tagManager4.addProducts(pathname, items, user);

            // VIEWS ANALYTICS
            productsVariantViewsAnalytics(items);
        }  
    })

    useLayoutEffect(() => 
    {
        registerCallback(loadProducts);
        resetFilters();

        const colorQuery   = searchParams.get("color");
        const sizeQuery    = searchParams.get("size");
        const orderByQuery = searchParams.get("orderBy");
        const ascQuery     = searchParams.get("asc");
        const termQuery    = searchParams.get("term");
        const pageQuery    = searchParams.get("page");

        if (colorQuery) 
        {
            const colors2 = colorVariantsData.filter((color: any) => color.value == colorQuery);

            if (colors2.length > 0)
            {
                setFilter("color",  colors2[0]);
            }
        }

        if (sizeQuery) 
        {
            const sizes2 = sizeVariantsData.filter((size: any) => size.value == sizeQuery);

            if (sizes2.length > 0)
            {
                setFilter("size", sizes2[0]);
            }				
        } 

        if (termQuery)
        {
            setFilter("term", termQuery);
        }

        setFilter("orderBy", orderByQuery || orderBy);
        setFilter("asc",     (ascQuery ? (ascQuery == "true" ? true : false) : asc));        
        setFilter("perPage", perPage);
        setFilter("where",   where);
        setFilter("page",    pageQuery ? parseInt(pageQuery) : 1);

        if (category)
        {
            tagManager4.registerEvent("view-category", "page", category?.name, 0, null);
        }

    }, [])

    // CATEGORY ANALYTICS
    useCategoryClickAnalytics(category);	

    return (
        <Suspense>

            <FilterFrontCategory
                orderOptions={orderOptions}
                colors={colorVariantsData}
                sizes={sizeVariantsData}
                orderBy={orderBy}
                loadProducts={loadProducts}
            />

            <ProductList 
                loading={loading} 
                products={items} 								 
                colorTableData={colorTableData}
                totalItems={totalItems}
                installmentRuleData={installmentRuleData}
                hasNextPage={hasNextPage} 
                loadProducts={loadProducts} />
                
        </Suspense>
    );
}

export { ProductsClient }