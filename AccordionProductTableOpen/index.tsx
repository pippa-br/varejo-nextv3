"use client"

import styles from "./styles.module.scss";
import { useState } from "react";
import { motion } from "framer-motion";
import { THEME_SETTING } from "@/setting/setting";
import { ImageSet } from "@/component-nextv3/ImageSet";

export const AccordionProductTableOpen = ({ children, title }: any) => 
{
    const [ toggle, setToggle ] = useState(true);

    return (
        <motion.div
            className={styles.accordionProductTableOpen}
        >
            <motion.p className={ !THEME_SETTING?.tabelAccordionProduct ? styles.accordionTitleBold : styles.accordionTitle } onClick={() => setToggle(!toggle)}>
                {title}
                {toggle ? (
                    <ImageSet
                        className={styles.accordionSvg}
                        width={20}
                        height={20}
                        src="/assets/icons/AiOutlineMinus.svg"
                        responsive={false}
                        alt=""
                    />
                ) : (
                    <ImageSet
                        className={styles.accordionSvg}
                        width={20}
                        height={20}
                        src="/assets/icons/AiOutlinePlus.svg"
                        responsive={false}
                        alt=""
                    />
                )}
            </motion.p>
            {toggle ? <div className={styles.content}>
                {children}
            </div> : ""}      
        </motion.div>
    );
};