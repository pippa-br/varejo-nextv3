import { useState } from "react";
import styles from "./styles.module.scss";
import Link from "next/link";
import { ImageSet } from "@/component-nextv3/ImageSet";
import { useCore } from "@/core-nextv3/core/core";

export const MenuTypeMobile = ({ item }: any) =>  
{
    const [ openSubMenu, setOpenSubMenu ] = useState(false);
    const { setIsOpenMenu } = useCore();

    const getMenu = (item: any) => 
    {
        switch (item?.type?.value) 
        {
            case "path":
                return <>{item?._children?.length > 0 ? (
                    <li>
                        <p className={(item.highlight ? styles.highlight : "") + " " + (item.bold ? styles.bold : "") + " " + (item.differentFont ? styles.differentFont : "") + " " + styles.subMenuNavigation} onClick={() => setOpenSubMenu(!openSubMenu)}>
                            {item.name}
                            <ImageSet
                                width={20} 
                                height={20}
                                src="/assets/icons/IoIosArrowForward.svg"
                                responsive={false}
                                alt=""
                            />
                        </p>
                        {openSubMenu && <div className={(styles.subMenuChildren) + " " + (item.differentFont ? styles.differentFont : "")}>
                            {item?.addSeeAll && <p className={(item.highlight ? styles.highlight : "") + " " + (item.bold ? styles.bold : "") + " " + (item.differentFont ? styles.differentFont : "") + " menu-" + item.id}>
                                <Link href={`${item.link}`} prefetch={true}
                                    onClick={() => 
                                    {
                                        setOpenSubMenu(false); setIsOpenMenu(false) 
                                    }}>
                                    Ver Todos
                                </Link>
                            </p>}
                            {item?._children?.map((child: any, index:number) => (
                                child.status && <div
                                    key={index}>
                                    {getMenu(child)}
                                </div>
                            ))}
                        </div>}                        
                    </li>
                ) : (
                    <p className={(item.highlight ? styles.highlight : "") + " " + (item.bold ? styles.bold : "") + " " + (item.differentFont ? styles.differentFont : "") + " menu-" + item.id}>
                        <Link href={`${item.link}`} prefetch={true} onClick={() => 
                        {
                            setOpenSubMenu(false); setIsOpenMenu(false) 
                        }}>
                            {item?.name}
                        </Link>
                    </p>
                )}
                </>                
            case "category":
                return <>{item?._children?.length > 0 ? (
                    <li>
                        <p className={(item.highlight ? styles.highlight : "") + " " + (item.bold ? styles.bold : "") + " " + (item.differentFont ? styles.differentFont : "") + " menu-" + item.id}>
                            <a className={styles.subMenuNavigation} onClick={() => setOpenSubMenu(!openSubMenu)}>
                                {item.name}
                                <ImageSet
                                    width={20}
                                    height={20}
                                    src="/assets/icons/IoIosArrowForward.svg"
                                    responsive={false}
                                    alt=""
                                />
                            </a>
                        </p>
                        {openSubMenu && <div className={styles.subMenuChildren}>
                            {item?.addSeeAll && <p className={(item.highlight ? styles.highlight : "") + " " + (item.bold ? styles.bold : "") + " " + (item.differentFont ? styles.differentFont : "") + " menu-" + item.id}>
                                <Link href={`/categoria/${item?.category?.slug}/`} prefetch={true}
                                    onClick={() => 
                                    {
                                        setOpenSubMenu(false); setIsOpenMenu(false)
                                    }}>
                                    Ver Todos
                                </Link>
                            </p>}
                            {item?._children?.map((child: any, index:number) => (
                                child.status && <div
                                    key={index}>
                                    {getMenu(child)}
                                </div>
                            ))}                           
                        </div>}
                    </li>
                ) : (
                    <p className={(item.highlight ? styles.highlight : "") + " " + (item.bold ? styles.bold : "") + " " + (item.differentFont ? styles.differentFont : "") + " menu-" + item.id}>
                        <Link href={`/categoria/${item?.category?.slug}/`} prefetch={true}
                            onClick={() => 
                            {
                                setOpenSubMenu(false); setIsOpenMenu(false) 
                            }}>
                            {item?.name}
                        </Link>
                    </p>
                )}
                </>
            case "collection":
                return <>{item?._children?.length > 0 ? (
                    <li>
                        <p className={(item.highlight ? styles.highlight : "") + " " + (item.bold ? styles.bold : "") + " " + (item.differentFont ? styles.differentFont : "") + " " + styles.subMenuNavigation} onClick={() => setOpenSubMenu(!openSubMenu)}>
                            {item.name}
                            <ImageSet
                                width={20}
                                height={20}
                                src="/assets/icons/IoIosArrowForward.svg"
                                responsive={false}
                                alt=""
                            />
                        </p>
                        {openSubMenu && <div className={(styles.subMenuChildren) + " " + (item.differentFont ? styles.differentFont : "")}>
                            {item?.addSeeAll && <p className={(item.highlight ? styles.highlight : "") + " " + (item.bold ? styles.bold : "") + " " + (item.differentFont ? styles.differentFont : "") + " menu-" + item.id}>
                                <Link href={`/colecao/${item?.collection?.slug}/`} prefetch={true}
                                    onClick={() => 
                                    {
                                        setOpenSubMenu(false); setIsOpenMenu(false); 
                                    }}>
                                    Ver Todos
                                </Link>
                            </p>}
                            {item?._children?.map((child: any, index:number) => (
                                child.status && <div
                                    key={index}>
                                    {getMenu(child)}
                                </div>
                            ))}
                        </div>}                        
                    </li>
                ) : (
                    <p className={(item.highlight ? styles.highlight : "") + " " + (item.bold ? styles.bold : "") + " " + (item.differentFont ? styles.differentFont : "") + " menu-" + item.id}>
                        <Link href={`/colecao/${item?.collection?.slug}/`} prefetch={true}
                            onClick={() => 
                            {
                                setOpenSubMenu(false); setIsOpenMenu(false); 
                            }}>
                            {item?.name}
                        </Link>
                    </p>
                )}
                </>
            default:
                return null; 
        }
    }   

    return (
        <div className={styles.menuTypeMobile}>
            {item.status && getMenu(item)}            
        </div>
    );
}