import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import InputMask from "react-input-mask";
import { useForm } from "react-hook-form";
import { useState } from "react";
import { toast } from "react-hot-toast";
import ccValid from "card-validator";
import { validateCpf } from "../../core-nextv3/util/validate";
import { AnimatedLoading } from "../../component-nextv3/AnimatedLoading";
import ErrorMessage from "../../component-nextv3/ErrorMessage";
import { buscaCep, cloudflareLoader } from "../../core-nextv3/util/util";
import Image from "next/image";

type AddressProps = {
  bairro: string;
  cep: string;
  complemento: string;
  localidade: string;
  logradouro: string;
  uf: string;
};

// type CreditCardProps = {
//   cardnumber: string;
//   owner: string;
//   cvv: string;
//   expirydate: string;
//   _cpf: string;
//   address: {
//     city: string;
//     complement: string;
//     district: string;
//     housenumber: string;
//     state: string;
//     street: string;
//     zipcode: string;
//   };
//   referencePath?: string;
// };

export const AddCreditCardModal = ({ setOpenModal }: any) => 
{
    const [ address, setAddress ] = useState<AddressProps>();
    const [ loadingUserCreditCard, setLoadingUserCreditCard ] = useState(false);

    const router = useRouter();

    const {
        register,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm();

    const onSubmit = async (data: any) => 
    {
        data;
        
        // const newData = {
        //     data : {
        //         client : {
        //             referencePath : userData.referencePath,
        //         },
        //         creditCard : {
        //             cardnumber : data.cardnumber,
        //             owner      : data.owner,
        //             cvv        : data.cvv,
        //             expirydate : data.expirydate,
        //             docType    : {
        //                 value : "cpf",
        //                 label : "CPF",
        //                 id    : "cpf",
        //                 type  : "individual",
        //             },
        //             _cpf    : data.cpf,
        //             address : {
        //                 zipcode     : data.cep,
        //                 street      : data.street,
        //                 housenumber : data.housenumber,
        //                 complement  : data.complement,
        //                 district    : data.district,
        //                 city        : data.city,
        //                 state       : data.state,
        //                 country     : { id : "br", label : "Brasil", value : "br" },
        //             },
        //         },
        //     },
        // };

        await setLoadingUserCreditCard(true);
        // await creditCardPlugin.addDocumentFront(newData);
        router.reload();
        toast.success("Cartão salvo com sucesso!");
    };

    return (
        <div className={styles.addAddressModal} onClick={() => setOpenModal(false)}>
            <div className={styles.content} onClick={(e) => e.stopPropagation()}>
                <Image
                    onClick={() => setOpenModal(false)}
                    width={20}
                    height={20}
                    src="/assets/icons/GrFormClose.svg"
                    loader={cloudflareLoader}
                    alt=""
                />
                <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                    <div className={styles.formItem}>
                        <label>Nome do titular do cartão</label>
                        <input
                            type="text"
                            autoComplete="new-off"
                            placeholder="Ex: José da Silva"
                            {...register("owner", { required : "Este campo é obrigatório!" })}
                        />
                        {errors.owner && <ErrorMessage message={errors.owner?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>CPF</label>
                        <InputMask
                            {...register("cpf", {
                                validate : (value) => validateCpf(value) || "CPF inválido!",
                            })}
                            autoComplete="new-off"
                            mask="999.999.999-99"
                            placeholder="Ex: 123.456.789-10"
                            maskChar=""
                        />
                        {errors.cpf && <ErrorMessage message={errors.cpf?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>Número do cartão</label>
                        <InputMask
                            mask="9999999999??????"
                            // formatChars={{
                            //     "9" : "[0-9]",
                            //     "?" : "[0-9]",
                            // }}
                            autoComplete="new-off"
                            maskChar=""
                            placeholder="Ex: 1234567809101112"
                            {...register("cardnumber", {
                                validate : (value) =>
                                    ccValid.number(value.toString()).isValid === true ||
                  "Insira um número de cartão válido!",
                            })}
                        />
                        {errors.cardnumber && <ErrorMessage message={errors.cardnumber?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>Data de expiração</label>
                        <InputMask
                            mask="99/99"
                            placeholder="Ex: 03/24"
                            autoComplete="new-off"
                            maskChar=""
                            {...register("expirydate", {
                                validate : (value) =>
                                    value.length >= 5 ||
                  "Sua data de expiração deve ser do tipo DD/AA",
                            })}
                        />
                        {errors.expirydate && <ErrorMessage message={errors.expirydate?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>CVV</label>
                        <InputMask
                            mask="999?"
                            placeholder="Ex: 123"
                            // formatChars={{
                            //     "9" : "[0-9]",
                            //     "?" : "[0-9]",
                            // }}
                            autoComplete="new-off"
                            maskChar=""
                            {...register("cvv", {
                                validate : (value) =>
                                    value.length >= 3 ||
                  "Seu CVV tem que possuir no mínimo 3 números",
                            })}
                        />
                        {errors.cvv && <ErrorMessage message={errors.cvv?.message}/>}
                    </div>
                    <p className={styles.formItem}>Endereço de cobrança</p>
                    <div className={styles.formItem}>
                        <label>CEP</label>
                        <InputMask
                            mask="99999-999"
                            placeholder="Ex: 12345-678"
                            autoComplete="new-off"
                            maskChar=""
                            onKeyUp={(e: any) =>
                                e.target.value.length === 9 &&
                buscaCep(e.target.value, setAddress, setValue)
                            }
                            {...register("cep", {
                                validate : (value) =>
                                    value.length >= 9 || "Seu CEP deve possuir 8 números",
                            })}
                        />
                        {errors.cep && <ErrorMessage message={errors.cep?.message}/>}
                    </div>

                    <div className={styles.formItem}>
                        <label>Rua</label>
                        <input
                            defaultValue={address?.logradouro}
                            type="text"
                            placeholder="Ex: Rua Guaranabara"
                            autoComplete="new-off"
                            {...register("street", { required : "Este campo é obrigatório!" })}
                        />
                        {errors.street && <ErrorMessage message={errors.street?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>Número</label>
                        <input
                            type="text"
                            autoComplete="new-off"
                            placeholder="Ex: 17"
                            {...register("housenumber", {
                                required : "Este campo é obrigatório!",
                            })}
                        />
                        {errors.housenumber && <ErrorMessage message={errors.housenumber?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>Complemento</label>
                        <input
                            type="text"
                            autoComplete="new-off"
                            placeholder="Ex: Ap 15B"
                            {...register("complement", {})}
                        />
                        {errors.complement && <ErrorMessage message={errors.complement?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>Bairro</label>
                        <input
                            defaultValue={address?.bairro}
                            type="text"
                            autoComplete="new-off"
                            placeholder="Ex: Dom Bosco"
                            {...register("district", {
                                required : "Este campo é obrigatório!",
                            })}
                        />
                        {errors.district && <ErrorMessage message={errors.district?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>Cidade</label>
                        <input
                            defaultValue={address?.localidade}
                            type="text"
                            placeholder="Ex: São Paulo"
                            autoComplete="new-off"
                            {...register("city", { required : "Este campo é obrigatório!" })}
                        />
                        {errors.city && <ErrorMessage message={errors.city?.message}/>}
                    </div>
                    <div className={styles.formItem}>
                        <label>Estado</label>
                        <input
                            defaultValue={address?.uf}
                            type="text"
                            placeholder="Ex: São Paulo"
                            autoComplete="new-off"
                            {...register("state", { required : "Este campo é obrigatório!" })}
                        />
                        {errors.state && <ErrorMessage message={errors.state?.message}/>}
                    </div>

                    <button
                        className={styles.submitPerfilButton}
                        type="button"
                        disabled={loadingUserCreditCard && true}
                        onClick={handleSubmit(onSubmit)}
                    >
                        {loadingUserCreditCard ? <AnimatedLoading /> : "Salvar"}
                    </button>
                </form>
            </div>
        </div>
    );
};
