import styles from "./styles.module.scss";
import { THEME_SETTING } from "@/setting/setting";
import { ProductItemSkeleton } from "../ProductItemSkeleton";
import { ProductItem } from "../ProductItem";
import useInfiniteScroll from "react-infinite-scroll-hook";
import useProductFilter from "@/core-nextv3/product/product.filter";

export const ProductInfiniteScroll = ({
    products,
    loading, 
    hasNextPage, 
    colorTableData, 
    installmentRuleData
}:any) => 
{
    const { filter, setFilter, reloadFilter } = useProductFilter();

    const [ infiniteRef ] = useInfiniteScroll({
        loading     : loading,
        hasNextPage : hasNextPage,
        onLoadMore  : () => 
        {
            onNextPage();
        },
        disabled   : loading || !hasNextPage,
        rootMargin : "0px 0px 400px 0px",
    });

    const onNextPage = async () =>
    {
        setFilter("merge", true);
        await reloadFilter();

        setFilter("page", filter.page + 1);
    }

    return (
        <div className={styles.productInfiniteScroll}>
            
            <div className={styles.productList}>
                {products && products.length > 0 ? (
                    products.map((product:any, index:number) => (
                        <ProductItem key={index} product={product} colorTable={colorTableData} installmentRuleData={installmentRuleData} />
                    ))
                ) : (
                    filter.page > 0 && !loading && (
                        <p className={styles.noProducts}>Não foram encontrados produtos.</p>
                    )
                )}

                {loading && (
                    Array.from({ length : 24 }).map((_, index) => (
                        <ProductItemSkeleton key={index} width={THEME_SETTING.widthProductThumb} height={THEME_SETTING.heightProductThumb} />
                    ))
                )}

            </div>            

            {!loading && hasNextPage && (
                <div className={styles.loadingWarning} ref={infiniteRef}>
                    <button type="button" onClick={onNextPage}>Carregar mais produtos</button>
                </div>
            )}

            {loading && !hasNextPage && (
                <div className={styles.loadingWarning}>
                    <span className="noMoreProductsToLoad">Todos os produtos foram carregados</span>
                </div>
            )}
            
        </div>
    );
};
