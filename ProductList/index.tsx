import { THEME_SETTING } from "@/setting/setting";
import { ProductInfiniteScroll } from "./products.infinite.scroll";
import { ProductPagination } from "./products.pagination";

export const ProductList = ({
    products,
    loading, 
    hasNextPage, 
    colorTableData, 
    totalItems, 
    installmentRuleData
}:any) => 
{
    return (
        THEME_SETTING.disabledInfiniteScroll ? 
            (
                <ProductPagination
                    products={products}
                    loading={loading}
                    hasNextPage={hasNextPage}
                    colorTableData={colorTableData}
                    totalItems={totalItems}
                    installmentRuleData={installmentRuleData}/>
            )
            :
            (
                <ProductInfiniteScroll
                    products={products}
                    loading={loading}
                    hasNextPage={hasNextPage}
                    colorTableData={colorTableData}
                    totalItems={totalItems}
                    installmentRuleData={installmentRuleData}/>                
            )
    );
};
