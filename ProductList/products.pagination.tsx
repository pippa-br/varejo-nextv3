import styles from "./styles.module.scss";
import { THEME_SETTING } from "@/setting/setting";
import { ProductItemSkeleton } from "../ProductItemSkeleton";
import { PaginationNumber } from "@/component-nextv3/PaginationNumber";
import { ProductItem } from "../ProductItem";
import useProductFilter from "@/core-nextv3/product/product.filter";
import { useEffect } from "react";

export const ProductPagination = ({
    products,
    loading, 
    hasNextPage, 
    colorTableData, 
    totalItems, 
    installmentRuleData
}:any) => 
{
    const { filter, setFilter, reloadFilter } = useProductFilter();

    useEffect(() => 
    {
        reloadFilter()
        
    }, [])

    const onPaging = (page:number) => 
    {
        setFilter("page", page);
        reloadFilter()
    }

    return (
        <div className={styles.productInfiniteScroll}>
            <div className={styles.productList}>
                {products && products.length > 0 ? (
                    products.map((product:any, index:number) => (
                        <ProductItem key={`${product.code}-${index}`} product={product} colorTable={colorTableData} installmentRuleData={installmentRuleData} />
                    ))
                ) : (
                    filter.page > 0 && !loading && (
                        <p className={styles.noProducts}>Não foram encontrados produtos.</p>
                    )
                )}

                {loading && (
                    Array.from({ length : 24 }).map((_, index) => (
                        <ProductItemSkeleton key={index} width={THEME_SETTING.widthProductThumb} height={THEME_SETTING.heightProductThumb} />
                    ))
                )}

            </div>            

            {loading && !hasNextPage && (
                <div className={styles.loadingWarning}>
                    <span className="noMoreProductsToLoad">Todos os produtos foram carregados</span>
                </div>
            )}

            {THEME_SETTING.disabledInfiniteScroll && (
                <div className={styles.pagination}>
                    <PaginationNumber
                        perPage={THEME_SETTING.perPage || 24}
                        total={totalItems}
                        page={filter.page}
                        onPaging={onPaging}
                        infoPages={true}
                        gotoPages={true}
                        maxVisiblePages={7}
                    />
                </div>
            )}
        </div>
    );
};
