import styles from "./styles.module.scss";
import Select from "react-select";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { productVariantByValue, sortArray } from "../../core-nextv3/product/product.util";
import { customSelectStyles } from "../../core-nextv3/util/util.style";

export const FiltersFront = ({ allProducts, updateFilters }: any) => 
{
    const [ defaultCategoryFilter, setDefaultCategoryFilter ] = useState<any>();
    const [ defaultColorFilter,    setDefaultColorFilter ]    = useState<any>();
    const [ defaultSizeFilter,     setDefaultSizeFilter ]     = useState<any>();
    const [ defaultOrderFilter,    setDefaultOrderFilter ]    = useState<any>();
    const [ categories,    		  setCategories ]    	    = useState<any>([]);
    const [ colors,    		  	  setColors ]    	    	= useState<any>([]);
    const [ sizes,    		      setSizes ]    	    		= useState<any>([]);
    const { query, push, isReady }  						= useRouter();		

    const orderOptions = [
	  { value : "indexes.price|false", label : "Maior Preço" },
	  { value : "indexes.price|true",  label : "Menor Preço" }
    ];

    const onCategory = (event:any) =>
    {
        if (event) 
        {
            push({ query : { ...query, category : event.label } }, undefined, { scroll : false });			
        } 
        else 
        {
            delete query.category;
            push({ query : { ...query } }, undefined, { scroll : false });
        }		
    }

    const onColor = (event:any) =>
    {
        if (event) 
        {
            push({ query : { ...query, color : event.value } }, undefined, { scroll : false });			
        } 
        else 
        {
            delete query.color;
            push({ query : { ...query } }, undefined, { scroll : false });		
        }		
    }

    const onSize = (event:any) =>
    {
        if (event) 
        {
            push({ query : { ...query, size : event.value } }, undefined, { scroll : false });
        } 
        else 
        {
            delete query.size;
            push({ query : { ...query } }, undefined, { scroll : false });		
        }		
    }

    const onOrderBy = (event:any) =>
    {
        if (event) 
        {
            const values  = event.value.split("|");
            push({ query : { ...query, orderBy : values[0], asc : values[1] } });
        } 
        else 
        {
            delete query.orderBy;
            delete query.asc;
            push({ query : { ...query } }, undefined, { scroll : false });		
        }		
    }

    useEffect(() => 
    {
        if (isReady)
        {
            const categoriesMap : any = {};
            const colorsMap     : any = {};
            const sizesMap      : any = {};
            const items         : any = getItems();

            // CATEGORIAS DE TODOS OS ITENS
            if (allProducts)
            {
                for (const product of allProducts)
                {
                    if (product.categories)
                    {
                        for (const item of product.categories)
                        {
                            categoriesMap[item.id] = {
                                id    : item.id,
                                label : item.name,
                                value : item.id,
                            };
                        }	
                    }
                }	
            }	
					
            // VARIANTES DO PRODUTOS DA CATEGORIA
            for (const product of items)
            {
                if (product.skuVariant && product.skuVariant.length == 2)
                {
                    for (const item of product.skuVariant[0].items)
                    {
                        colorsMap[item.id] = item;
                    }
	
                    for (const item of product.skuVariant[1].items)
                    {
                        sizesMap[item.id] = item;
                    }
                }
            }
	
            const categories = [];
            const colors     = [];
            const sizes      = [];
	
            for (const key in categoriesMap)
            {
                categories.push(categoriesMap[key]);
            }
	
            for (const key in colorsMap)
            {
                colors.push(colorsMap[key]);
            }
	
            for (const key in sizesMap)
            {
                sizes.push(sizesMap[key]);
            }
	
            setCategories(categories);
            setColors(colors);
            setSizes(sizes);
			
            // DEFAULT CATEGORY
            if (query.category) 
            {
                setDefaultCategoryFilter(				
                    categories.filter((item: any) => item.label == query.category)
                );	
            } 
            else
            {
                setDefaultCategoryFilter(null);
            }	

            // COLOR
            if (query.color) 
            {
                setDefaultColorFilter(
                    colors.filter((color: any) => color.value == query.color)
                );
            } 
            else 
            {
                setDefaultColorFilter(null);
            }

            // SIZE
            if (query.size)
            {
                setDefaultSizeFilter(
                    sizes.filter((size: any) => size.value == query.size)
                );
            } 
            else 
            {
                setDefaultSizeFilter(null);
            }

            // ORDER BY
            if (query.orderBy && query.asc !== undefined) 
            {
                const value  = query.orderBy + "|" + query.asc;
                setDefaultOrderFilter(orderOptions.filter((order: any) => order.value == value));
            }
            else
            {
                setDefaultOrderFilter(null);
            }

            updateFilters(items);
        }		

    }, [ isReady, allProducts ])

    const getItems = () =>
    {
        let items = [];

        if (allProducts)
        {
            items = allProducts.filter((item:any) => 
            {
                return item;
            });
        }

        // CATEGORY
        if (query.category) 
        {
            items = items.filter((item:any) => 
            {
                for (const item2 of item.categories)
                {	
                    if (item2.name == query.category)
                    {
                        return item;
                    }
                }					
            });			
        }

        // COLOR
        if (query.color) 
        {
            items = items.filter((item:any) => 
            {
                return productVariantByValue(item, query.color, "skuVariant");
            });			
        } 

        // SIZE
        if (query.size)
        {
            items = items.filter((item:any) => 
            {
                return productVariantByValue(item, query.size, "skuVariant");
            });
        } 

        // ORDER BY
        if (query.orderBy && query.asc !== undefined) 
        {
            items = sortArray(items, "realPrice", query.asc == "true");
        }

        return items;
    }

    return (
        <div className={styles.container}>
            <Select
                placeholder="Categoria"
                options={sortArray(categories)}
                styles={customSelectStyles}
                isClearable
                isSearchable={false}
                value={defaultCategoryFilter || null}
                onChange={(e:any) => onCategory(e)}
            />
            <Select
                placeholder="Cores"
                options={sortArray(colors)}
                styles={customSelectStyles}
                isClearable
                isSearchable={false}
                value={defaultColorFilter || null}
                onChange={(e:any) => onColor(e)}
            />
            <Select
                placeholder="Tamanhos"
                options={sortArray(sizes)}
                styles={customSelectStyles}
                isClearable
                isSearchable={false}
                value={defaultSizeFilter || null}
                onChange={(e:any) => onSize(e)}
            />
            <Select
                placeholder="Ordenar Por"
                options={orderOptions}
                styles={customSelectStyles}
                isClearable
                isSearchable={false}
                value={defaultOrderFilter || null}
                onChange={(e:any) => onOrderBy(e)}
            />
            {/* <Select
				placeholder="Tipo"
				options={typeOptions}
				styles={customSelectStyles}
				isClearable
			/>
			<Select
				placeholder="Preço"
				options={priceOptions}
				styles={customSelectStyles}
				isClearable
			/> */}
        </div>
    );
};
