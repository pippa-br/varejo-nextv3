import { calls } from "@/core-nextv3/util/call.api";
import { ACCOUNT_SETTING, GATEWAY_SETTING, TOP_CART_PAGE_SETTING } from "@/setting/setting";
import { getDocument } from "@/core-nextv3/document/document.api";
import { getAccount } from "@/core-nextv3/account/account.api";
import { CheckoutForm } from "@/component-nextv3/CheckoutForm";
import { Suspense } from "react";
import { CartEvent } from "@/component-nextv3/AnalyticsEvents/cart.event";

const getCheckoutPageServer = async () => 
{
    const [ accountResult, installmentRuleResult, cartPageResult ] = await calls(
        getAccount(ACCOUNT_SETTING.merge({ cache : "force-cache" })), 
        getDocument(GATEWAY_SETTING.merge({ cache : "force-cache" })),
        getDocument(TOP_CART_PAGE_SETTING.merge({ cache : "force-cache" }),
        ));

    return {
        accountData         : accountResult?.data || {},
        installmentRuleData : installmentRuleResult?.data?.installmentRule || {},        
        cartPageData        : cartPageResult?.data || {}    
    };
}

const GenerateMetadata = async () =>
{
    return {
        title : "Checkout"
    }
}

const CheckoutPage = async () => 
{
    const { accountData, installmentRuleData, cartPageData } = await getCheckoutPageServer();

    return (
        <Suspense>
            <CheckoutForm
                typeDoc="cpf"
                redirectUrl="/pedido/"
                accountData={accountData}
                installmentRuleData={installmentRuleData}
                cartPageData={cartPageData}
            />

            <CartEvent/>
        </Suspense>
    );
}

export { 
    CheckoutPage,
    GenerateMetadata
}