"use client"

import styles from "./styles.module.scss";
import { PageTitle } from "../../component-nextv3/PageTitle";
import { THEME_SETTING } from "../../setting/setting";
import { useEffect } from "react";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { productsVariantViewsAnalytics } from "../../core-nextv3/analytics/analytics.api";
import { useResponsive } from "../../core-nextv3/util/useResponsive";
import { usePathname } from "next/navigation";
import { useCore } from "../../core-nextv3/core/core";
import { useCollectionClickAnalytics } from "@/core-nextv3/analytics/analytics.use";
import { useStoreContext } from "../Providers/store.providers";
import { useLoadProductsByCollection } from "../Hooks/useLoadProductsByCollection";
import { ProductList } from "../ProductList";
import useProductFilter from "@/core-nextv3/product/product.filter";

const CollectionDisplay = ({ collectionData }:any) =>
{
    const perPage                                 = THEME_SETTING.perPage || 24;
    const pathname 	 							  = usePathname(); 
    const { user } 				 				  = useCore();
    const { colorTableData, installmentRuleData } = useStoreContext();
    const { isDesktop }   					 	  = useResponsive();
    const { setFilter, registerCallback, resetFilters } 					        = useProductFilter();
    const { loading, items, hasNextPage, loadProducts, totalItems } = useLoadProductsByCollection({ 
        collectionData, 
        onLoadProducts : (items:any) =>
        {
            // GTM
            tagManager4.addProducts(pathname, items, user);

            // VIEWS ANALYTICS
            productsVariantViewsAnalytics(items);
        }  
    })

    useEffect(() => 
    {        
        registerCallback(loadProducts);
        resetFilters();

        setFilter("perPage", perPage);
        setFilter("page",    1);

        if (collectionData)
        {
            tagManager4.registerEvent("view-collection", "page", collectionData?.name, 0, null);
        }
    }, [])    

    // COLLECAO ANALYTICS
    useCollectionClickAnalytics(collectionData)

    return (
        <>
            <div className={styles.contentPage}>
                <div className={styles.pageBanner}>
                    {isDesktop && collectionData?.image && <img 
                        className={styles.desktop}
                        src={collectionData?.image?._url}
                        alt=""
                    />}
                    {!isDesktop && !THEME_SETTING.disabledFotoCategoryMobile && collectionData?.mobile && <img
                        className={styles.mobile}
                        src={collectionData?.mobile?._url}
                        alt=""
                    />}
                </div>

                <div className={styles.content}>
					
                    {/* <PageTitle name={page.name}/>
					<FiltersFront colors={colors} sizes={sizes} categories={categories} allProducts={allProducts} setProducts={setProducts}/>		

					{products?.length > 0 && <>	
						<div className={styles.products}>
							{products?.map((product: any) => (
								(product.published && product.slug && 
								<ProductItem key={product.id} 
											 product={product} 
											 colorTable={colorTable} 
											 labelImageType={false}
											 disabledQuery={true}/>)
							))}
						</div>
					</>}

					{products?.length == 0 && <p className={styles.noProducts}>Não foram encontrados produtos.</p>} */}

                    <PageTitle name={collectionData?.name} noTitle={true}/>

                    {/* <FiltersFront allProducts={page?.productsVariant} 
								  updateFilters={updateFilters}/>	 */}

                    {/* <FilterFrontCollection
                        updateFilters={updateFilters}
                        allProducts={collectionData?.productsVariant} 
                    /> */}

                    <ProductList 
                        loading={loading} 
                        products={items} 								 
                        colorTableData={colorTableData}
                        totalItems={totalItems}
                        installmentRuleData={installmentRuleData}
                        hasNextPage={hasNextPage} 
                        loadProducts={loadProducts} />

                </div>
            </div>
        </>		
    );
}

export { CollectionDisplay }