import { Suspense } from "react";
import { collectionDocument } from "../../core-nextv3/document/document.api";
import { COLLECTION_SETTING, SLUG_COLLECTION_SETTING } from "../../setting/setting";
import { CollectionDisplay } from "./collection.display";
import { getProduct } from "@/core-nextv3/product/product.api";

const GenerateStaticParams = async () =>
{
    const pages = await collectionDocument(SLUG_COLLECTION_SETTING.merge({
        perPage : 100,
        where   : [ {
            field  	 : "published",
            operator : "==",
            value  	 : true,
        } ]
    }));

    pages.collection = pages.collection ? pages.collection : [];
    const paths = pages.collection.map((item:any) => ({ slug : item.slug }));

    console.info("(Collection Static:", paths.length, ")");
  
    return paths
}

const GenerateMetadata = async ({ params }:any) =>
{
    const { slug } = await params;

    const { collectionData } = await getCollectionServer(slug);
 
    return {
        title : collectionData.name,        
    }
}

const getCollectionServer = async (slug:string) => 
{
    const result = await getProduct(COLLECTION_SETTING.merge({
        slug : slug
    }))

    return {
        collectionData : result.data
    };
}

const CollectionPage = async ({ params }:any) =>
{
    const { slug } = await params;

    const { collectionData } = await getCollectionServer(slug);

    return (
        <Suspense>
            <CollectionDisplay collectionData={collectionData} />
        </Suspense>
    );
}

export { 
    CollectionPage,
    GenerateStaticParams,
    GenerateMetadata
}