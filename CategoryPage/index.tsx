import styles from "./styles.module.scss";
import { PageTitle } from "../../component-nextv3/PageTitle";
import { collectionDocument, getDocument } from "../../core-nextv3/document/document.api";
import { CATEGORIES_BANNERS_PAGE_SETTING, CATEGORY_SETTING, SLUG_CATEGORY_SETTING, THEME_SETTING } from "../../setting/setting";
import { BannerTopPage } from "../BannerTopPage";
import { ProductsClient } from "../ProductsPage/products.client";
import { Suspense } from "react";
import { CategoryEvent } from "@/component-nextv3/AnalyticsEvents/category.event";
import { calls } from "@/core-nextv3/util/call.api";

const GenerateStaticParams = async () =>
{
    const categories = await collectionDocument(SLUG_CATEGORY_SETTING.merge({
        perPage : 100,
        where   : [ {
            field  	 : "published",
            operator : "==",
            value  	 : true,
        } ]
    }));
		
    categories.collection = categories.collection ? categories.collection : []
    const paths = categories.collection.map((item:any) => ({ slug : [ item.slug ] })); 

    console.info("(Category Static:", paths.length, ")"); 

    return paths;
}

const getCategoryData = async (slug:string) => 
{
    const [ categoryResult, pageResult ] = await calls(
        getDocument(CATEGORY_SETTING.merge({
            slug : slug
        })), 
        getDocument(CATEGORIES_BANNERS_PAGE_SETTING.merge({
            slug : slug
        })),
    )

    return {
        categoryData   : categoryResult.data,
        bannersTopPage : pageResult.data,
    };
}

const GenerateMetadata = async ({ params }:any) =>
{
    const { slug } = await params;

    const slug2 : any = slug || [];
    const slug3 = slug2.length > 0 ? slug[0] : "";

    const { categoryData } : any = await getCategoryData(slug3);
 
    return {
        title : categoryData.name,        
    }
}

const CategoryPage = async ({ params, where = [], orderBy = "categoryOrder", asc = false, perPage = THEME_SETTING.perPage || 24 }: any) =>
{
    const { slug } = await params;

    const slug2 : any = slug || [];
    const slug3 = slug2.length > 0 ? slug[0] : "";

    const { categoryData, bannersTopPage } : any = await getCategoryData(slug3);

    return (
        <Suspense>
            <div className={styles.container + " page"}>
                <BannerTopPage category={categoryData} page={bannersTopPage}/>				
                <div className={styles.content}>					
                    <PageTitle name={categoryData?.name} noTitle={true}/>					 
                    <ProductsClient 
                        category={categoryData}
                        where={where}
                        orderBy={orderBy}
                        asc={asc}
                        perPage={perPage}/>
                </div>
            </div>

            <CategoryEvent categoryData={categoryData}/>

        </Suspense>
    );
}

export { 
    CategoryPage,
    GenerateStaticParams,
    GenerateMetadata
}