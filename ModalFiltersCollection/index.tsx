import styles from "./styles.module.scss";
import Select from "react-select";
import { motion } from "framer-motion";
import { useRouter } from "next/router";
import { customSelectStyles } from "../../core-nextv3/util/util.style";
import { sortArray } from "../../core-nextv3/product/product.util";
import { modalBagAnimation } from "@/component-nextv3/animation";
import Image from "next/image";
import { cloudflareLoader } from "@/core-nextv3/util/util";

export const ModalFiltersCollection = ({ closeModal, categories, colors, sizes, size, setSize, color, setColor, category, setCategory }: any) => 
{
    const { query, push } = useRouter();	

    function filter() 
    {
        delete query.color;
        delete query.size;
        delete query.category;

        const newQuery = { ...query }

        if (color) 
        {
            newQuery.color = color.value;
        } 
		
        if (size) 
        {
            newQuery.size = size.value;
        } 

        if (category) 
        {
            newQuery.category = category.label;
        }

        push({ query : newQuery }, undefined, { scroll : false });	
        
        closeModal(false);
    }

    function cleanFilter() 
    {
        delete query.color;
        delete query.size;
        delete query.category;

        push({ query : { ...query } }, undefined, { scroll : false });		

        closeModal(false);
    }

    return (
        <motion.div
            className={styles.modalFiltersCollection}
            variants={modalBagAnimation}
            initial="hidden"
            animate="show"
            exit="exit"
            onClick={() => closeModal(false)}
        >
            <div className={styles.container} onClick={(e) => e.stopPropagation()}>
                <Image
                    onClick={() => closeModal(false)}
                    width={20}
                    height={20}
                    src="/assets/icons/GrFormClose.svg"
                    loader={cloudflareLoader}
                    alt=""
                />
                <small>Filtros</small>    

                <div className={styles.content}>
                    <div className={styles.selects}>
                        <Select
                            placeholder="Categoria"
                            options={sortArray(categories)}
                            styles={customSelectStyles}
                            isClearable
                            isSearchable={false}
                            value={category || null}
                            onChange={(e:any) => setCategory(e)}
                        />

                        <Select
                            placeholder="Cores"
                            options={sortArray(colors)}
                            styles={customSelectStyles}
                            isClearable
                            isSearchable={false}
                            value={color || null}
                            onChange={(e:any) => setColor(e)}
                        />

                        <Select
                            placeholder="Tamanhos"
                            options={sortArray(sizes)}
                            styles={customSelectStyles}
                            isClearable
                            isSearchable={false}
                            value={size || null}
                            onChange={(e:any) => setSize(e)}
                        />
                    </div>

                    <div className={styles.filterActions}>
                        <button onClick={filter}>Filtrar</button>
                        <button onClick={cleanFilter}>Limpar</button>
                    </div>
                </div>
            </div>
        </motion.div>
    );
}