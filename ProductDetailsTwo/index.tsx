"use client";

import styles from "./styles.module.scss";
import { Rating } from "@smastrom/react-rating"
import { toast } from "react-hot-toast";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { Suspense, useEffect, useState } from "react";
import { useCore } from "../../core-nextv3/core/core";
import { ImageSet } from "../../component-nextv3/ImageSet";
import { ProductItemSkeleton } from "../ProductItemSkeleton";
import { setItemCart } from "../../core-nextv3/cart/cart.api";
import VariantProps from "../../core-nextv3/interface/i.variant";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { useResponsive } from "../../core-nextv3/util/useResponsive";
import { productVideos } from "../../core-nextv3/video/product.util";
import { setFavorite } from "../../core-nextv3/favorite/favorite.api";
import { AnimatedLoading } from "../../component-nextv3/AnimatedLoading";
import { addItemCartAnalytics } from "../../core-nextv3/analytics/analytics.api";
import { clearHtml, innerHTML, removePhoneMask, sortArrayByProp } from "../../core-nextv3/util/util";
import { CART_SETTING, COMMET_SETTING, FAVORITE_SETTING, NOTIFICATION_PRODUCT_SETTING, PARTIAL_PRODUCT_VARIANT_SETTING, THEME_SETTING } from "../../setting/setting";
import { hasStockByListVariant, hasStockByVariant, hasStockProduct, quantityStockByListVariant } from "../../core-nextv3/stock/stock.util";
import { percentageByPaymentMethod, productDiscount, productInstallments, productPrice, productPromotionalPercentage, productPromotionalPrice, productRealPrice } from "../../core-nextv3/price/price.util";
import {
    firstProductImage,
    imageColorTableByVariant, 
    nameWithVariant,
    productImages, 
    productVariantByValue,
} from "../../core-nextv3/product/product.util";
import Link from "next/link";
import { AccordionProductTableOpen } from "../AccordionProductTableOpen";
import { useProductClickAnalytics } from "@/core-nextv3/analytics/analytics.use";
import { useGetFavorite } from "@/core-nextv3/favorite/favorite.use";
import { ProductCarouselPhotoTwo } from "@/component-nextv3/ProductCarouselPhotoTwo";
import { SizeTable } from "@/component-nextv3/SizeTable";
import { InputCalculateShipping } from "@/component-nextv3/InputCalculateShipping";
import { AccordionProduct } from "../AccordionProduct";
import { DiscountPaymentMethod } from "@/component-nextv3/DiscountPaymentMethod";
import { SizeTableModal } from "@/component-nextv3/SizeTableModal";
import { CommentBox } from "@/component-nextv3/CommentBox";
import { useAppContext } from "../Providers/app.providers";
import { useStoreContext } from "../Providers/store.providers";
import { useProductContext } from "../Providers/product.providers";
import { useCollectionDocument } from "@/core-nextv3/document/document.use";
import { useCollectionProductVariant } from "@/core-nextv3/product/product.use";
import { ProductItem } from "../ProductItem";
import { ProductEvent } from "@/component-nextv3/AnalyticsEvents/product.event";
import { addToCartRetail } from "@/core-nextv3/retail/retail.api";
import { notificationProduct } from "@/core-nextv3/product/product.api";

declare const window: any;

export function ProductDetailsTwo({ 
    productData, 
    color,
    images,
    videos
}: any) 
{
    const { isDesktop }				  	                = useResponsive();
    const router			                            = useRouter();
    const searchParams                                  = useSearchParams()
    const pathname                                      = usePathname()    
    const [ variantColor, setVariantColor ]			    = useState<VariantProps>(color);	
    const [ variantImages, setVariantImages ]			= useState(images);
    const [ variantVideos, setVariantVideos ]			= useState(videos);
    const [ productQuantity, setProductQuantity ]		= useState(1);
    const [ loadingAddToCart, setLoadingAddToCart ]	    = useState(false);
    const [ productSizeTable, setProductSizeTable ] 	= useState(false);
    const { setCart, setIsOpenCart, user }              = useCore();
    const [ productPersonTable, setProductPersonTable ] = useState(false);
    const [ hasFavorite, setHasFavorite ]               = useState<any>(false)
    const [ comments, setComments ] = useState([]);
    const [ relatedProducts, setRelatedProducts ] = useState([]);    
    const { accountData } = useAppContext();
    const { colorTableData, installmentRuleData } = useStoreContext();
    const { exchangeData, featuresProductData, measurementTableData } = useProductContext();
    const [ totalRating, setTotalRating ] = useState<any>();
    const [ variantSize, setVariantSize ] = useState<VariantProps>(() => 
    {
        const defaultVariant = productData?.variant[1]?.items?.find((item:any) => 
            hasStockByListVariant(productData, [ color, item ])
        );

        return defaultVariant;
    });

    // COMENTS
    useCollectionDocument(COMMET_SETTING.merge({
        where : [
            {
                field    : "product",
                operator : "==",
                value    : {
                    referencePath : productData?.referencePath,
                },
            },
            {
                field    : "status",
                operator : "==",
                value    : true,
            },
        ],
    }),
    (collection:any) => 
    {
        if (collection && collection.length > 0)
        {   
            let total = 0;
    
            for (const item of collection)
            {
                total += item.rating;
            }
    
            total = +Number(total / collection.length).toFixed(1);
    
            setTotalRating(total);
        }
        
        setComments(collection);
    });

    // PRODUTOS RELACIONADOS
    if (productData?.relatedProducts && productData?.relatedProducts.length > 0)
    {
        productData.relatedProducts.forEach((item:any) => 
        {
            delete item.id;
        });

        useCollectionProductVariant(PARTIAL_PRODUCT_VARIANT_SETTING.merge({
            orderBy : "order",
            where   : [
                {
                    field    : "published",
                    operator : "==",
                    value    : true,
                },		            
                {
                    field    : "_parent",
                    operator : "in",
                    value    : productData.relatedProducts,
                },		
            ],
        }),
        (collection:any) => 
        {
            setRelatedProducts(collection);
        });
    }

    // FAVORITE
    useGetFavorite(
        FAVORITE_SETTING.merge({
            document : {
                referencePath : productData?.referencePath,
            },
        }),
        (status: boolean) => 
        {
            setHasFavorite(status)
        }
    )

    // PRODUCTS ANALYTICS
    useProductClickAnalytics(productData)

    function handleIncrementProductQuantity() 
    {
        if (quantityStockByListVariant(productData, [ variantColor, variantSize ]) > productQuantity) 
        {
            setProductQuantity(productQuantity + 1);
        }
    }

    function handleDecrementProductQuantity() 
    {
        if (productQuantity > 1) 
        {
            setProductQuantity(productQuantity - 1);
        }
    }	

    async function handleChangeColor(color: any) 
    {
        let images 	    : any = null;
        let imagesGhost : any = null;
        let videos 	    : any = null;	
        const imageType       = searchParams.get("imageType");	

        if (imageType)
        {
            images = productImages(productData, [ color ], imageType);
            videos = productVideos(productData, [ color ], imageType);
        }
        else
        {
            images      = productImages(productData, [ color ], "images");
            videos      = productVideos(productData, [ color ], "videos");
            imagesGhost = productImages(productData, [ color ], "imagesGhost");

            if (imagesGhost)
            {
                images.push(...imagesGhost)
            }
        }

        setProductQuantity(1);
        setVariantColor(color);
        setVariantImages(images);
        setVariantVideos(videos);

        const pathParts = pathname.split("/").filter(Boolean);
        pathParts[pathParts.length - 1] = color?.value; 

        const newPath    = pathParts.join("/");
        const params     = searchParams.toString();
        const updatedUrl =  params ? `${newPath}/?${params}` : newPath;
        
        router.replace("/" + updatedUrl);
    }

    async function handleChangeSize(size: any) 
    {
        // if (hasStockByListVariant(productData, [ variantColor, size ]))
        // {
        setProductQuantity(1);
        setVariantSize(size);

        const params = new URLSearchParams(searchParams.toString());
        params.set("size", size?.value);

        router.replace(`?${params.toString()}`, { scroll : false });            
        //}
    }

    // async function handleCalculateShipping() 
    // {
    // 	setLoadingShipping(true);

    // 	const result = await calculateShipping(SHIPPING_SETTING.merge({
    // 		destination: destination,
    // 		weight: product.weight || 300,
    // 	}));

    // 	if(result.status) 
    // 	{
    // 		setShippingMethods(result.collection);
    // 	}

    // 	setLoadingShipping(false);
    // }

    const handleAddToCart = async () => 
    {
        if (!variantColor) 
        {
            return toast.error("Seleciona uma Cor",
                {
                    duration : 1000,
                });
        }

        if (!variantSize) 
        {
            return toast.error("Seleciona um Tamanho",
                {
                    duration : 1000,
                });
        }

        const newData = {
            data : {
                product : {
                    referencePath : productData.referencePath,
                },
                variant  : [ variantColor, variantSize ],
                quantity : productQuantity,
            },
        };

        setLoadingAddToCart(true);

        const result = await setItemCart(CART_SETTING.merge(newData));

        setLoadingAddToCart(false);

        if (!result.status) 
        {
            return toast.error(result.error,
                {
                    duration : 1000,
                });
        }

        setCart(result.data);

        toast.success("Produto adicionado com sucesso!", { icon : "👏", duration : 1000 });

        //não pode ser no product pq precisa das outras variants pra renderizar
        const productCopy = Object.assign({}, productData);
        productCopy.variant = [ variantColor, variantSize ];
        tagManager4.addToCart(productCopy, 1, user);

        // ADD ITEM CART ANALYTICS
        addItemCartAnalytics(productData, [ variantColor, variantSize ]);

        // EVENT RETAIL
        addToCartRetail(CART_SETTING);

        // push("/carrinho");
        setIsOpenCart(true);
    };

    async function handleFavoriteProduct() 
    {
        const newData = {
            document : {
                referencePath : productData?.referencePath,
            },
        };

        const result = await setFavorite(FAVORITE_SETTING.merge(newData));

        setHasFavorite(result.status);

        if (result.status) 
        {			
            toast.success("Produto adicionado!", { icon : "👏", duration : 1000 });
            tagManager4.addToWishlist(productData, user, [ variantColor, variantSize ]);
        }
        else 
        {
            toast.success("Produto removido!", { icon : "💔", duration : 1000 });
        }
    }

    async function handleNotificationProduct() 
    {
        if (user)
        {
            setLoadingAddToCart(true);

            const newData = {
                document : {
                    referencePath : productData?.referencePath,
                },
                variant : [ variantColor, variantSize ]
            };

            await notificationProduct(NOTIFICATION_PRODUCT_SETTING.merge(newData));

            toast.success("Notificação adicionada!", { icon : "👏", duration : 1000 });

            setLoadingAddToCart(false);
        }
        else
        {
            router.push("/login/")
        }        
    }

    useEffect(() => 
    {
        //let variantColor = productVariantByValue(product, query.color) || firstVariantByLevel(product, 0);
        
        const size        = searchParams.get("size");	
        const variantSize = productVariantByValue(productData, size);

        if (variantSize)
        {
            setVariantSize(variantSize);
        }
        else
        {
            const variantSize = productData?.variant[1]?.items?.find((item:any) => 
                hasStockByListVariant(productData, [ color, item ])
            );
            setVariantSize(variantSize);                    
        }
        
        let category = "";

        if (productData.categories && productData.categories.length > 0)
        {
            const singleCategory = productData.categories[productData.categories.length - 1];
            category = singleCategory.name;
        }

        // FORCA ATUALIZACAO 
        setVariantColor(color)
        setVariantImages(images);
        setVariantVideos(videos);

        tagManager4.productView(productData, category, user, [ variantColor, variantSize ]);

    }, [ pathname, searchParams ]);

    return (<>
        <div className={styles.productDetails}>
            
            <div className={styles.imagesSlider}>
                {
                    variantImages && variantImages.length > 0 ?
                        <ProductCarouselPhotoTwo perPage={isDesktop ? 2 : 1} 
                            images={variantImages} 
                            sizes="25vw"
                            videos={variantVideos} 
                            width={THEME_SETTING.widthProductThumb} 
                            height={THEME_SETTING.heightProductThumb}/>
                        : <ProductItemSkeleton perPage={isDesktop ? 2 : 1}  width={THEME_SETTING.widthProductThumb} height={THEME_SETTING.heightProductThumb}/>
                }	
            </div>				

            <div className={styles.productContent}>
                <div className={styles.productInfo}>
                    <p className={styles.productName + " productName"}>{nameWithVariant(productData?.name, [ variantColor ])}</p>
                    <div className={styles.productSku}>
                        <span>Ref</span> <span className={styles.sku}>{productData?.code}</span>
                    </div>
                    <div className={styles.productPrice}>
                        {productPromotionalPrice(productData, [ variantColor, variantSize ], false) > 0 ? (
                            <>
                                <div className={styles.saleBox}>
                                    <span className={styles.sale}>{productPrice(productData, [ variantColor, variantSize ])}</span>
                                    {!THEME_SETTING.disabledPromotionalPercentage && <span className={styles.saleOff}>-{productPromotionalPercentage(productData, [ variantColor, variantSize ], false)}%</span>}
                                </div>								
                                <p className={styles.pricePromotional}>
                                    {productPromotionalPrice(productData, [ variantColor, variantSize ])}
                                </p>
                            </>
                        ) : (
                            <span className={styles.price}>
                                {productPrice(productData, [ variantColor, variantSize ])}
                            </span>
                        )}
                    </div>
                    <div className={styles.discount}>
                        <p className={styles.productInstallments}>
                            {productInstallments(productData, installmentRuleData?.max, installmentRuleData?.minValue, [ variantColor, variantSize ])}
                        </p>
                        {percentageByPaymentMethod("pix", installmentRuleData) > 0 && <p className={styles.productPIXDiscount}>
                            OU {productDiscount(productData, percentageByPaymentMethod("pix", installmentRuleData), [ variantColor, variantSize ])} no PIX
                        </p>}
                    </div>
                    {/* <div
                        className={styles.productDescription}
                        dangerouslySetInnerHTML={{ __html: product?.description }}
                    /> */}
                    {comments?.length > 0 &&
                        <div className={styles.assessmentStar}>
                            ({totalRating})
                            <Rating           
                                style={{ maxWidth : 100 }}
                                value={totalRating}
                                readOnly={true}
                            />
                            <Link href="#comments" prefetch={true} className={styles.stars}> {comments?.length} avaliações</Link>
                            {/* {!isDesktop && <Link href="#comments" className={styles.stars}>{comments?.length} avaliações</Link>} */}
                        </div>
                    }
                    {hasStockProduct(productData) ? <div className={styles.productColorSize}>
                        <div className={styles.productColor}>
                            <span>Cor:</span>
                            <div className={styles.ballsColor}>
                                {productData?.variant[0]?.items.map((item: any, index: any) => (
                                    firstProductImage(productData, [ item ]) && <div key={index} className={styles[hasStockByVariant(productData, item) ? "inStock" : "outStock"] + " itemVariant "}>
                                        {/* <div  className={`${THEME_SETTING?.backgroundGrayNotColor ? '' : styles[hasStockByVariant(product, item) ? 'inStock' : 'outStock']}`}> */}
                                        {colorTableData && colorTableData?.status && colorTableData?.table?.length > 0 ? (
                                            <>
                                                {THEME_SETTING?.typeColorVariantImage === "square" ?
                                                    <>
                                                        {hasStockByVariant(productData, item) ?
                                                            <p
                                                                data-tip={item.label}
                                                                onClick={() => handleChangeColor(item)}
                                                                className={(variantColor && variantColor.value == item.value ? styles.active : styles.colorVariantImageCircle)}
                                                            >
                                                                <ImageSet
                                                                    width={480} 
                                                                    height={480} 
                                                                    image={imageColorTableByVariant(colorTableData, item)} 
                                                                />
                                                                {/* {variantColor && variantColor.value == item.value && <BsCheck />} */}
                                                            </p>
                                                            :
                                                            <div
                                                                className={styles.colorVariantImageCircle}
                                                                data-tip={item.label}
                                                                onClick={() => handleChangeColor(item)}
                                                            >
                                                                <p className={styles.close}>
                                                                    <ImageSet
                                                                        width={20}
                                                                        height={20}
                                                                        src="/assets/icons/GrFormClose.svg"
                                                                        responsive={false}
                                                                        alt=""
                                                                    />
                                                                </p>
                                                                <ImageSet
                                                                    width={480} 
                                                                    height={480} 
                                                                    image={imageColorTableByVariant(colorTableData, item)} 
                                                                />
                                                                {/* {variantColor && variantColor.value == item.value && <BsCheck />} */}
                                                            </div>
                                                        }
                                                    </>
                                                    :
                                                    <>
                                                        {hasStockByVariant(productData, item) ?
                                                            <div
                                                                className={styles.colorVariantImage}
                                                                data-tip={item.label}
                                                                onClick={() => handleChangeColor(item)}
                                                            >
                                                                <p className={styles.check}>
                                                                    {variantColor && variantColor.value == item.value && <ImageSet
                                                                        width={20}
                                                                        height={20}
                                                                        src="/assets/icons/BsCheck.svg"
                                                                        responsive={false}
                                                                        alt=""
                                                                    />}
                                                                </p>
                                                                <div className={styles.color}>
                                                                    <ImageSet
                                                                        width={480} 
                                                                        height={480} 
                                                                        image={imageColorTableByVariant(colorTableData, item)} 
                                                                    />																	
                                                                </div>
                                                            </div>
                                                            :
                                                            <div
                                                                className={styles.colorVariantImage}
                                                                data-tip={item.label}
                                                                onClick={() => handleChangeColor(item)}
                                                            >
                                                                <p className={styles.close}>
                                                                    <ImageSet
                                                                        width={20}
                                                                        height={20}
                                                                        src="/assets/icons/GrFormClose.svg"
                                                                        responsive={false}
                                                                        alt=""
                                                                    />
                                                                </p>
                                                                <div className={styles.color}>
                                                                    <ImageSet
                                                                        width={480} 
                                                                        height={480} 
                                                                        image={imageColorTableByVariant(colorTableData, item)} 
                                                                    />
                                                                </div>
                                                                {/* {variantColor && variantColor.value == item.value && <BsCheck />} */}
                                                            </div>
                                                        }
                                                    </>
                                                }
                                            </>
                                        ) : (
                                            firstProductImage(productData, [ item ]) && <p
                                                className={styles.colorVariantText + " " + (variantColor && variantColor.value == item.value ? styles["selected"] : "")}
                                                // data-tip={item.label}
                                                onClick={() => handleChangeColor(item)}
                                            >
                                                {item && item.value &&
                                                    item?.label
                                                }
                                            </p>
                                        )}
                                    </div>
                                ))}
                            </div>
                        </div>
                        
                        <div className={styles.productSize}>
                            <span>Tamanho:</span>
                            <div className={styles.numberSize}>
                                {sortArrayByProp(productData?.variant[1].items, "label", THEME_SETTING.variantAsc, THEME_SETTING.noSortVariant).map((item: any, index: any) => (
                                    <div className={styles.sizes} key={index}>
                                        <div className={(hasStockByListVariant(productData, [ variantColor, item ]) ? styles.check : styles.close)}>
                                            <ImageSet
                                                width={20}
                                                height={20}
                                                src="/assets/icons/BsCheck2.svg"
                                                responsive={false}
                                                alt=""
                                            />
                                        </div>
                                        <p
                                            className={styles.sizeVariant + " " + (variantSize && variantSize.value == item.value ? styles["selected"] : "")}													
                                            onClick={() => handleChangeSize(item)}
                                        >
                                            {item.label.replace(/\s/g, "")}
                                        </p>
                                    </div>
                                ))}
                            </div>
                        </div>

                        {!THEME_SETTING?.disabledQuantityStock &&
                            (quantityStockByListVariant(productData, [ variantColor, variantSize ]) > 0 && quantityStockByListVariant(productData, [ variantColor, variantSize ]) <= 3 && <p className={styles.productInStock}><small>{quantityStockByListVariant(productData, [ variantColor, variantSize ])} unidades disponíveis</small></p>)
                        }
                        
                        {THEME_SETTING?.disabledModalTabelproduct &&
                            <>
                                {productData?.measures && 
                                    <AccordionProductTableOpen title="Tamanhos e Medidas">
                                        <div className={styles.sizeTable}>
                                            {measurementTableData?.table && (
                                                <SizeTable table={measurementTableData?.table} />
                                            )}
                                        </div>
                                    </AccordionProductTableOpen>
                                }
                            </>
                        }

                        {!THEME_SETTING?.disabledModalTabelproduct &&
                            <>
                                {(productData?.measures || measurementTableData?.table || productData?.measuresTable) && <div className={styles.productLink}
                                    onClick={() => setProductSizeTable(true)}
                                >
                                    {/* <span>Tabela de Medidas</span> */}
                                    <span>Tamanhos e Medidas</span>
                                    <ImageSet
                                        width={20} 
                                        height={20} 
                                        src="/assets/tail-prod-ver-fita-metrica.png" 
                                        responsive={false}
                                        alt=""/>
                                </div>}
                            </>
                        }
                        <div className={styles.productQuantity}>
                            <span>Quantidade:</span>
                            <div className={styles.inputQuantity}>
                                <span onClick={handleDecrementProductQuantity}>-</span>
                                <span className={styles.value}>{productQuantity}</span>
                                <span onClick={handleIncrementProductQuantity}>+</span>
                            </div>
                        </div>
                    </div> 
                        :
                        <div className={styles.soldOff + " " + styles.desktop}>
                            <button type="button" className="buttonBlock">Esgotado</button>	
                        </div>
                    }

                    {productData?.description && (
                        <div className={styles.description + " productDescription " + styles.mobile} dangerouslySetInnerHTML={innerHTML(clearHtml(productData?.description))}></div>
                    )}

                    {productData?._model &&
                        <div className={styles.model + " " + styles.mobile}>
                            <p className={styles.title}>Tamanho da Modelo:</p>
                            <p>{productData?._model}</p>
                        </div>
                    }

                    <div className={styles.productActions + " " + styles.desktop}>
                        <div className={styles.contentActions}>
                            <div className={styles.iconActions}>
                                {user && (
                                    <>
                                        <span>Lista de desejos</span>
                                        {hasFavorite ? (
                                            <ImageSet
                                                onClick={() => handleFavoriteProduct()}
                                                width={20}
                                                height={20}
                                                src="/assets/icons/AiFillHeart.svg"
                                                responsive={false}
                                                alt=""
                                            />
                                        ) : (
                                            <ImageSet
                                                onClick={() => handleFavoriteProduct()}
                                                width={20}
                                                height={20}
                                                src="/assets/icons/AiOutlineHeart.svg"
                                                responsive={false}
                                                alt=""
                                            />															
                                        )}
                                    </>
                                )}
                            </div>
                            {hasStockByListVariant(productData, [ variantColor, variantSize ]) ? (
                                <button
                                    type="button"
                                    className={"block buttonBlock " + styles.submit}
                                    disabled={loadingAddToCart}
                                    onClick={() => handleAddToCart()}
                                >
                                    <ImageSet
                                        className={styles.pagarme} 
                                        width={20}
                                        height={20}
                                        src="/assets/icons/BsBag.svg"
                                        responsive={false}
                                        alt=""
                                    />
                                    {loadingAddToCart ? <AnimatedLoading /> : "Comprar"}
                                </button>
                            ) : 
                                <button
                                    type="button"
                                    className={"block buttonBlock " + styles.submit}
                                    disabled={loadingAddToCart}
                                    onClick={() => handleNotificationProduct()}
                                >
                                    <ImageSet
                                        className={styles.pagarme} 
                                        width={20}
                                        height={20}
                                        src="/assets/icons/notificacao.png"
                                        responsive={false}
                                        alt=""
                                    />
                                    {loadingAddToCart ? <AnimatedLoading /> : "Avise-me quando chegar"}
                                </button>
                            }
                        </div>								
                    </div> 

                    {productData?.description && (
                        <AccordionProduct title="Descrição" className={styles.desktop} open={true}>
                            <div dangerouslySetInnerHTML={innerHTML(clearHtml(productData?.description))} className={styles.description + " productDescription"}></div>
                        </AccordionProduct>
                    )}                    

                    <div className={styles.mobile}>
                        <p className={styles.titleShipping}>CALCULE O FRETE E PRAZO DE ENTREGA</p>
                        <InputCalculateShipping
                            product={productData} 
                            title=""
                            listVariant={[ variantColor, variantSize ]}
                            totalItems={productRealPrice(productData, null, false)}
                        />
                    </div>                

                    <AccordionProduct title="Calcule o frete e prazo de entrega" className={styles.desktop}>
                        <InputCalculateShipping
                            product={productData} 
                            title=""
                            listVariant={[ variantColor, variantSize ]}
                            totalItems={productRealPrice(productData, null, false)}
                        />
                    </AccordionProduct>

                    {productData?.composition &&
                        <AccordionProduct title="Composição" className={styles.desktop}>
                            <p>{productData?.composition}</p>
                        </AccordionProduct>						
                    }

                    <AccordionProduct title="Forma de Pagamento" className={styles.desktop}>
                        <DiscountPaymentMethod
                            all={true}
                            totalItems={productRealPrice(productData, null, false)} 
                            installmentRule={installmentRuleData}
                        />
                    </AccordionProduct>
                    
                    {productData?._model && <AccordionProduct title="Modelo" className={styles.desktop}>
                        <p>{productData?._model}</p>
                    </AccordionProduct>}

                    <AccordionProduct title="Política de troca" className={styles.desktop}>
                        <div dangerouslySetInnerHTML={innerHTML(clearHtml(exchangeData?.content))} className={styles.description}></div>
                    </AccordionProduct>
                </div>
                    
                <div className={styles.noPadding + " " + styles.mobile}>
                    
                    <section className={styles.productExtraInfo}>
                        {featuresProductData && featuresProductData?.features &&
                            featuresProductData?.features?.map((item: any, index: any) => (
                                <div className={styles.item} key={index}>
                                    <div className={styles.image}>
                                        <ImageSet                                            
                                            width={20}
                                            height={20} 
                                            image={item?.icon}
                                            responsive={false}
                                            alt=""
                                        />
                                    </div>
                                    <p>{item?.title}</p>
                                </div>
                            )
                            )}
                    </section>

                    <AccordionProduct title="Forma de Pagamento">
                        <DiscountPaymentMethod
                            all={true}
                            totalItems={productRealPrice(productData, null, false)} 
                            installmentRule={installmentRuleData}
                        />
                    </AccordionProduct>

                    {productData?.composition &&
                        <AccordionProduct title="Composição">
                            <p>{productData?.composition}</p>
                        </AccordionProduct>						
                    }

                    {/* {product?._model && <AccordionProduct title="Modelo">
                                <p>{product?._model}</p>
                            </AccordionProduct>} */}

                    <AccordionProduct title="Política de troca">
                        <div dangerouslySetInnerHTML={innerHTML(clearHtml(exchangeData?.content))} className={styles.description}></div>
                    </AccordionProduct>

                </div>

                <div className={styles.productActions + " " + styles.mobile}>
                    <div className={styles.contentActions}>
                        <div className={styles.iconActions}>
                            {user && (
                                <>
                                    {/* <span>Lista de desejos</span> */}
                                    {hasFavorite ? (
                                        <ImageSet
                                            onClick={() => handleFavoriteProduct()}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/AiFillHeart.svg"
                                            responsive={false}
                                            alt=""
                                        />
                                    ) : (
                                        <ImageSet
                                            onClick={() => handleFavoriteProduct()}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/AiOutlineHeart.svg"
                                            responsive={false}
                                            alt=""
                                        />
                                    )}
                                    <div className={styles.columnSeparator}></div>
                                </>
                            )}
                            <div
                                onClick={() =>
                                {
                                    tagManager4.registerEvent("contact", "whatsapp-button", "Contato", 0, null);
                                    window.open(
                                        `https://api.whatsapp.com/send?phone=${removePhoneMask(accountData?.whatsapp)}&text=Olá, gostaria de tirar algumas duvidas.`,
                                        "_blank"
                                    )
                                }
                                }
                            >
                                <ImageSet
                                    width={20}
                                    height={20}
                                    src="/assets/icons/BsWhatsapp.svg"
                                    responsive={false}
                                    alt=""
                                />
                            </div>
                        </div>

                        {hasStockByListVariant(productData, [ variantColor, variantSize ]) ?
                            <button
                                type="button"
                                className={"block buttonBlock " + styles.submit}
                                disabled={loadingAddToCart}
                                onClick={() => handleAddToCart()}
                            >
                                <ImageSet
                                    className={styles.pagarme} 
                                    width={20}
                                    height={20}
                                    src="/assets/icons/BsBag.svg"
                                    responsive={false}
                                    alt=""
                                />
                                {loadingAddToCart ? <AnimatedLoading /> : "Comprar"}
                            </button>
                            : 
                            <button
                                type="button"
                                className={"block buttonBlock " + styles.submit}
                                disabled={loadingAddToCart}
                                onClick={() => handleNotificationProduct()}
                            >
                                <ImageSet
                                    className={styles.pagarme} 
                                    width={20}
                                    height={20}
                                    src="/assets/icons/notificacao.png"
                                    responsive={false}
                                    alt=""
                                />
                                {loadingAddToCart ? <AnimatedLoading /> : "Avise-me quando chegar"}
                            </button>
                        }
                    </div>
                </div>				
            </div>
        </div>

        <CommentBox product={productData} comments={comments} />

        {relatedProducts?.length > 0 && (
            <>
                <p className={styles.title}>Produtos Relacionados </p>
                <div className={styles.relatedProducts}>
                    {relatedProducts?.map((product: any, index:number) =>
                        product.published == true && product.slug && (
                            <ProductItem
                                key={index}
                                product={product}
                                disabledQuery={true}
                                colorTable={colorTableData}
                                installmentRuleData={installmentRuleData}
                            />
                        )
                    )}
                </div>
            </>
        )}

        {productSizeTable && (productData?.measures?.table || measurementTableData?.table || productData?.measuresTable) && (
            <SizeTableModal
                table={productData?.measures?.table || productData?.measuresTable}
                setModal={setProductSizeTable}
                tablePerson={measurementTableData?.table}
            />
        )}

        {productPersonTable && productData?.measuresPerson?.table && (
            <SizeTableModal
                table={productData?.measuresPerson?.table}
                setModal={setProductPersonTable}
            />
        )}

        <Suspense>
            <ProductEvent productData={productData} variants={[ color ]}/>
        </Suspense>
    </>);
}
