"use client"

import styles from "./styles.module.scss";
import { THEME_SETTING } from "../../setting/setting";
import { ImageSet } from "../../component-nextv3/ImageSet";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import { Options } from "@splidejs/splide";
import Link from "next/link";

export const Matches = ({ images }: any) => 
{
    //console.error('images', images) 

    const mainOptions : Options = {
        perPage      : 4,
        perMove      : 1,
        rewind       : true,
        rewindByDrag : true,
        gap          : "2rem",
        pagination   : false,
        slideFocus   : true,
        arrows       : true,
        drag     	   : true,
        start        : 1,
        breakpoints  : { 
            1024 : {
                perPage : 2,
            },
        }
    };
  
    return (
        <section className={styles.matches}>
            <div className={styles.content}>
                
                {/* <SectionTitle category="" sectionTitle="Matches" /> */}
                {/* <p className={styles.titleMatches}>Categorias</p> */}

                <Splide
                    options={mainOptions}
                    aria-labelledby="Banners"
                    className={styles.slideContent}
                > 
                    {images && images.map((matche:any, index:any) => (
                        <SplideSlide key={index}>
                            <Link href={matche?.url} prefetch={true}>
                                <figure key={index}>
                                    <div className={styles.imageContent}> 
                                        <ImageSet
                                            image={matche?.mobile}
                                            width={THEME_SETTING.widthCategoryThumb}
                                            height={THEME_SETTING.heightCategoryThumb}
                                        />
                                    </div>
                                    {matche?.name && <div className={styles.label}>
                                        {/* <p>{matche?.name}</p> */}
                                        {/* <Link href={`${matche?.url}`}>View all</Link> */}
                                    </div>}
                                </figure>
                            </Link>
                        </SplideSlide>
                    ))}
                </Splide>
            </div>
        </section>
    );
};
