import styles from "./styles.module.scss";
import { PAGE_SETTING } from "../../setting/setting";
import { innerHTML } from "../../core-nextv3/util/util";
import { PageTitle } from "../../component-nextv3/PageTitle";
import { collectionDocument, getDocument } from "../../core-nextv3/document/document.api";
import { Suspense } from "react";

const GenerateStaticParams = async () =>
{
    const pages = await collectionDocument(PAGE_SETTING.merge({
        perPage : 100,
    }));

    let paths = [];

    if (pages.collection) 
    {
        paths = pages.collection.map((item:any) => ({
            params : { slug : item.slug || "" },
        }));
    }

    console.info("(Content Static:", paths.length, ")");

    return paths;
}

const getPageData = async (slug:string) => 
{
    const result = await getDocument(PAGE_SETTING.merge({
        slug : slug
    }))

    return { 
        pageData : result.data 
    };
}

const GenerateMetadata = async ({ params }:any) =>
{
    const { slug } = await params
    const { pageData } = await getPageData(slug);
 
    return {
        title : pageData.name,        
    }
}

const ContentPage = async ({ params }: 
{
  	params : Promise<{ slug : string }>
}) =>
{
    const { slug } = await params
    const { pageData } = await getPageData(slug);

    return ( 
        <Suspense>
            <div className={styles.contentPage + " page"}>
                {pageData?.image && (
                    <img className={styles.desk} src={pageData?.image?._url} alt="" />
                )}
                {pageData?.mobile && (
                    <img className={styles.mobile} src={pageData?.mobile?._url} alt="" />
                )}
                <div className={styles.content}>
                    <PageTitle name={pageData.name} noTitle={true}/>
                    <div className={styles.pageContent} dangerouslySetInnerHTML={innerHTML(pageData.content)}></div>
                </div>
            </div>
        </Suspense>		
    );
}

export { 
    ContentPage,
    GenerateStaticParams,
    GenerateMetadata
}