import styles from "./styles.module.scss";
import { motion } from "framer-motion";
import { header_submenu_animation } from "../animation";
import { randomNumber } from "../../core-nextv3/util/util";
import { currencyMask } from "../../core-nextv3/util/mask";
import { MenuType } from "../../component-nextv3/MenuType";
import { CarouselTopHeader } from "../../component-nextv3/CarouselTopHeader";
import { MENU_FOOTER_STATIC, THEME_SETTING } from "../../setting/setting";
import Link from "next/link";
import { ImageSet } from "@/component-nextv3/ImageSet";

export const InlineHeader = async ({
    accountData,
    topHeaderData,
    pathname,
    menuData,
    setSearchBar,
    user,
    doLogin,
    setIsOpenCart,
    cart,
    setIsMobileMenuOpen
}:any) => 
{
    return ( 
		 <div
            className={styles.header + " header " + (accountData?.hasHeaderFixed ? styles.fixed : "")}
            style={
                pathname == "/"
                    ? {
                        position   : "absolute",
                        background : "transparent",
                        color      : "#fff",
                    }
                    : {
                        color : "#000",
                    }
            }
        >
            <CarouselTopHeader topHeader={topHeaderData} />

            <div className={styles.content}>
                <div className={styles.top}>
                    <div className={styles.tranparent} />
                    <li>
                        <Link href="/" prefetch={true} aria-label={accountData?.name}>
                            {pathname == "/" ? (
                                <>
                                    <ImageSet
                                        className={styles.logo + " logo"}
                                        src="/assets/logo_white.png"
                                        alt={accountData?.name}		
                                        width={THEME_SETTING.widthLogo}
                                        height={THEME_SETTING.heightLogo}
                                        priority={true}
                                    />
                                    <ImageSet
                                        className={styles.logoColor + " logo"}
                                        src="/assets/logo.png"
                                        alt={accountData?.name}
                                        width={THEME_SETTING.widthLogo}
                                        height={THEME_SETTING.heightLogo}
                                        priority={true}
                                    />
                                </>
                            ) : (
                                <ImageSet
                                    className={styles.logoOthers + " logo"}
                                    src="/assets/logo.png"
                                    alt={accountData?.name}
                                    width={THEME_SETTING.widthLogo}
                                    height={THEME_SETTING.heightLogo}
                                    priority={true}
                                />
                            )}
                        </Link>								
                    </li>
                    <div className={styles.otherPages}>
                        {MENU_FOOTER_STATIC && (MENU_FOOTER_STATIC.map((item: any) =>
                            (
                                <motion.li
                                    className={styles.menuItemGrid}
                                    initial="animate"
                                    animate="animate"
                                    whileHover="hover"
                                    key={randomNumber(8)}
                                >
                                    {item.label}
                                    <motion.ul
                                        className={styles.subMenuGrid}
                                        variants={header_submenu_animation}
                                    >
                                        {item.children.map((item2: any) => (
                                            <p key={randomNumber(8)}>
                                                <Link href={item2.url} prefetch={true}>
                                                    {item2.label}
                                                </Link>
                                            </p>
                                        ))}
                                    </motion.ul>
                                </motion.li>
                            )))}
                    </div>
                </div>
                <div className={styles.bottom}>
                    <div></div>
                    <div className={styles.nav}>
                        <MenuType menu={menuData} />										
                    </div>
                    <ul className={styles.menu}>
                        <li
                            className={styles.cartIconPoints}
                        >
                            {user?.points > 0 && <p>
									
                                <ImageSet
                                    width={20}
                                    height={20}
                                    src="/assets/icons/BiWalletAlt.svg"
                                    responsive={false}
                                    alt=""
                                />
								
                                {currencyMask(user?.points)}
                            </p>}
                            <ImageSet
                                onClick={() => doLogin()}
                                width={20}
                                height={20}
                                src="/assets/icons/BsPerson.svg"
                                responsive={false}
                                alt=""
                            />
                        </li>

                        <li onClick={() => setSearchBar(true)} aria-label="Busca">
                            <ImageSet
                                width={20}
                                height={20}
                                src="/assets/icons/BiSearch.svg"
                                responsive={false}
                                alt=""
                            />
                        </li>

                        <li>
                            <Link href="/lista-de-desejos/" prefetch={true} aria-label="Lista de Desejo">
                                <ImageSet
                                    width={20}
                                    height={20}
                                    src="/assets/icons/AiOutlineHeart.svg"
                                    responsive={false}
                                    alt=""
                                />
                            </Link>									
                        </li>
							
                        <li 
                            className={styles.cartIcon}
                        >
                            {/* <Link href="/carrinho" aria-label="Carrinho"> */}
                            <ImageSet
                                onClick={() => setIsOpenCart(true)}
                                width={20}
                                height={20}
                                src="/assets/icons/BsBag.svg"
                                responsive={false}
                                alt=""
                            />	
                            <span aria-hidden="true">{cart?.items?.length || 0}</span>
                            {/* </Link> */}
                        </li>
                    </ul>
                </div>
                <ul className={styles.mobileMenu}>
                    <li>
                        <ImageSet
                            onClick={() => setIsMobileMenuOpen(true)}
                            className={styles.mobileMenuNav}
                            src="/assets/icons/bars-solid.svg"
                            responsive={false}
                            alt=""
                            width={THEME_SETTING.widthLogo}
                            height={THEME_SETTING.heightLogo}
                        />
                        <Link href="/" prefetch={true} aria-label={accountData?.name}>
                            <ImageSet className={styles.mobileHeaderLogo + " logo"}
                                src="/assets/logo.png"
                                alt={accountData?.name}
                                width={THEME_SETTING.widthLogo}
                                height={THEME_SETTING.heightLogo}
                                responsive={false}
                                priority={true}
                            />
                        </Link>
                    </li>
                    <li
                        className={styles.mobileSearch}
                        onClick={() => setSearchBar(true)}>
                        <ImageSet
                            width={20}
                            height={20}
                            src="/assets/icons/BiSearch.svg"
                            responsive={false}
                            alt=""
                        />
                    </li>
                    <li
                        className={styles.mobileLogin}
                        onClick={() => doLogin()}>
                        <ImageSet
                            className={styles.pagarme} 
                            width={20}
                            height={20}
                            src="/assets/icons/BsPerson.svg"
                            responsive={false}
                            alt=""
                        />
                    </li>
                    <li
                        className={styles.mobileCart}
                    >
                        {/* <Link href="/carrinho" aria-label="Carrinho"> */}
                        <ImageSet
                            onClick={() => setIsOpenCart(true)}
                            className={styles.pagarme} 
                            width={20}
                            height={20}
                            src="/assets/icons/BsBag.svg"
                            responsive={false}
                            alt=""
                        />
                        <span aria-hidden="true">{cart?.items?.length || 0}</span>
                        {/* </Link> */}
                    </li> 
                </ul>
            </div>

        </div>
    );
};
