"use client"

import styles from "./styles.module.scss";
import { useCore } from "@/core-nextv3/core/core";
import { currencyMask } from "@/core-nextv3/util/mask";
import { useHeader } from "./header.context";
import { ImageSet } from "@/component-nextv3/ImageSet";

export const UserIcon = () => 
{
    const { user } = useCore();
    const { doLogin } = useHeader();

    return ( 
        <>
            {user?.points > 0 && <p className={styles.points}>
                <ImageSet
                    width={20}
                    height={20}
                    src="/assets/icons/BiWalletAlt.svg"
                    responsive={false}
                    alt=""
                />
                <small>
                    {currencyMask(user?.points)}
                </small>                
            </p>}
            <ImageSet
                onClick={() => doLogin()}
                width={20}
                height={20}
                src="/assets/icons/BsPerson.svg"
                responsive={false}
                alt=""
            />
        </>
    );
};
