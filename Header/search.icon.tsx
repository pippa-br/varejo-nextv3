"use client"

import { useHeader } from "./header.context";
import { ImageSet } from "@/component-nextv3/ImageSet";

export const SearchIcon = () => 
{
    const { setSearchBar } = useHeader();

    return ( 
        <ImageSet
			  onClick={() => setSearchBar(true)}
            width={20}
            height={20}
            src="/assets/icons/BiSearch.svg"
            responsive={false}
            alt=""
        />
    );
};
