"use client";

import React, { createContext, useContext, useState } from "react";
import { useRouter } from "next/navigation";
import { useCore } from "@/core-nextv3/core/core";
import { useGetLoggedAuth } from "@/core-nextv3/auth/user.auth.api";
import { AUTH_SETTING, CART_SETTING } from "@/setting/setting";
import { useGetCart } from "@/core-nextv3/cart/cart.use";
import { useSetUTMParameters } from "@/core-nextv3/util/util.use";

interface HeaderContextType 
{
    isOpenMenu    : boolean;
    searchBar     : boolean;
    setSearchBar  : any;
    doLogin       : () => void;
    toggleMenu    : () => void;
}

const HeaderContext = createContext<HeaderContextType | undefined>(undefined);

export const HeaderProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => 
{
    const { user, setUser, setCart, setIsOpenMenu, isOpenMenu }  = useCore();
    const [ searchBar,  setSearchBar ]  = useState(false);

    useGetLoggedAuth(AUTH_SETTING, (data: any) => 
    {
        setUser(data);
    });

    useGetCart(CART_SETTING, (data: any) => 
    {
        setCart(data);
    });

    useSetUTMParameters();

    const router = useRouter();

    const doLogin = () => 
    {        
        setIsOpenMenu(false);

        if (user) 
        {
            router.push("/perfil");
        } 
        else 
        {
            router.push("/login");
        }
    };

    const toggleMenu = () => 
    {
        setIsOpenMenu(!isOpenMenu)
    }

    return (
        <HeaderContext.Provider value={{ 
            isOpenMenu,
            searchBar,
            setSearchBar,  
            doLogin,
            toggleMenu,
        }}>
            {children}
        </HeaderContext.Provider>
    );
};

export const useHeader = () => 
{
    const context = useContext(HeaderContext);
    
    if (!context) 
    {
        throw new Error("useHeader must be used within a HeaderProvider");
    }
    
    return context;
};