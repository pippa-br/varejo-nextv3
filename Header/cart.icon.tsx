"use client"

import styles from "./styles.module.scss";
import { useCore } from "@/core-nextv3/core/core";
import { ImageSet } from "@/component-nextv3/ImageSet";

export const CartIcon = () => 
{
    const { cart, setIsOpenCart } = useCore();

    return ( 
        <>
            <ImageSet
                onClick={() => setIsOpenCart(true)}
                responsive={false}
                className={styles.pagarme} 
                width={20}
                height={20}
                src="/assets/icons/BsBag.svg"
                alt=""
            />
            <span
                aria-hidden="true"
                onClick={() => setIsOpenCart(true)}
            >
                {cart?.items?.length || 0}
            </span>
        </>
    );
};
