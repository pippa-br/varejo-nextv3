"use client"

import styles from "./styles.module.scss";
import { MenuType } from "../../component-nextv3/MenuType";
import { CarouselTopHeader } from "../../component-nextv3/CarouselTopHeader";
import { THEME_SETTING } from "../../setting/setting";
import Link from "next/link";
import { InlineHeader } from "./inline.header";
import { StickyHeader } from "./sticky.header";
import { HeaderProvider } from "./header.context";
import { SearchIcon } from "./search.icon";
import { SearchBar } from "../SearchBar";
import { CartIcon } from "./cart.icon";
import ModalCart from "@/component-nextv3/ModalCart";
import { UserIcon } from "./user.icon";
import { HeaderMobile } from "../HeaderMobile";
import { ImageSet } from "@/component-nextv3/ImageSet";

export const Header = ({ accountData, topHeaderData, menuData, giftData }:any) => 
{ 
    return ( 
        <HeaderProvider>

            {!THEME_SETTING.disabledHeaderInline &&
				<InlineHeader/>
            }
 
            <StickyHeader topHeader={topHeaderData}>
                <>
                    {topHeaderData?.published === true &&
							<div className={styles.topHeaderDesktop}>
							    <CarouselTopHeader topHeader={topHeaderData} />
							</div>
                    }

                    <div className={styles.content}>
                        <div>
                            <Link href="/" prefetch={true} aria-label={accountData?.name}>
                                <ImageSet className={styles.logo + " logo"}
                                    alt={accountData?.name + ""} 
                                    src="/assets/logo.png"												
                                    width={THEME_SETTING.widthLogo}
                                    height={THEME_SETTING.heightLogo}
                                    responsive={false}  
                                    priority={true}			 
                                />
                            </Link>
                        </div>
                        <div className={styles.nav}>
                            <MenuType menu={menuData} />						
                        </div>
                        <ul className={styles.menu}>
                            <li
                                className={styles.cartIconPoints}
                            >
                                <UserIcon/>
                            </li>
                            <li>
                                <SearchIcon/>
                            </li>
                            <li>
                                <Link href="/lista-de-desejos" prefetch={true} aria-label="Lista de Desejo">
                                    <ImageSet
                                        width={20}
                                        height={20}												
                                        src="/assets/icons/AiOutlineHeart.svg"
                                        responsive={false}
                                        alt=""
                                    />
                                </Link>							
                            </li>
                            <li
                                className={styles.cartIcon}
                            >
                                <CartIcon/>
                            </li>
                        </ul>					
                    </div>	
                </>
 
            </StickyHeader>

            <HeaderMobile
                menu={menuData}
                account={accountData}
                topHeader={topHeaderData}
                giftData={giftData}
            />

            <SearchBar/>
            
            <ModalCart giftPage={giftData}/>

        </HeaderProvider>
    );
};
