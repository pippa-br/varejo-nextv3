"use client"

import styles from "./styles.module.scss";
import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import { THEME_SETTING } from "@/setting/setting";
import { isDesktop } from "react-device-detect";
declare const window: any;

export const StickyHeader = ({ children }:any) => 
{
    const [ stickyHeaderNone, setStickyHeaderNone ] = useState("fixed");
    const [ stickyHeader,	   setStickyHeader ]     = useState("normal");

    const pathname = usePathname();

    const nonPathsHeaderStick: boolean = [ "/produtos/[slug]", "/produto-two/[slug]" ].includes( pathname );
	
    useEffect(() => 
    {			
        if (isDesktop)
        {
            window.addEventListener("scroll", () => 
            {
				 if (window.scrollY >= 100) 
                {
                    setStickyHeader("sticky");
                }
                else 
                {
                    setStickyHeader("normal");
                }
            });	
        }
    }, [ isDesktop ]);

    useEffect(() => 
    {			
        if (!isDesktop)
        {
            window.addEventListener("scroll", () => 
            {
                if (window.scrollY >= 1 && THEME_SETTING?.disabledHeaderSticke == true && nonPathsHeaderStick) 
                {
                    setStickyHeaderNone("none");
                }
                else 
                {
                    setStickyHeaderNone("fixed");
                }
            });	
        }		
    }, [ !isDesktop ]);

    return ( 
        <div className={`header ${stickyHeaderNone === "none" ? styles.headerInitial : styles.headerSticky + " " + styles[stickyHeader] + " " + (THEME_SETTING.disabledHeaderInline ? styles.headerStickyFixed  : styles.headerNormalFixed)}`}>
            {children}
        </div>
    );
};
