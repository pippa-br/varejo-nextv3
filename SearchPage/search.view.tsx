"use client"

import { Suspense, useLayoutEffect } from "react";
import { usePathname, useSearchParams } from "next/navigation";
import { THEME_SETTING } from "../../setting/setting";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { useStoreContext } from "../Providers/store.providers";
import { productsVariantViewsAnalytics } from "@/core-nextv3/analytics/analytics.api";
import { useCore } from "@/core-nextv3/core/core";
import { ProductList } from "../ProductList";
import useProductFilter from "@/core-nextv3/product/product.filter";
import { useSearchProducts } from "@/core-nextv3/Hooks/useSearchProducts";

const SearchView = ({ where = [], perPage = THEME_SETTING.perPage || 24 }:any) =>
{
    const searchParams               							  = useSearchParams();
    const pathname               								  = usePathname();
    const { user } 								                  = useCore();
    const { setFilter, registerCallback, resetFilters } 		  = useProductFilter();
    const { colorTableData, installmentRuleData } = useStoreContext();
    
    const { loading, items, hasNextPage, loadProducts, totalItems } = useSearchProducts({ 
        onLoadProducts : (items:any) =>
        {
            // GTM
            tagManager4.addProducts(pathname, items, user);

            // VIEWS ANALYTICS
            productsVariantViewsAnalytics(items);
        }  
    })

    useLayoutEffect(() => 
    {
        registerCallback(loadProducts);
        resetFilters();
        
        const termQuery    = searchParams.get("term");
        const pageQuery    = searchParams.get("page");        

        if (termQuery)
        {
            setFilter("term", termQuery);
        }

        setFilter("perPage", perPage);
        setFilter("where",   where);
        setFilter("page",    pageQuery ? parseInt(pageQuery) : 1);

    }, [])

    return (
        <Suspense>

            <ProductList 
                loading={loading} 
                products={items} 								 
                colorTableData={colorTableData}
                totalItems={totalItems}
                installmentRuleData={installmentRuleData}
                hasNextPage={hasNextPage} 
                loadProducts={loadProducts} />
                
        </Suspense>
    );
}

export { SearchView }