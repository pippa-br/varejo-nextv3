import styles from "./styles.module.scss";
import { THEME_SETTING } from "../../setting/setting";
import { PageTitle } from "../../component-nextv3/PageTitle";
import { SearchView } from "./search.view";
import { Suspense } from "react";

const GenerateMetadata = async () =>
{
    return {
        title : "Busca"
    }
}

const SearchPage = async ({ where = [], orderBy = "order", asc = false, pageContent, perPage = THEME_SETTING.perPage || 24 }:any) =>
{
    return (
        <Suspense>
            <div className={styles.newsPage + " page"}>
                {pageContent && <div className={styles.categoryBanner}>
                    {pageContent?.image && <img
                        className={styles.desktop}
                        src={pageContent?.image?._url}
                        alt=""
                    />}
                    {!THEME_SETTING.disabledFotoCategoryMobile && pageContent?.mobile && <img
                        className={styles.mobile}
                        src={pageContent?.mobile?._url}
                        alt=""
                    />}
                </div>}
                <div className={styles.content}>
                    <PageTitle name={"Busca"} noTitle={true}/>
                    <SearchView 
                        where={where}
                        orderBy={orderBy}
                        asc={asc}
                        perPage={perPage}/>
                </div>
            </div>
        </Suspense>		
    );
}

export { 
    SearchPage,
    GenerateMetadata
}