import { ImageSet } from "@/component-nextv3/ImageSet";
import styles from "./styles.module.scss";

export const FeaturesColumns = ({ features }: any) => 
{
    return (
        <div className={styles.featureColumns}>
            <div className={styles.content}>
                {features?.map((feature: any) => (
                    feature.status && <div className={styles.column} key={feature.id}>
                        <div className={styles.columnIcon}>
                            <ImageSet                  
                                image={feature?.icon}
                                alt="Icon"
                                width={300}
                                height={300} 
                            />
                        </div>
                        <div className={styles.info}>
                            <p className={styles.title}>{feature?.title}</p>
                            <p className={styles.text}>{feature?.subtitle}</p>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};