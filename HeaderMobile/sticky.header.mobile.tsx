"use client"

import styles from "./styles.module.scss";
import { useHeaderState, useShowFooter } from "@/core-nextv3/util/util.style";
import { useCore } from "@/core-nextv3/core/core";

export const StickyHeaderMobile = ({ children }:any) => 
{
    const showFooter 	 = useShowFooter();
    const headerState    = useHeaderState();
    const { isOpenMenu } = useCore();

    return ( 
        <div className={`
					${styles.mainHeader}
					${ styles[showFooter && headerState ? "sticky" : "transparent"]}
					${(isOpenMenu) && styles.open}
			`}
        >
            {children}
        </div>
    );
};
