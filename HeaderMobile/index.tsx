import styles from "./styles.module.scss";
import { THEME_SETTING } from "@/setting/setting";
import { CarouselTopHeader } from "@/component-nextv3/CarouselTopHeader";
import Link from "next/link";
import { MobileMenu } from "../MobileMenu";
import { SearchIcon } from "../Header/search.icon";
import { UserIcon } from "../Header/user.icon";
import { CartIcon } from "../Header/cart.icon";
import { StickyHeaderMobile } from "./sticky.header.mobile"; 
import { SearchBar } from "../SearchBar";
import ModalCart from "@/component-nextv3/ModalCart";
import { MenuIcon } from "./menu.icon";
import { ImageSet } from "@/component-nextv3/ImageSet";

export const HeaderMobile = ({ account, topHeader, menu, giftData }: any) => 
{
    return (
        <>
            
            <div className={styles.header + " header"}>
                
                <StickyHeaderMobile>

                    {topHeader?.published === true &&
                        <div className={styles.topHeaderMobile}>
                            <CarouselTopHeader topHeader={topHeader} />
                        </div>
                    }

                    <div className={styles.content}>
                        <div className={styles.leftTopMobile}>
                            <MenuIcon/>
                        </div>
                        <div className={styles.center}>						
                            <Link href="/" prefetch={true} aria-label={account?.name}>
                                <ImageSet
                                    className={styles.mobileHeaderLogo + " " + styles.logo}
                                    src={"/assets/logo.png"}
                                    width={THEME_SETTING.widthLogo}
                                    height={THEME_SETTING.heightLogo}
                                    alt={account?.name || ""}			
                                    priority={true}	
                                    responsive={false}					
                                />
                            </Link>
                        </div>
                        <ul className={styles.right}>
                            <li
                                className={styles.mobileIconPoints}
                            >
                                <UserIcon/>
                            </li>
                            <li
                                className={styles.mobileSearch}>
                                <SearchIcon/>
                            </li>
                            <li
                                className={styles.mobileCart}
                            >
                                <CartIcon/>
                            </li>
                        </ul>
                    </div>

                </StickyHeaderMobile>

            </div>

            <MobileMenu menu={menu}/>

            <SearchBar/>

            <ModalCart giftPage={giftData}/>            
            
        </>
    );
}