"use client"

import { useHeader } from "../Header/header.context";
import styles from "./styles.module.scss";

export const MenuIcon = () => 
{
    const { toggleMenu, isOpenMenu } = useHeader();

    return ( 
    	<div
            onClick={() => toggleMenu()}
            className={`${styles.headerMenu} ${ isOpenMenu && styles.open }`}
        >
            <nav className={`${styles.menuBurguer} ${styles.open}`} />
        </div>
    );
};
