import React from "react";
import { getDocument } from "@/core-nextv3/document/document.api";
import { COLOR_TABLE_SETTING, COLOR_VARIANT_SETTING, GATEWAY_SETTING, SIZE_VARIANT_SETTING } from "@/setting/setting";
import { calls } from "@/core-nextv3/util/call.api";
import { StoreProvider } from "../Providers/store.providers";

const getStoreServer = async () => 
{
    const [ colorVariantsResult, sizeVariantsResult, colorTableResult, installmentRuleResult ] : any = await calls(
        getDocument(COLOR_VARIANT_SETTING.merge({ cache : "force-cache" })), 
        getDocument(SIZE_VARIANT_SETTING.merge({ cache : "force-cache" })),       
        getDocument(COLOR_TABLE_SETTING.merge({ cache : "force-cache" })),
        getDocument(GATEWAY_SETTING.merge({ cache : "force-cache" }))        
    );

    return {
        colorVariantsData   : colorVariantsResult.data ? colorVariantsResult.data?.items : [],
        sizeVariantsData    : sizeVariantsResult.data ? sizeVariantsResult.data?.items : [],
        colorTableData      : colorTableResult?.data || {},
        installmentRuleData : installmentRuleResult?.data?.installmentRule || {}
    };
}

export default async function StoreServer({
    children,
}: Readonly<{
  children: React.ReactNode;
}>) 
{
    const { colorVariantsData, sizeVariantsData, colorTableData, installmentRuleData } = await getStoreServer();    

    return (
        <StoreProvider 
            colorVariantsData={colorVariantsData} 
            sizeVariantsData={sizeVariantsData}
            colorTableData={colorTableData}
            installmentRuleData={installmentRuleData}>
            {children}
        </StoreProvider>	
    );
}