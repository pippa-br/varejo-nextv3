import "core-js";
import "@splidejs/react-splide/css"; 
import "scroll-behavior-polyfill";
import "react-aspect-ratio/aspect-ratio.css";
import "@djthoms/pretty-checkbox";
import "@smastrom/react-rating/style.css"
import "video.js/dist/video-js.css";
import "react-modern-drawer/dist/index.css"
import "vanilla-cookieconsent/dist/cookieconsent.css";
// import 'videojs-contrib-hls';

// BUG BROWSER ANTIGOS
if (!Array.prototype.at) 
{
    Object.defineProperty(Array.prototype, "at", 
        {
            value : function (index:number) 
            {
                index = Math.trunc(index) || 0;
                if (index < 0) index += this.length;
                if (index < 0 || index >= this.length) return undefined;
                return this[index];
            },
            writable     : true,
            enumerable   : false,
            configurable : true,
        });
}

import { AppProvider } from "../Providers/app.providers";
import { getAccount } from "@/core-nextv3/account/account.api";
import { collectionDocument, getDocument } from "@/core-nextv3/document/document.api";
import { calls } from "@/core-nextv3/util/call.api";
import { ACCOUNT_SETTING, GIFT_PAGE_SETTING, MENU_SETTING, POPUP_PAGE_SETTING, TOP_HEADER_PAGE_SETTING } from "@/setting/setting";


const getAppPageServer = async () => 
{
    const [ accountResult, topHeaderResult, menuResult, giftResult, popupResult ] = await calls(
        getAccount(ACCOUNT_SETTING.merge({ cache : "force-cache" })), 
        getDocument(TOP_HEADER_PAGE_SETTING.merge({ cache : "force-cache" })), 
        collectionDocument(MENU_SETTING.merge({
            cache : "force-cache",
            where : [
                {
                    field    : "_level",
                    operator : "==",
                    value    : 1,
                },
            ],
        })), 
        getDocument(GIFT_PAGE_SETTING.merge({ cache : "force-cache" })),
        getDocument(POPUP_PAGE_SETTING.merge({ cache : "force-cache" })),        
    );

    return {
        accountData   : accountResult?.data || {},
        topHeaderData : topHeaderResult?.data || {},
        menuData      : menuResult?.collection || [],
        giftData      : giftResult?.data || {},
        popupData     : popupResult?.data || {},        
    };
}

const RootServer = async ({ children }: any) => 
{
    const { 
        accountData, 
        topHeaderData,
        giftData,
        popupData, 
        menuData,
    } = await getAppPageServer();

    return (
        <AppProvider 
            accountData={accountData} 
            topHeaderData={topHeaderData}
            giftData={giftData}
            menuData={menuData}
            popupData={popupData}>   
            
            {children}

        </AppProvider>
    );
}

export { RootServer };
