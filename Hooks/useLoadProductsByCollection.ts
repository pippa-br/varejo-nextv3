import { collectionProductVariantByCollection } from "@/core-nextv3/product/product.api";
import { calls } from "@/core-nextv3/util/call.api";
import { PARTIAL_PRODUCT_VARIANT_SETTING } from "@/setting/setting";
import { useState } from "react";

export function useLoadProductsByCollection({ collectionData, onLoadProducts }:any) 
{
    const [ loading, setLoading ] = useState(false);
    const [ items, setItems ] = useState<any[]>([]);
    const [ totalItems, setTotalItems ] = useState<number>(0);
    const [ hasNextPage, setHasNextPage ] = useState<boolean>(true);
    const [ error, setError ] = useState<Error>();

    async function loadProducts(filter:any) 
    {
        setLoading(true);

        try 
        {
            const params : any = {
                page     : filter.page,
                perPage  : filter.perPage,
                document : {
                    referencePath : collectionData.referencePath
                }                        
            };

            const [ result ] = await calls(
                collectionProductVariantByCollection(PARTIAL_PRODUCT_VARIANT_SETTING.merge(params)),
            );

            setTotalItems(result.total);

            if (result.collection && result.collection.length > 0)
            {
                if (filter.merge)
                {
                    setItems([ ...items, ...result.collection ]);                
                }
                else
                {
                    setItems(result.collection);
                }

                setHasNextPage(filter.perPage == result.total);

                if (onLoadProducts)
                {
                    onLoadProducts(result.collection)
                }
            }
            else
            {
                setHasNextPage(false);
            }	

            setLoading(false);
        }
        catch (error_) 
        {
            setError(
                error_ instanceof Error ? error_ : new Error("Something went wrong"),
            );
        }
        finally 
        {
            setLoading(false);
        }
    }

    return { loading, items, hasNextPage, error, loadProducts, totalItems };
}