"use client"

import styles from "./styles.module.scss"
import { toast } from "react-hot-toast";
import { useForm } from "react-hook-form";
import { useCookies } from "react-cookie";
import { useEffect, useState } from "react";
import { useCore } from "../../core-nextv3/core/core";
import { cloudflareLoader, getRecaptcha, innerHTML } from "../../core-nextv3/util/util";
import ErrorMessage from "../../component-nextv3/ErrorMessage";
import { mergeCart, setCouponCart } from "../../core-nextv3/cart/cart.api";
import { validateEmail } from "../../core-nextv3/util/validate";
import { AUTH_SETTING, CART_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../../component-nextv3/AnimatedLoading";
import { ImageSet } from "../../component-nextv3/ImageSet";
import { addUserAuth, loginAuth } from "../../core-nextv3/auth/auth.api";
import dynamic from "next/dynamic";
import Image from "next/image";
declare const document: any;
interface ProductModalHomeProps {
    popupData: any
}

const Drawer = dynamic(() => import("react-modern-drawer"), {
    ssr : false,
})

export const ProductModalHome = ({ popupData }:ProductModalHomeProps) => 
{
    const [ isOpen, setIsOpen ]                                = useState<boolean>(false);
    const [ isSend, setIsSend ]                                = useState<boolean>(false);
    const [ cookies, setCookie ]                               = useCookies([ "homePopup" ]);
    const { awaitLoadingHTTP, setAwaitLoadingHTTP, setCart, cart, setUser } = useCore();

    const {
        register,
        handleSubmit,
        //setValue,
        //watch: watch,
        formState: { errors },
    } = useForm();

    useEffect(() => 
    {
        if (isOpen) 
        {
            document.body.style.overflow = "hidden";
        } 
        else 
        {
            document.body.style.overflow = "auto";
        }
        
    }, [ isOpen ]);

    useEffect(() => 
    {
    //   setTimeout(() => {
    //     if (document) {
    //       const element = document.getElementById("whatsPopover");
    //       if (element) {
    //         element!.style!.display = "none";
    //       }
    //       return;
    //     }
    //   }, 5000);
  
        if (!cookies.homePopup) 
        {
            setTimeout(() => 
            {
                setIsOpen(true);
            }, 3000);   
        }

    }, []);

    const onClose = () => 
    {        
        const expires = new Date()
    	expires.setTime(expires.getTime() + (24 * 60 * 60 * 1000));

        setCookie("homePopup", "close", { path : "/", expires });

        setIsOpen(false);
    };

    const onSubmit = async (data: any) => 
    {
        const token = await getRecaptcha("addUser");

        if (token) 
        {
            setAwaitLoadingHTTP(true);

            // CADASTRO
            const newData = {
                token : token,
                data  : {
                    email  	 : data.email.toLowerCase(),
                    password : data.email.toLowerCase()
                },
            };

            const result = await addUserAuth(AUTH_SETTING.merge(newData));
            
            // CADASTRO NOVO FAZ LOGIN
            if (result.status) 
            {
                const token = await getRecaptcha("login");

                // LOGIN
                const result2 = await loginAuth(AUTH_SETTING.merge({
                    login  	 : data.email.toLowerCase(),
                    password : data.email.toLowerCase(),
                    token    : token
                }));

                // MERGE CART SESSION
                await mergeCart(CART_SETTING.merge({
                    document : {
                        referencePath : cart?.referencePath
                    }
                }));

                setUser(result2.data);
            }

            const couponData = {
                _code : popupData.coupon.toUpperCase().replace(/\s/g, ""),
            };

            const result2 = await setCouponCart(CART_SETTING.merge(couponData));

            setCart(result2.data);         

            toast("Seu Cupom foi adicionado ao carrinho!", { icon : "👏", duration : 1000 });

            const expires = new Date()
            expires.setTime(expires.getTime() + (365 * 24 * 60 * 60 * 1000));

            setCookie("homePopup", "close", { path : "/", expires });

            setIsSend(true);
            
            setAwaitLoadingHTTP(false);

            onClose();
        }        
    };

    return (
        <>
            {isOpen && <Drawer
                className={styles.productModalHome}
                open={isOpen}
                onClose={onClose}
                direction="top"
                duration={0}
            >
                <section>
                    <div className={styles.close} >
                        <Image                            
                            onClick={onClose}
                            width={30}
                            height={30}
                            src="/assets/icons/GrFormClose.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                    </div>                    
                    <div className={styles.image}>
                        <ImageSet
                            image={popupData?.image}
                            width={1024}
                            height={1024}
                        />
                    </div>                    
                    <div className={styles.content}>
                        <h1>{popupData?.title}</h1>
                        <h2>{popupData?.subTitle}</h2>
                        <p>{popupData?.discount}</p>
                        <h2>{popupData?.information}</h2>

                        {!isSend ? <div className={styles.emailInput}>
                            <input
                                type="email"
                                placeholder="Digite seu email..." 
                                {...register("email", {
                                    validate : (value) => validateEmail(value) || "E-mail inválido",
                                })}
                            />
                            {errors.email && <ErrorMessage message={errors.email?.message}/>}

                            <button onClick={handleSubmit(onSubmit)}>                  
                                {awaitLoadingHTTP ? (
                                    <AnimatedLoading />
                                ) : (
                                    "Cadastre-se"
                                )}
                            </button>
                        </div> : <div className={styles.coupon}>Cupom: {popupData.coupon}</div>}

                        {/* <small>
                            *Cupom não válido para outras promoções, descontos e seção OFF
                        </small> */}
                        <div
                            dangerouslySetInnerHTML={innerHTML(popupData?.description)}
                            className={styles.description}
                        ></div>
                    </div>
                    {/* <button className={styles.closeButton} onClick={onClose}>Fechar</button> */}
                </section>
            </Drawer>}
        </>        
    );
}