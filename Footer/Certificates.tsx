import { THEME_SETTING } from "@/setting/setting";
import styles from "./styles.module.scss";
import Link from "next/link";
import { ImageSet } from "@/component-nextv3/ImageSet";

export const Certificates = () => 
{
    return (
        <section className={styles.certificates}>
            {/* <h1>Certificados</h1> */}
            {THEME_SETTING?.footerInfoAddress ?
                <div className={styles.certificatesListAddress}>
                    <div className={styles.imagesRight}>
                        <div className={styles.images}>
                            <ImageSet
                                className={styles.pagarme} 
                                width={80}
                                height={28}
                                src="/assets/icons/gateway.png"
                                responsive={false}
                                alt=""
                            />
                            <ImageSet
                                className={styles.letsencrypt} 
                                width={80}
                                height={18}
                                src="/assets/icons/letsencrypt.svg"
                                responsive={false}
                                alt="Let's Encrypt"
                            />
                            <Link href="https://binext.com.br" prefetch={true} target="_blank" rel="noreferrer">
                                <ImageSet
                                    className={styles.binext} 
                                    width={80}
                                    height={16}
                                    src="/assets/binext_logo_gray.png"
                                    responsive={false}
                                    alt="BINEXT"
                                />
                            </Link>
                        </div>
                        <div className={styles.address} dangerouslySetInnerHTML={{ __html : THEME_SETTING.footerInfoAddress }}></div>
                    </div>
                    {THEME_SETTING?.logoUpCommerce &&
                    <ImageSet
                        className={styles.upCommerce} 
                        src="/assets/up-commerce-logo.png"
                        responsive={false}
                        alt="BINEXT"
                        width={80}
                        height={16}
                    />
                    }
                </div>
                :
                <div className={styles.certificatesList}>
                    <ImageSet
                        className={styles.pagarme} 
                        // priority
                        width={80}
                        height={28}
                        src="/assets/icons/gateway.png"
                        responsive={false}
                        alt=""
                    />
                    <ImageSet
                        className={styles.letsencrypt} 
                        // priority
                        src="/assets/icons/letsencrypt.svg"
                        responsive={false}
                        width={80}
                        height={18}
                        alt="Let's Encrypt"
                    />
                    <Link href="https://binext.com.br" prefetch={true} target="_blank" rel="noreferrer">
                        <ImageSet
                            className={styles.binext} 
                            src="/assets/binext_logo_gray.png"
                            responsive={false}
                            alt="BINEXT"
                            width={80}
                            height={16}
                        />
                    </Link>
                    {THEME_SETTING?.logoUpCommerce &&
                    <ImageSet
                        className={styles.upCommerce} 
                        src="/assets/up-commerce-logo.png"
                        responsive={false}
                        alt="BINEXT"
                        width={80}
                        height={16}
                    />
                    }
                </div>
            }
        </section>
    );
};
