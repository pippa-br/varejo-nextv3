import styles from "./styles.module.scss";
import { useState } from "react";
import { ImageSet } from "@/component-nextv3/ImageSet";

export const FadeInContent = ({ homePage }: any) => 
{  
    const [ isPlaying, setIsPlaying ] = useState<boolean>(true);

    return (
        <section className={styles.fadeInContent}>
            {isPlaying ? (
                <ImageSet
                    onClick={() => setIsPlaying(!isPlaying)}
                    width={20}
                    height={20}
                    src="/assets/icons/BsPause.svg"
                    responsive={false}
                    alt=""
                />
            ) : (
                <ImageSet
                    onClick={() => setIsPlaying(!isPlaying)}
                    width={20}
                    height={20}
                    src="/assets/icons/BsPlay.svg"
                    responsive={false}
                    alt=""
                />
            )}

            <div className={styles.features}>
                <div className={`${styles.marquee} ${!isPlaying && styles.pause}`}>
                    {homePage?.features?.map((item: any, index: any) => (
                        <div className={styles.item} key={index}>
                            <img src={item?.icon?._url} alt="" />
                            <p>
                                {item?.subtitle}
                            </p>
                        </div>
                    ))}
                    {/* <p>
            <BsTruck />
            Frete grátis em compras acima de R$499
          </p>
          <p>
            <GoIssueReopened />7 dias corridos para devolução após o recebimento
            do produto
          </p>
          <p>
            <RiSecurePaymentLine />
            Segurança processo de compra 100% segura
          </p> */}
                </div>
            </div>
        </section>
    );
};
