import styles from "./styles.module.scss";
import { THEME_SETTING } from "../../setting/setting";
import { ImageSet } from "@/component-nextv3/ImageSet";
declare const window: any;

export const Sac = ({ account }: any) => 
{
    return (
        <section className={styles.sac}>
            <div className={styles.topSac}>
                {THEME_SETTING.sacInfo2 && 
          <div>
              <h1>DEVOLUÇÃO</h1>
              <div dangerouslySetInnerHTML={{ __html : THEME_SETTING.sacInfo2 }}></div>
          </div>
                }
                <div>
                    <h1>
                        {/* <i>
              <img
                width={20}
                height={20}
                src="/assets/icons/sac.svg"
                loader={cloudflareLoader}
                alt="SAC"
              />
            </i>  */}
            SAC
                    </h1>
                    <div dangerouslySetInnerHTML={{ __html : THEME_SETTING.sacInfo1 }}></div>
                </div>
            </div>

            <div className={styles.socials}>
                {account?.instagram && 
            <ImageSet
                onClick={() => window.open(account?.instagram, "_blank")}
                width={20}
                height={20}
                src="/assets/icons/BsInstagram.svg"
                responsive={false}
                alt=""
            />
                }

                {account?.pinterest && 
          <ImageSet
              onClick={() => window.open(account?.pinterest, "_blank")}
              width={20}
              height={20}
              src="/assets/icons/BsPinterest.svg"
              responsive={false}
              alt=""
          />}

                {account?.facebook && <ImageSet
                    onClick={() => window.open(account?.facebook, "_blank")}
                    width={20}
                    height={20}
                    src="/assets/icons/BsFacebook.svg"    
                    responsive={false}          
                    alt=""
                />}

                {account?.youtube && 
          <ImageSet
              onClick={() => window.open(account?.youtube, "_blank")}
              width={20}
              height={20}
              src="/assets/icons/BsYoutube.svg"
              responsive={false}
              alt=""
          />}
            </div>
            {/* <div>
        <h1>
          <i>
            <img
              width={20}
              height={20}
              src="/assets/icons/whatsapp.svg"
              alt="WhatsApp"
            />
          </i>
          SAC
        </h1>
        <div dangerouslySetInnerHTML={{ __html: THEME_SETTING.sacInfo2 }}></div>
      </div> */}
        </section>
    );
};
