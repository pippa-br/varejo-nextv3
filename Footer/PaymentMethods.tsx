import { THEME_SETTING } from "../../setting/setting";
import styles from "./styles.module.scss";
import { ImageSet } from "@/component-nextv3/ImageSet";

export const PaymentsMethods = () => 
{
    return (
        <section className={styles.paymentMethods}>
            {/* <h1>Formas de Pagamento</h1> */}
            <div className={styles.paymentsList}>
                <ImageSet
                    width={40}
                    height={32}
                    src="/assets/icons/visa.svg"
                    responsive={false}
                    priority
                    alt="Visa"
                />
                <ImageSet
                    width={40}
                    height={32}
                    src="/assets/icons/mastercard.svg"
                    responsive={false}
                    alt="Master"
                />
                <ImageSet  
                    width={40} 
                    height={32} 
                    src="/assets/icons/elo.svg"
                    responsive={false}
                    alt="Elo" />
                <ImageSet
                    width={40} 
                    height={32} 
                    src="/assets/icons/pix.svg"
                    responsive={false}
                    alt="Pix"/>
                <ImageSet
                    width={40}
                    height={32}
                    src="/assets/icons/americanexpress.svg"
                    responsive={false}
                    alt="American Express"
                />
                <ImageSet
                    width={40}
                    height={32}
                    src="/assets/icons/dinners.svg"
                    responsive={false}
                    alt="Dinners"
                />
                <ImageSet
                    width={40}
                    height={32}
                    src="/assets/icons/hipercard.svg"
                    responsive={false}
                    alt="Hipercard"
                />
            </div>
            <p>{THEME_SETTING.paymentInfo}</p>
        </section>
    );
};
