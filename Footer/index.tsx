"use client";

import styles from "./styles.module.scss";
import { Newsletter } from "./Newsletter";
import { Accordions } from "./Accordions";
import { FooterMenuDesk } from "./FooterMenuDesk";
import { Sac } from "./Sac";
import { Bottom } from "./Bottom";

export const Footer = ({ accountData }: any) => 
{
    return (
        <footer className={styles.footer + " footer"}>
            <div className={styles.contentFooter}>
                <Newsletter />
                <Accordions />
                <FooterMenuDesk />
                <Sac account={accountData} />
            </div>        
            <Bottom />
        </footer>
    );
};
