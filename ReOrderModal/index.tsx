import { useForm } from "react-hook-form";
import Select from "react-select";
import styles from "./styles.module.scss";
import { useState } from "react";
import { toast } from "react-hot-toast";
import { AnimatedLoading } from "../../component-nextv3/AnimatedLoading";
import Image from "next/image";
import { cloudflareLoader } from "@/core-nextv3/util/util";

export function ReOrderModal({
    order,
    setModal,
    reorderType,
    reorderRefund,
}: any) 
{
    const [ loadingRequest, setLoadingRequest ] = useState(false);
    let selectedItems: any = [];
    const {
        register,
        handleSubmit,
        // watch,
        // formState: { errors },
    } = useForm();

    const customSelectStyles: any = {
        control : (base: any, state: any) => ({
            ...base,
            borderColor : state.isFocused
                ? "var(--border-color)"
                : "var(--border-color)",
            boxShadow : state.isFocused ? "none" : "",
            "&:hover" : {
                borderColor : "var(--border-color)",
            },
            width        : "100%",
            color        : "#000",
            fontSize     : "16px",
            borderRadius : "20px",
            padding      : "3px 15px",
        }),
        singleValue        : (styles: any) => ({ ...styles, color : "#000" }),
        indicatorSeparator : () => "",
        option             : (styles: any) => ({ ...styles, fontSize : "16px" }),
    };

    function handleChangeType(e: any) 
    {
        register("type", { value : e, required : true });
    }

    function handleChangeRefund(e: any) 
    {
        register("refund", { value : e, required : true });
    }

    // function parseProducts(value: any) 
    // {
    //     const products = [];

    //     if (value.length > 0) 
    //     {
    //         for (let i = 0; i < order?.items?.length; i++) 
    //         {
    //             for (let j = 0; j < value.length; j++) 
    //             {
    //                 if (value[j] == order?.items[i].id) 
    //                 {
    //                     products.push(order?.items[i]);
    //                 }
    //             }
    //         }
    //     }

    //     return products;
    // }

    const submitRequest = async (data: any) => 
    {
        setLoadingRequest(true);

        // const newData = {
        //     order : {
        //         referencePath : order.referencePath,
        //     },
        //     data : {
        //         items  : [ ...selectedItems ],
        //         type   : data.type,
        //         refund : data.refund,
        //     },
        // };

        if (!data.type || !data.refund || selectedItems?.length == 0) 
        {
            setModal(false);
            setLoadingRequest(false);
            return toast.error(
                "Oops! Verifique se todos os campos do formulário foram preenchidos corretamente!",
                {
                    duration : 1000,
                }
            );
        }

        const result : any = null;//await orderPlugin.addReorder(newData);

        if (!result || !result.status) 
        {
            setModal(false);
            setLoadingRequest(false);
            return toast.error(
                "Oops! Verifique se todos os campos do formulário foram preenchidos corretamente!",
                {
                    duration : 1000,
                }
            );
        }

        toast.success("Pedido feito com sucesso!", {
            duration : 1000,
        });

        setModal(false);

        setLoadingRequest(false);
    };

    function handleChangeCheckboxes(e: any) 
    {
        if (e.checked == true) 
        {
            selectedItems.push(
                ...order.items.filter((item: any) => item.code == e.value)
            );
        }
        else 
        {
            selectedItems = selectedItems.filter(
                (element: any) => element.code != e.value
            );
        }
    }

    return (
        <div className={styles.reOrderModal}>
            <div className={styles.content}>
                <Image
                    className={styles.closeModal}
                    onClick={() => setModal(false)}
                    width={20}
                    height={20}
                    src="/assets/icons/GrFormClose.svg"
                    loader={cloudflareLoader}
                    alt=""
                />
                <div className={styles.selectProducts}>
                    <p className={styles.title}>
            Selecione quais produtos deseja troca/devolver:
                    </p>
                    <form>
                        {order?.items?.map((item: any, index: number) => (
                            <div className={styles.formItem} key={index}>
                                <input
                                    type="checkbox"
                                    value={item.code}
                                    name={item.name}
                                    onChange={(e: any) => handleChangeCheckboxes(e.target)}
                                />
                                <label>
                                    {item?.name} | {item?.variant[0].label} |{" "}
                                    {item?.variant[1].label}
                                </label>
                            </div>
                        ))}
                    </form>
                </div>

                <Select
                    placeholder="Tipo de solicitação"
                    className={styles.selectOption}
                    options={reorderType.items}
                    styles={customSelectStyles}
                    isClearable
                    onChange={(e) => handleChangeType(e)}
                />
                <Select
                    placeholder="Como gostaria de ser Reembolsado?"
                    options={reorderRefund.items}
                    className={styles.selectOption}
                    styles={customSelectStyles}
                    isClearable
                    onChange={(e) => handleChangeRefund(e)}
                />
                <button
                    className={styles.reOrderButton}
                    type="button"
                    disabled={loadingRequest && true}
                    onClick={handleSubmit(submitRequest)}
                >
                    {loadingRequest ? <AnimatedLoading /> : "Solicitar"}
                </button>
            </div>
        </div>
    );
}
