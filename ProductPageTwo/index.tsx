import styles from "./styles.module.scss"
import { ProductDetailsTwo } from "../ProductDetailsTwo"
import {
    collectionDocument,
} from "../../core-nextv3/document/document.api"
import {
    FULL_PRODUCT_SETTING,
    SLUG_PRODUCT_SETTING,
} from "../../setting/setting"
import { firstVariantByLevel, productImages, productVariantByValue } from "@/core-nextv3/product/product.util"
import { productVideos } from "@/core-nextv3/video/product.util"
import { getProduct } from "@/core-nextv3/product/product.api"
import { stripHtmlTags } from "@/core-nextv3/util/util"
import JsonLd from "@/component-nextv3/JsonLd"
import { productRealPrice } from "@/core-nextv3/price/price.util"
import { env } from "process"
import { Suspense } from "react"

const getProductPageServer = async (slug:string) => 
{
    const result = await getProduct(FULL_PRODUCT_SETTING.merge({
        slug : slug
    }))

    return {
        productData : result.data
    };
}

const GenerateStaticParams = async () =>
{
    const products = await collectionDocument(
        SLUG_PRODUCT_SETTING.merge({
            perPage : 10000,
            where   : [
                {
                    field    : "published",
                    operator : "==",
                    value    : true,
                },
            ],
        }),
    )

    const paths = []

    if (products.collection)
    {
        for (const product of products.collection)
        {
            if (product.variant)
            {
                for (const item of product.variant[0].items)
                {
                    paths.push({ slug : product.slug, color : item.value });
                }
            }
        }
    }

    console.info("(Products Static:", paths.length, ")");

    return paths;
}

const GenerateMetadata = async ({ params }:any) =>
{
    const { slug, color } = await params;
    const { productData } = await getProductPageServer(decodeURIComponent(slug));

    //PEGAR IMAGENS E COR
    const colorVariant = productVariantByValue(productData, decodeURIComponent(color)) || firstVariantByLevel(productData, 0);
    const images      : any = productImages(productData, [ colorVariant ], "images");
    const videos      : any = productVideos(productData, [ colorVariant ], "videos");
    const imagesGhost : any = productImages(productData, [ colorVariant ], "imagesGhost");

    if (imagesGhost)
    {
        images.push(...imagesGhost)
    }
 
    return {
        title       : `${productData.name} ${colorVariant.label}`,
        description : stripHtmlTags(productData.description),
        openGraph   : {
            title       : productData.name,
            description : stripHtmlTags(productData.description),
            images      : images.map((item: any) => item._url),
            videos      : videos.map((item: any) => item._url),
            url         : `${env.NEXT_PUBLIC_DOMAIN}/produtos/${productData.slug}/${colorVariant.value}/`,
            locale      : "pt_BR",
            price       : {
                amount   : productRealPrice(productData, [ colorVariant ]),
                currency : "BRL",
            },
            availability : "in stock",
            icons        : {
                icon : "/favicon.ico",
            },
        },
    }
}

const ProductPageTwo = async ({ params }: any) => 
{
    const { slug, color } = await params;
    const { productData } = await getProductPageServer(decodeURIComponent(slug));

    //PEGAR IMAGENS E COR
    const colorVariant = productVariantByValue(productData, decodeURIComponent(color)) || firstVariantByLevel(productData, 0);
    const images      : any = productImages(productData, [ colorVariant ], "images");
    const videos      : any = productVideos(productData, [ colorVariant ], "videos");
    const imagesGhost : any = productImages(productData, [ colorVariant ], "imagesGhost");

    if (imagesGhost)
    {
        images.push(...imagesGhost)
    } 

    return (
        <div className={styles.productPage + " page"}>

            <JsonLd 
                name={`${productData.name} ${colorVariant.label}`}
                description={productData.description}
                productData={productData} 
                images={images} 
                videos={videos}
                price={productRealPrice(productData, [ colorVariant ])}
                url={`${env.NEXT_PUBLIC_DOMAIN}/produtos/${productData.slug}/${colorVariant.value}/`}/>
      
            <div className={styles.content}>
                        
                <Suspense>
                    <ProductDetailsTwo
                        productData={productData}
                        color={colorVariant}
                        images={images}
                        videos={videos}
                    />
                </Suspense>         
                    
            </div>
        </div>
    )
}

export {
    ProductPageTwo,
    GenerateMetadata,
    GenerateStaticParams
}
