import styles from "./styles.module.scss";
import Select from "react-select";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { sortArray } from "../../core-nextv3/product/product.util";
import { customSelectStyles } from "../../core-nextv3/util/util.style";

export const Filters = ({ colors, sizes, updateFilter }: any) => 
{
    const [ defaultColorFilter, setDefaultColorFilter ] = useState<any>();
    const [ defaultSizeFilter,  setDefaultSizeFilter ]  = useState<any>();
    const [ defaultOrderFilter, setDefaultOrderFilter ] = useState<any>();
    const { query, push, isReady } = useRouter();

    const orderOptions = [
	  { value : "salesAmount|false",   label : "Mais Vendidos" },
	  { value : "order|false", 	      label : "Mais Recentes" },
	  { value : "order|true", 	      label : "Mais Antigos" },
	  { value : "indexes.price|false", label : "Maior Preço" },
	  { value : "indexes.price|true",  label : "Menor Preço" }
    ]

    const onColor = (event:any) =>
    {
        if (event) 
        {
            push({ query : { ...query, color : event.value } }, undefined, { scroll : false });
        } 
        else 
        {
            delete query.color;
            push({ query : { ...query } }, undefined, { scroll : false });		
        }		
    }

    const onSize = (event:any) =>
    {
        if (event) 
        {
            push({ query : { ...query, size : event.value } }, undefined, { scroll : false });
        } 
        else 
        {
            delete query.size;
            push({ query : { ...query } }, undefined, { scroll : false });		
        }		
    }

    const onOrderBy = (event:any) =>
    {
        if (event) 
        {
            const values  = event.value.split("|");
            push({ query : { ...query, orderBy : values[0], asc : values[1] } }, undefined, { scroll : false });
        } 
        else 
        {
            delete query.orderBy;
            delete query.asc;
            push({ query : { ...query } }, undefined, { scroll : false });		
        }		
    }

    useEffect(() => 
    {
        if (isReady)
        {
            if (query.color) 
            {
                setDefaultColorFilter(
                    colors.filter((color: any) => color.value == query.color)
                );
            } 
            else 
            {
                setDefaultColorFilter(null);
            }
	
            if (query.size) 
            {
                setDefaultSizeFilter(
                    sizes.filter((size: any) => size.value == query.size)
                );
            } 
            else 
            {
                setDefaultSizeFilter(null);
            }
	
            if (query.orderBy && query.asc !== undefined) 
            {
                const value  = query.orderBy + "|" + query.asc;
                setDefaultOrderFilter(orderOptions.filter((order: any) => order.value == value));
            } 
            else 
            {
                setDefaultOrderFilter(null);
            }
			
            updateFilter();
        }
    }, [ isReady, colors ]);

    return (
        <div className={styles.container}>
            <Select
                placeholder="Cores"
                options={sortArray(colors)}
                styles={customSelectStyles}
                isClearable
                isSearchable={false}
                value={defaultColorFilter || null}
                onChange={(e:any) => onColor(e)}
            />
            <Select
                placeholder="Tamanhos"
                options={sortArray(sizes)}
                styles={customSelectStyles}
                isClearable
                isSearchable={false}
                value={defaultSizeFilter || null}
                onChange={(e:any) => onSize(e)}
            />
            <Select
                placeholder="Ordenar Por"
                options={orderOptions}
                styles={customSelectStyles}
                isClearable
                isSearchable={false}
                value={defaultOrderFilter || null}
                onChange={(e:any) => onOrderBy(e)}
            />
            {/* <Select
				placeholder="Tipo"
				options={typeOptions}
				styles={customSelectStyles}
				isClearable
			/>
			<Select
				placeholder="Preço"
				options={priceOptions}
				styles={customSelectStyles}
				isClearable
			/> */}
        </div>
    );
};
