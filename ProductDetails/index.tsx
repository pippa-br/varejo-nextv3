import styles from "./styles.module.scss";
import { useEffect, useState } from "react";
import { toast } from "react-hot-toast"; 
import { useRouter } from "next/router";
import VariantProps from "../../core-nextv3/interface/i.variant";
import { setItemCart } from "../../core-nextv3/cart/cart.api";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { setFavorite } from "../../core-nextv3/favorite/favorite.api";
import { ImageSet } from "../../component-nextv3/ImageSet";
import { SizeTableModal } from "../../component-nextv3/SizeTableModal";
import {
    firstProductImage,
    firstVariantByLevel,
    imageColorTableByVariant,
    nameWithVariant,  
    productImages,
    productVariantByValue,
} from "../../core-nextv3/product/product.util";
import { AnimatedLoading } from "../../component-nextv3/AnimatedLoading";
import { CART_SETTING, FAVORITE_SETTING, THEME_SETTING } from "../../setting/setting";
import { addItemCartAnalytics } from "../../core-nextv3/analytics/analytics.api";
import { hasStockByListVariant, hasStockByVariant, hasStockProduct, quantityStockByListVariant } from "../../core-nextv3/stock/stock.util";
import { percentageByPaymentMethod, productDiscount, productInstallments, productPrice, productPromotionalPercentage, productPromotionalPrice, productRealPrice } from "../../core-nextv3/price/price.util";
import { ProductCarouselPhoto } from "../../component-nextv3/ProductCarouselPhoto";
import { ProductItemSkeleton } from "../ProductItemSkeleton";
import { productVideos } from "../../core-nextv3/video/product.util";
import { clearHtml, cloudflareLoader, innerHTML } from "../../core-nextv3/util/util";
import { AccordionProduct } from "../AccordionProduct";
import { InputCalculateShipping } from "../../component-nextv3/InputCalculateShipping";
import { useResponsive } from "../../core-nextv3/util/useResponsive";
import { useCore } from "../../core-nextv3/core/core";
import { DiscountPaymentMethod } from "../../component-nextv3/DiscountPaymentMethod";
import Image from "next/image";
  
export function ProductDetails({ product, user, hasFavorite, setHasFavorite, colorTable, page, installmentRule }: any) 
{
    // console.log('usuario', product);

    const { isDesktop }				  			      = useResponsive();
    const [ variantSize, setVariantSize ]				  = useState<VariantProps>();
    const [ variantColor, setVariantColor ]			  = useState<VariantProps>();
    const { push, query, asPath, isReady }			  = useRouter();
    const [ variantImages, setVariantImages ]			  = useState([]);
    const [ variantVideos, setVariantVideos ]			  = useState([]);
    const [ productQuantity, setProductQuantity ]		  = useState(1);
    const [ productSizeTable, setProductSizeTable ] 	  = useState(false);
    const [ loadingAddToCart, setLoadingAddToCart ]	  = useState(false);
    const [ productPersonTable, setProductPersonTable ] = useState(false);
    const { setCart,  setIsOpenCart } = useCore();

    function handleIncrementProductQuantity() 
    {
        if (quantityStockByListVariant(product, [ variantColor, variantSize ]) > productQuantity) 
        {
            setProductQuantity(productQuantity + 1);
        }
    }

    function handleDecrementProductQuantity() 
    {
        if (productQuantity > 1) 
        {
            setProductQuantity(productQuantity - 1);
        }
    }	

    async function handleChangeColor(color: any) 
    {
        let images : any = null;
        let videos : any = null;

        if (query.imageType)
        {
            const imageType : any = query.imageType;
            images = productImages(product, [ variantColor ], imageType);
            videos = productVideos(product, [ variantColor ], imageType);
        }

        if (!images)
        {
            images = productImages(product, [ variantColor ], "images");
            videos = productVideos(product, [ variantColor ], "videos");
        }

        setProductQuantity(1);
        setVariantColor(color);
        setVariantImages(images);
        setVariantVideos(videos);
        push({ query : { ...query, color : color?.value } });
    }

    async function handleChangeSize(size: any) 
    {
        if (hasStockByListVariant(product, [ variantColor, size ]))
        {
            setProductQuantity(1);
            setVariantSize(size);
            push({ query : { ...query, size : size?.value } }, undefined, { scroll : false });
        }
    }

    // async function handleCalculateShipping() 
    // {
    // 	setLoadingShipping(true);

    // 	const result = await calculateShipping(SHIPPING_SETTING.merge({
    // 		destination: destination,
    // 		weight: product.weight || 300,
    // 	}));

    // 	if(result.status) 
    // 	{
    // 		setShippingMethods(result.collection);
    // 	}

    // 	setLoadingShipping(false);
    // }

    const handleAddToCart = async () => 
    {
        if (!variantColor) 
        {
            return toast.error("Seleciona uma Cor",
                {
                    duration : 1000,
                });
        }

        if (!variantSize) 
        {
            return toast.error("Seleciona um Tamanho",
                {
                    duration : 1000,
                });
        }

        const newData = {
            data : {
                product : {
                    referencePath : product.referencePath,
                },
                variant  : [ variantColor, variantSize ],
                quantity : productQuantity,
            },
        };

        setLoadingAddToCart(true);

        const result = await setItemCart(CART_SETTING.merge(newData));

        setLoadingAddToCart(false);

        if (!result.status) 
        {
            return toast.error(result.error,
                {
                    duration : 1000,
                });
        }

        setCart(result.data);

        toast.success("Produto adicionado com sucesso!", { icon : "👏", duration : 1000 });

        //não pode ser no product pq precisa das outras variants pra renderizar
        const productCopy = Object.assign({}, product);
        productCopy.variant = [ variantColor, variantSize ];
        tagManager4.addToCart(productCopy, 1, user);

        // ADD ITEM CART ANALYTICS
        addItemCartAnalytics(product, [ variantColor, variantSize ]);

        // push("/carrinho");
        setIsOpenCart(true)
    };

    async function handleFavoriteProduct(product: any) 
    {
        const newData = {
            document : {
                referencePath : product?.referencePath,
            },
        };

        const result = await setFavorite(FAVORITE_SETTING.merge(newData));

        setHasFavorite(result.status);

        if (result.status) 
        {				
            toast.success("Produto adicionado!", { icon : "👏", duration : 1000 });
            tagManager4.addToWishlist(product, user, [ variantColor, variantSize ]);
        }
        else 
        {
            toast.success("Produto removido!", { icon : "💔", duration : 1000 });
        }
    }

    useEffect(() => 
    {
        if (isReady) 
        {
            const variantColor = productVariantByValue(product, query.color) || firstVariantByLevel(product, 0);
            let variantSize  = productVariantByValue(product, query.size);

            if (variantColor) 
            {
                let images : any = null;
                let videos : any = null;

                if (query.imageType)
                {
                    const imageType : any = query.imageType;
                    images = productImages(product, [ variantColor ], imageType);
                    videos = productVideos(product, [ variantColor ], imageType);
                }

                if (!images)
                {
                    images = productImages(product, [ variantColor ], "images");
                    videos = productVideos(product, [ variantColor ], "videos");
                }

                setVariantColor(variantColor);
                setVariantImages(images);
                setVariantVideos(videos);

                if (variantSize)
                {
                    setVariantSize(variantSize);
                }
                else 
                {
                    if (product?.variant?.length > 1 && product.variant[1]?.items)
                    {
                        for (const item of product.variant[1].items) 
                        {
                            if (hasStockByListVariant(product, [ variantColor, item ])) 
                            {
                                variantSize = item;
                                setVariantSize(item);
                                break;
                            }
                        }
                    }                    
                }

                let category = "";

                if (product.categories && product.categories.length > 0)
                {
                    const singleCategory = product.categories[product.categories.length - 1];
                    category = singleCategory.name;
                }

                tagManager4.productView(product, category, user, [ variantColor, variantSize ]);
            }
        }

    }, [ isReady, asPath ]);

    return (<>
        <div className={styles.productDetails}>
            
            <div>
                {
                    variantImages?.length > 0 ?
                        <ProductCarouselPhoto perPage={isDesktop ? 2 : 1} 
                            images={variantImages} 
                            sizes="25vw"
                            videos={variantVideos} 
                            width={THEME_SETTING.widthProductThumb}  
                            height={THEME_SETTING.heightProductThumb}/>
                        : <ProductItemSkeleton height={700}/>
                }				
            </div>

            <div className={styles.productInfo}>
                <p className={styles.productName}>{nameWithVariant(product?.name, [ variantColor ])}</p>
                <div className={styles.productSku}>
                    <span>Ref</span> <span className={styles.sku}>{product?.code}</span>
                </div>
                <div className={styles.productPrice}>
                    {productPromotionalPrice(product, [ variantColor, variantSize ], false) > 0 ? (
                        <>
                            <div className={styles.saleBox}>
                                <span className={styles.sale}>{productPrice(product, [ variantColor, variantSize ])}</span>
                                {!THEME_SETTING.disabledPromotionalPercentage && <span className={styles.saleOff}>{productPromotionalPercentage(product, [ variantColor, variantSize ])} OFF</span>}
                            </div>								
                            <p className={styles.pricePromotional}>
                                {productPromotionalPrice(product, [ variantColor, variantSize ])}
                            </p>
                        </>
                    ) : (
                        <span className={styles.price}>
                            {productPrice(product, [ variantColor, variantSize ])}
                        </span>
                    )}
                </div>
                <p className={styles.productInstallments}> 
                    {productInstallments(product, installmentRule?.max, installmentRule?.minValue, [ variantColor, variantSize ])}
                </p>
                {percentageByPaymentMethod("pix", installmentRule) > 0 && <p className={styles.productPIXDiscount}>
                    OU {productDiscount(product, percentageByPaymentMethod("pix", installmentRule), [ variantColor, variantSize ])} no PIX
                </p>}
                {/* <div
                    className={styles.productDescription}
                    dangerouslySetInnerHTML={{ __html: product?.description }}
                /> */}
                {hasStockProduct(product) ? <div className={styles.productColorSize}>
                    <div className={styles.productColor}>
                        <span>Cor:</span>
                        <div className={styles.ballsColor}>
                            {product?.variant[0]?.items.map((item: any, index: any) => (
                                <div key={index} className={styles[hasStockByVariant(product, item) ? "inStock" : "outStock"]}>
                                    {colorTable && colorTable?.status && colorTable?.table?.length > 0 ? (
                                        <>
                                            {THEME_SETTING?.typeColorVariantImage === "square" ?
                                                <p
                                                    className={(variantColor && variantColor.value == item.value ? styles.active : styles.colorVariantImageCircle)}
                                                    data-tip={item.label}
                                                    onClick={() => handleChangeColor(item)}
                                                >
                                                    <ImageSet
                                                        width={480} 
                                                        height={480}
                                                        // domainTo={THEME_SETTING.domainTo}
                                                        // domainFrom={THEME_SETTING.domainFrom} 
                                                        image={imageColorTableByVariant(colorTable, item)} 
                                                    />
                                                    {/* {variantColor && variantColor.value == item.value && <BsCheck />} */}
                                                </p>
                                                :
                                                <p
                                                    className={styles.colorVariantImage}
                                                    data-tip={item.label}
                                                    onClick={() => handleChangeColor(item)}
                                                >
                                                    <ImageSet
                                                        width={480} 
                                                        height={480}
                                                        // domainTo={THEME_SETTING.domainTo}
                                                        // domainFrom={THEME_SETTING.domainFrom} 
                                                        image={imageColorTableByVariant(colorTable, item)} 
                                                    />
                                                    {variantColor && variantColor.value == item.value && <Image
                                                        width={20}
                                                        height={20}
                                                        src="/assets/icons/BsCheck.svg"
                                                        loader={cloudflareLoader}
                                                        alt=""
                                                    />}
                                                </p>	
                                            }
                                        </>
                                    ) : (
                                        firstProductImage(product, [ item ]) && <p
                                            className={styles.colorVariantText + " " + (variantColor && variantColor.value == item.value ? styles["selected"] : "")}
                                            // data-tip={item.label}
                                            onClick={() => handleChangeColor(item)}
                                        >
                                            {item && item.value &&
                                                item?.label
                                            }
                                        </p>
                                    )}
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className={styles.productSize}>
                        <span>Tamanho</span>
                        <div className={styles.numberSize}>
                            {product?.variant[1].items.map((item: any, index: any) => (
                                (hasStockByListVariant(product, [ variantColor, item ]) ?
                                    (
                                        <div className={styles.sizes} key={index}>
                                            <div className={styles.check}>
                                                <Image
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/BsCheck2.svg"
                                                    loader={cloudflareLoader}
                                                    alt=""
                                                />
                                            </div>
                                            <p
                                                className={styles.sizeVariant + " " + (variantSize && variantSize.value == item.value ? styles["selected"] : "")}													
                                                onClick={() => handleChangeSize(item)}
                                            >
                                                {item.label.replace(/\s/g, "")}
                                            </p>
                                        </div>
                                    ) : 
                                    (
                                        <div className={styles.sizes} key={index}>
                                            <div className={styles.close}>
                                                <Image
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/GrFormClose.svg"
                                                    loader={cloudflareLoader}
                                                    alt=""
                                                />
                                            </div>
                                            <p className={styles.sizeVariant + " " + styles.disabled}>
                                                {item.label.replace(/\s/g, "")}
                                            </p>
                                        </div>
                                    ))
                            ))}
                        </div>
                    </div>
                    {quantityStockByListVariant(product, [ variantColor, variantSize ]) > 0 && quantityStockByListVariant(product, [ variantColor, variantSize ]) <= 3 && <p className={styles.productInStock}><small>{quantityStockByListVariant(product, [ variantColor, variantSize ])} unidades disponíveis</small></p>}
                    {(product?.measures || product?.measuresTable) && <div className={styles.productLink}
                        onClick={() => setProductSizeTable(true)}
                    >
                        <span>Tabela de Medidas</span>
                        <Image 
                            width={20}
                            height={20}
                            src="/assets/tail-prod-ver-fita-metrica.png" 
                            loader={cloudflareLoader}
                            alt=""/>
                    </div>}
                    <div className={styles.productQuantity}>
                        <span>Quantidade</span>
                        <div className={styles.inputQuantity}>
                            <span onClick={handleDecrementProductQuantity}>-</span>
                            <span className={styles.value}>{productQuantity}</span>
                            <span onClick={handleIncrementProductQuantity}>+</span>
                        </div>
                    </div>
                </div> :
                    <div className={styles.soldOff}>
                        <button type="button">Esgotado</button>	
                    </div>}
                <div className={styles.productActions}>
                    <div>
                        {user && (
                            <>
                                <span>Lista de desejos</span>
                                {hasFavorite ? (
                                    <Image
                                        onClick={() => handleFavoriteProduct(product)}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/AiFillHeart.svg"
                                        loader={cloudflareLoader}
                                        alt=""
                                    />
                                ) : (
                                    <Image
                                        onClick={() => handleFavoriteProduct(product)}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/AiOutlineHeart.svg"
                                        loader={cloudflareLoader}
                                        alt=""
                                    />
                                )}
                            </>
                        )}
                    </div>
                    {hasStockProduct(product) && (
                        <button
                            type="button"
                            className={"block " + styles.submit}
                            disabled={loadingAddToCart}
                            onClick={() => handleAddToCart()}
                        >
                            <Image
                                className={styles.pagarme} 
                                width={20}
                                height={20}
                                src="/assets/icons/BsBag.svg"
                                loader={cloudflareLoader}
                                alt=""
                            />
                            {loadingAddToCart ? <AnimatedLoading /> : "Comprar"}
                        </button>)}
                </div>
                <AccordionProduct title="Calcule o frete e prazo de entrega">
                    <InputCalculateShipping product={product} 
                        title=""
                        listVariant={[ variantColor, variantSize ]}
                        totalItems={productRealPrice(product, null, false)}/>
                </AccordionProduct>
                <AccordionProduct title="Forma de Pagamento">
                    <DiscountPaymentMethod totalItems={productRealPrice(product, null, false)} 
                        all={true}
                        installmentRule={installmentRule}/>
                </AccordionProduct>
                {product?.description && (
                    <AccordionProduct title="Descrição">
                        <div dangerouslySetInnerHTML={innerHTML(clearHtml(product?.description))} className={styles.description + " productDescription"}></div>
                    </AccordionProduct>
                )}					
                {product?.composition &&
                    <AccordionProduct title="Composição">
                        <p>{product?.composition}</p>
                    </AccordionProduct>						
                }
                {product?._model && <AccordionProduct title="Modelo">
                    <p>{product?._model}</p>
                </AccordionProduct>}
                <AccordionProduct title="Política de troca">
                    <div dangerouslySetInnerHTML={innerHTML(clearHtml(page?.content))} className={styles.description}></div>
                </AccordionProduct>						
            </div>
        </div>
        {productSizeTable && (product?.measures?.table || product?.measuresTable) && (
            <SizeTableModal
                table={product?.measuresTable || product?.measures?.table}
                setModal={setProductSizeTable}
            />
        )}
        {productPersonTable && product?.measuresPerson?.table && (
            <SizeTableModal
                table={product?.measuresPerson?.table}
                setModal={setProductPersonTable}
            />
        )}
    </>);
}
