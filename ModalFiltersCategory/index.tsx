"use client";

import styles from "./styles.module.scss";
import Select from "react-select";
import { motion } from "framer-motion";
import { useRouter, useSearchParams } from "next/navigation";
import { sortArray } from "../../core-nextv3/product/product.util";
import { customSelectStyles } from "../../core-nextv3/util/util.style";
import { modalBagAnimation } from "@/component-nextv3/animation";
import useProductFilter from "@/core-nextv3/product/product.filter";
import { ImageSet } from "@/component-nextv3/ImageSet";

export const ModalFiltersCategory = ({ closeModal, colors, sizes, loadProducts }: any) => 
{
    const router       = useRouter();
    const searchParams = useSearchParams();
    const { filter, setFilter, reloadFilter, resetField } = useProductFilter();

    const onFilter = () =>
    {
        setFilter("page", 1);
        setFilter("merge", false);
        reloadFilter(loadProducts);
        closeModal(false);
    }

    const onColor = (color:any) =>
    {
        const params = new URLSearchParams(searchParams);

        if (color) 
        {
            params.set("color", color.value);
            setFilter("color", color)
        }
        else
        {
            params.delete("color");
            resetField("color")
        }

        router.push(`?${params.toString()}`);        
    }

    const onSize = (size:any) =>
    {
        const params = new URLSearchParams(searchParams);

        if (size) 
        {
            params.set("size", size.value);
            setFilter("size", size)
        }
        else
        {
            params.delete("size");
            resetField("size")
        }

        router.push(`?${params.toString()}`);
    }

    function cleanFilter() 
    {
        const params = new URLSearchParams(searchParams);

        params.delete("color");
        params.delete("size");

        router.push(`?${params.toString()}`);
        closeModal(false);
    }

    return (
        <motion.div
            className={styles.modalFiltersCategory}
            variants={modalBagAnimation}
            initial="hidden"
            animate="show"
            exit="exit"
            onClick={() => closeModal(false)}
        >
            <div className={styles.container} onClick={(e) => e.stopPropagation()}>
                <ImageSet
                    onClick={() => closeModal(false)}
                    width={20}
                    height={20}
                    src="/assets/icons/GrFormClose.svg"
                    responsive={false}
                    alt=""
                />
                <small>Filtros</small>    

                <div className={styles.content}>
                    <div className={styles.selects}>
                        <Select
                            placeholder="Cores"
                            options={sortArray(colors)}
                            styles={customSelectStyles}
                            isClearable
                            isSearchable={false}
                            value={filter.color || null}
                            onChange={(e:any) => onColor(e)}
                        />

                        <Select
                            placeholder="Tamanhos"
                            options={sortArray(sizes)}
                            styles={customSelectStyles}
                            isClearable
                            isSearchable={false}
                            value={filter.size || null}
                            onChange={(e:any) => onSize(e)}
                        />
                    </div>

                    <div className={styles.filterActions}>
                        <button onClick={onFilter}>Filtrar</button>
                        <button onClick={cleanFilter}>Limpar</button>
                    </div>
                </div>
            </div>
        </motion.div>
    );
}