import styles from "./styles.module.scss";
import { useState } from "react";
import Link from "next/link";
import { ImageSet } from "@/component-nextv3/ImageSet";

export const MobileItemMenu = ({ handleModal, category }: any) => 
{
    const [ clothesSubMenu, setClothesSubMenu ] = useState(false);

    return (
        <div className={styles.mobileItemMenu}>
            <p
                className={styles.itemText}
                // key={category.id}
                onClick={() => 
                {
                    setClothesSubMenu(!clothesSubMenu) 
                }}
            >
                {/* <GiLargeDress /> */}
                {category?.name}

                <ImageSet
                    className={styles.Arrow}
                    width={20}
                    height={20}
                    src="/assets/icons/IoIosArrowForward.svg"
                    responsive={false}
                    alt=""
                />
            </p>

            {clothesSubMenu && (
                <div className={styles.subMenu}>
                    {category?._children?.map((child: any) => (
                        <p
                            key={child.id}
                            onClick={() => 
                            {
                                handleModal(false)
                            }}
                        >
                            <Link href={`/categoria/${child.name}`} prefetch={true}>
                                {child?.name}
                            </Link>
                        </p>
                    ))}
                </div>
            )}
        </div>
    );
}