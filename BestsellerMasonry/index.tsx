import Slider from "react-slick";
import { useRef } from "react";
import styles from "./styles.module.scss";
import { ProductItem } from "../ProductItem";
import { cloudflareLoader } from "@/core-nextv3/util/util";
import Image from "next/image";

export const BestsellerMasonry = ({ bestSellers }: any) => 
{
    const customSlider = useRef<any>(undefined);

    const sliderOpts = {
        infinite       : true,
        arrows         : false,
        speed          : 500,
        slidesToShow   : 4,
        slidesToScroll : 1,
        initialSlide   : 0,
        responsive     : [
            {
                breakpoint : 720,
                settings   : {
                    slidesToShow   : 2,
                    slidesToScroll : 2,
                    infinite       : true,
                },
            },
        ],
    };

    const nextSliderHandle = () => 
    {
        customSlider.current.slickNext();
    };

    const prevSliderHandle = () => 
    {
        customSlider.current.slickPrev();
    };

    return (
        <div className={styles.newestSlider}>
            <div className={styles.content}>
                <div className={styles.top}>
                    <p className={styles.title}>Best Sellers</p>
                    <div className={styles.sliderArrows}>
                        <Image
                            className={styles.backIcon}
                            onClick={prevSliderHandle}
                            src="/assets/icons/angle-left-solid.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                        <Image
                            className={styles.nextIcon}
                            onClick={nextSliderHandle}
                            src="/assets/icons/angle-right-solid.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                    </div>
                </div>
                <p className={styles.text}>
                    {/* Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
				eiusmod tempor incididunt ut labore et dolore magna aliqua. */}
                </p>

                <Slider
                    {...sliderOpts}
                    className="newestProductSlider"
                    ref={customSlider}
                >
                    {bestSellers?.products?.map((product: any, index: number) => (
                        <div className="slide" key={index}>
                            <ProductItem product={product} />
                        </div>
                    ))}
                </Slider>
            </div>
        </div>
    );
};

