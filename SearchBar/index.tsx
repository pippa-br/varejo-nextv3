"use client";

import styles from "./styles.module.scss";
import { useState } from "react";
import { AnimatePresence, motion } from "framer-motion";
import { useRouter } from "next/navigation";
import { searchAnimation } from "../animation";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { removeAccents } from "../../core-nextv3/util/util";
import { useCore } from "../../core-nextv3/core/core";
import { useHeader } from "../Header/header.context";
import { ImageSet } from "@/component-nextv3/ImageSet";
import useProductFilter from "@/core-nextv3/product/product.filter";

export const SearchBar = () => 
{
    const router = useRouter();
    const [ inputValue, setInputValue ] = useState("");
    const { user } = useCore();
    const { searchBar, setSearchBar } = useHeader();
    const { setFilter, reloadFilter, resetFilters } = useProductFilter();

    const onSearch = (e:any) =>
    {
        if (e.key == "Enter")
        {
            resetFilters();
            setSearchBar(false);
            
            tagManager4.registerEvent("click:search", "search", inputValue.trim(), 0, user);

            const term = removeAccents(inputValue).toLowerCase();
            
            router.push(`/busca/?term=${term}`);

            setFilter("term", term);
            reloadFilter();
        }
    }

    return (
        searchBar && <AnimatePresence
            initial={false}
            mode='wait'
            onExitComplete={() => null}
        >
            <motion.div
                className={styles.searchBar}
                variants={searchAnimation}
                initial="hidden"
                animate="show"
                exit="exit"
            >
                <div className={styles.content}>
                    <ImageSet
                        className={styles.icon}
                        onClick={() => setSearchBar(false)}
                        width={20}
                        height={20}
                        src="/assets/icons/GrFormClose.svg"
                        responsive={false}
                        alt=""
                    />
                    <input onChange={(value) => setInputValue(value.target.value)} type="search" placeholder="Pesquisar" onKeyUp={(e) => onSearch(e)}/>
                </div>
            </motion.div>

        </AnimatePresence>
    );
};
