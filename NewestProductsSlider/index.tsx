"use client"

import styles from "./styles.module.scss";
import { ProductItem } from "../ProductItem";
import { Options } from "@splidejs/splide";
import { Splide } from "@splidejs/react-splide";
import { removeSlidesEmpty } from "../../core-nextv3/util/util";
import Link from "next/link";

type NewestProductsSliderType = {
    title:any,
    products:any,
    colorTableData: any, 
    allLink:any
    installmentRuleData:any
};

export const NewestProductsSlider = ({ title, products, colorTableData, allLink, installmentRuleData }:NewestProductsSliderType) => 
{    
    const mainOptions : Options = {
        role         : "banner",
        type         : "loop",
        perPage      : 4,
        perMove      : 1,
        rewind       : true,
        rewindByDrag : true,
        gap          : "2rem",
        pagination   : false, 
        slideFocus   : true,
        arrows       : true,
        drag       	 : true,
        lazyLoad     : "nearby",
        preloadPages : 1,
        start        : 0,
        breakpoints  : { 
            1024 : {
			  	perPage : 2, 
            }
        }
    };

    return (
        <div className={styles.newestSlider}>
            <div className={styles.content}>
                <div className={styles.top}>
                    <p className={styles.title}>{title}</p>
                    <div className={styles.sliderArrows}>
                        {allLink && <Link href={allLink} prefetch={true}>Ver Todos</Link>}
                    </div>
                </div>
                <Splide
                    options={mainOptions}
                    aria-labelledby="Produtos"
                    onReady={removeSlidesEmpty}
                >
                    {products?.map((product: any, index:number) => (
                        <div key={index} className={styles.slideItem + " splide__slide"}>
                            {product?.published && <ProductItem product={product} colorTable={colorTableData} installmentRuleData={installmentRuleData}/>}
                        </div>
                    ))}
                </Splide>    
            </div>
        </div>
    );
};
