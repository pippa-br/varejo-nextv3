"use client";

import React, { Suspense } from "react";
import styles from "./styles.module.scss";
import { useSearchParams } from "next/navigation";
import { useState } from "react";
import { customSelectStyles } from "../../core-nextv3/util/util.style";
import dynamic from "next/dynamic";
import { ImageSet } from "@/component-nextv3/ImageSet";
import Select from "react-select";
import { useRouter } from "next/navigation";
import useProductFilter from "@/core-nextv3/product/product.filter";

const ModalFiltersCategory = dynamic(() => import("../ModalFiltersCategory").then((mod) => ({
    default : mod.ModalFiltersCategory
})),
{
	 ssr : false,
});

export const FilterFrontCategory = ({ colors, sizes, orderOptions, loadProducts }: any) => 
{
    const router 			          = useRouter();	
    const searchParams                = useSearchParams();
    const [ openModal, setOpenModal ] = useState(false);
    const { filter, setFilter, reloadFilter, resetField } = useProductFilter();

    const onOrderBy = (event:any) =>
    {
        const params = new URLSearchParams(searchParams);

        if (event)
        {
            const values = event.value.split("|");

            params.set("orderBy", values[0]);
            params.set("asc", 	  values[1]);
            
            setFilter("orderBy", values[0]);
            setFilter("asc",     values[1] == "true" ? true : false);
        }
        else 
        {
            params.delete("orderBy");
            params.delete("asc");

            resetField("orderBy");
            resetField("asc");
        }		

        router.push(`?${params.toString()}`);
        
        setFilter("page", 1);
        setFilter("merge", false);
        reloadFilter(loadProducts);
    }

    function cleanFilter() 
    {
        const params = new URLSearchParams(searchParams);

        params.delete("color");
        params.delete("size");

        router.push(`?${params.toString()}`);
        
        resetField("color");
        resetField("size");

        setFilter("page", 1);
        setFilter("merge", false);
        reloadFilter(loadProducts);
    }

    const getSelectedOrder = (orderBy:string, asc:boolean) =>
    {
        if (orderBy && asc !== undefined)
        {
            const value = orderBy + "|" + (asc ? "true" : "false")
            return orderOptions.filter((order: any) => order.value == value)
        }

        return null;
    } 

    return (
        <Suspense>
            <div className={styles.top}>

                {/* <div className={styles.sizeList}>
					<h1>Encontre os Produtos no seu tamanho</h1>
					<div className={styles.list}>
						{sizes && sizes.length > 0 &&
							sizes.map((size: any) => (
								<Link
									href={{
										pathname: pathname,
										query: { ...query, size: size.value },
									}}
									scroll={false}
									key={size?.id}
								>
									<small className={`${query.size === size.value && styles.active}`}>
										{size?.label}
									</small>
								</Link>
							))
						}
					</div>
				</div> */}

                <div></div>

                <div></div>

                {/* <FilterProductsSize sizes={sizes} /> */}

                <div className={styles.filtersFrontCategory}>
                    {/* <Select
						placeholder="Cores"
						options={sortArray(colors)}
						styles={customSelectStyles}
						isClearable
						isSearchable={false}
						value={defaultColorFilter || null}
						onChange={(e:any) => onColor(e)}
					/>
					<Select
						placeholder="Tamanhos"
						options={sortArray(sizes)}
						styles={customSelectStyles}
						isClearable
						isSearchable={false}
						value={defaultSizeFilter || null}
						onChange={(e:any) => onSize(e)}
					/> */}
					
                    <div className={styles.modalFilters + " " + styles[(filter.color || filter.size ? "selected" : "")]}>					
                        {filter.color && <small>{filter.color.label}</small>}
                        {filter.size && <small>{filter.size.label}</small>}					
                        <span onClick={() => setOpenModal(true)}>
							Filtro
                            <ImageSet
                                width={20}
                                height={20}
                                src="/assets/icons/BiFilter.svg"
                                responsive={false}
                                alt=""
                            />           
                        </span>
                        {(filter.color || filter.size) && <small onClick={cleanFilter}>
                            <ImageSet
                                width={20}
                                height={20}
                                src="/assets/icons/GrFormClose.svg"
                                responsive={false}
                                alt=""
                            />						
                        </small>}
                    </div>

                    <div>
                        <Select
                            placeholder="Ordenar Por"
                            options={orderOptions}
                            styles={customSelectStyles}
                            isClearable
                            isSearchable={false}
                            value={getSelectedOrder(filter.orderBy, filter.asc)}
                            onChange={(e:any) => onOrderBy(e)}
                        />
                    </div>
                </div>
            </div>			

            {openModal &&
                <ModalFiltersCategory
                    colors={colors}
                    sizes={sizes}
                    closeModal={setOpenModal}
                    loadProducts={loadProducts}
                />
            }
        </Suspense>
    );
}