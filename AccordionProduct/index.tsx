"use client"

import styles from "./styles.module.scss";
import { useState } from "react";
import { motion } from "framer-motion";
import { ImageSet } from "@/component-nextv3/ImageSet";
 
export const AccordionProduct = ({ children, title, className, open = false }: any) => 
{
    const [ toggle, setToggle ] = useState(open);

    return (
        <motion.div
            className={styles.accordionProduct + " " + className}          
        >
            <motion.p className={styles.accordionTitle} onClick={() => setToggle(!toggle)}>
                {title}
                {toggle ? (
                    <ImageSet
                        className={styles.accordionSvg}
                        width={20}
                        height={20}
                        src="/assets/icons/AiOutlineMinus.svg"
                        responsive={false}
                        alt=""
                    />
                ) : (
                    <ImageSet
                        className={styles.accordionSvg}
                        width={20}
                        height={20}
                        src="/assets/icons/AiOutlinePlus.svg"
                        responsive={false}
                        alt=""
                    />
                )}
            </motion.p>
            {toggle ? <div className={styles.content}>
                {children}
            </div> : ""}      
        </motion.div>
    );
};
