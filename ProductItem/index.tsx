"use client"

import styles from "./styles.module.scss"
//import { useSearchParams } from 'next/navigation'
import { toast } from "react-hot-toast"
import { useState } from "react"
import { useCore } from "../../core-nextv3/core/core"
import { ImageSet } from "../../component-nextv3/ImageSet"
import { setItemCart } from "../../core-nextv3/cart/cart.api"
import { tagManager4 } from "../../core-nextv3/util/TagManager4"
import { addItemCartAnalytics } from "../../core-nextv3/analytics/analytics.api"
import {
    CART_SETTING,
    THEME_SETTING, 
    parseSizeVariant,
} from "../../setting/setting"
import {
    hasStockByListVariant,
    hasStockByVariant,
} from "../../core-nextv3/stock/stock.util"
import {
    firstProductImage,
    imageColorTableByVariant,
    nameWithVariant,
    secondProductImage,
} from "../../core-nextv3/product/product.util"
import {
    percentageByPaymentMethod,
    productInstallments,
    productDiscount,
    productPrice,
    productPromotionalPercentage,
    productPromotionalPrice,
    productRealPrice,
} from "../../core-nextv3/price/price.util"
import Image from "next/image"
import { isDesktop } from "react-device-detect"
import { cloudflareLoader, sortArrayByProp } from "@/core-nextv3/util/util"
import Link from "next/link"
import { addToCartRetail } from "@/core-nextv3/retail/retail.api"

// firstLevel1Variant: Caso de produtos relacionados
export function ProductItem({
    installmentRuleData,
    product,
    colorTable,
    labelImageType = true,
}: any) 
{
    //const  searchParams = useSearchParams()
    const [ size, setSize ] = useState<any>(null)
    //const [color, setColor] = useState<any>(null)
    const [ isHovering, setIsHovering ] = useState(false)
    const [ loadingAddToCart, setLoadingAddToCart ] = useState(false)
    const { user, setCart, setIsOpenCart } = useCore();

    const [ color, setColor ] = useState(() => 
    {
        if (product.imagesListVariant)
        {
            return product.imagesListVariant[0]
        }      
    })

    // useEffect(() => {
    //   if (product.imagesListVariant) {
    //     setColor(product.imagesListVariant[0])
    //   }
    // }, [product])

    // useEffect(() => 
    // {
    //   const color = searchParams.get("color");

    //   if (
    //     !disabledQuery &&
    //     color &&
    //     product.variant &&
    //     product.variant.length > 0
    //   ) {
    //     setColor(getVarianteByValue(color, product.variant[0].items))
    //   }
    // }, [searchParams])

    const handleAddToCart = async () => 
    {
        const newData = {
            data : {
                product  : product._parent,
                variant  : [ color, size ],
                quantity : 1,
            },
        }

        toast.success("Adicionado ao carrinho...", { duration : 1000 })

        //console.error("new data", newData)

        setLoadingAddToCart(true)

        const result = await setItemCart(CART_SETTING.merge(newData))

        setLoadingAddToCart(false)

        if (!result.status) 
        {
            return toast.error(result.error, {
                duration : 1000,
            })
        }

        setCart(result.data)

        toast.success("Produto adicionado com sucesso!", { icon : "👏", duration : 1000 })

        //não pode ser no product pq precisa das outras variants pra renderizar
        const productCopy = Object.assign({}, product)
        productCopy.variant = [ color, size ]
        tagManager4.addToCart(productCopy, 1, user)

        // ADD ITEM CART ANALYTICS
        addItemCartAnalytics(product, [ color, size ])

        // EVENT RETAIL
        addToCartRetail(CART_SETTING);

        setSize(null)
        setIsOpenCart(true)
    }

    return (<>
        {hasStockByListVariant(product, product.imagesListVariant) && 
        firstProductImage(product, [ color ]) &&
        productRealPrice(product, [ color ], false) > 0 && (
            <div className={styles.container}>
                <div className={styles.imageBox}>
                          
                    <div
                        className={styles.productImage}
                        onMouseEnter={() => isDesktop ? setIsHovering(true) : false}
                        onMouseLeave={() => isDesktop ? setIsHovering(false) : false}
                    >
                        <Link
                            href={`/produtos/${product?.slug}/${encodeURIComponent(color?.value)}/?size=${encodeURIComponent(size?.value || "")}&imageType=${product?.imageType?.value || ""}`}
                            prefetch={true}
                            aria-label={product?.name}
                        >
                            {
                                <ImageSet
                                    image={
                                        isHovering
                                            ? secondProductImage(product, [ color ])
                                            : firstProductImage(product, [ color ])
                                    }
                                    width={THEME_SETTING.widthProductThumb}
                                    height={THEME_SETTING.heightProductThumb}
                                    sizes="(max-width: 992px) 50vw, 20vw"
                                />

                                // hasProductThumbVideo(product, [color]) ?
                                // 	<VideosSet field="thumb" video={getProductVideo(product, [color])} autoplay={true} loop={true} controls={false}/>
                                // :
                                // 	<ImageSet aspectRatio={THEME_SETTING.aspectRatio} image={isHovering ? secondProductImage(product, [color]) : firstProductImage(product, [color])} width={800} />
                            }
                        </Link>
                        {/* {productHasPromotional(product) && <span className={styles.promocional}>Sale</span>} */}
                        {/* {hasStockByListVariant(product, product.imagesListVariant) ? ( */}
                        <div>
                            {!THEME_SETTING.disabledPromotionalPercentage && productPromotionalPercentage(
                                product,
                                [ color, size ],
                                false
                            ) > 0 && (
                                <span className={styles.promocional}>
                                    {" "} -{productPromotionalPercentage(product, [
                                        color,
                                        size,
                                    ], false)}%
                                </span>
                            )}
                        </div>
                        {/* ) : (
                  <span className={styles.soldOff}>Esgotado</span>
                )} */}
                        {labelImageType && product.imageType && (
                            <span className={styles.imageType}>
                                {product.imageType.label}
                            </span>
                        )}
                        {product?.tags && product?.tags?.map((item: any, index: number) => (
                            <span key={index} className={styles.tag}>
                                {item.label}
                            </span>
                        ))}
                    </div>

                    {/* {!isDesktop && (
              <div className={styles.productImage}>
                <a
                  href={`/produtos/${product?.slug}/${
                    color?.value
                  }?size=${size?.value || ''}&imageType=${
                    product?.imageType?.value || ''
                  }`}
                  aria-label={product?.name}
                >
                  {
                    <ImageSet
                      image={firstProductImage(product, [color])}
                      width={THEME_SETTING.widthProductThumb}
                      height={THEME_SETTING.heightProductThumb}
                      sizes='(max-width: 992px) 50vw, 25vw'
                    />

                    // hasProductThumbVideo(product, [color]) ?
                    // 	<VideosSet field="thumb" video={getProductVideo(product, [color])} autoplay={true} loop={true} controls={false}/>
                    // :
                    // 	<ImageSet aspectRatio={THEME_SETTING.aspectRatio}  image={firstProductImage(product, [color])} width={480} />
                  }
                </a>
                {hasStockByListVariant(product, product.imagesListVariant) ? (
                  <div>
                    {!THEME_SETTING.disabledPromotionalPercentage &&
                      productPromotionalPercentage(
                        product,
                        [color, size],
                        false
                      ) > 0 && (
                        <span className={styles.promocional}>
                          {' '}
                          -{' '}
                          {productPromotionalPercentage(product, [
                            color,
                            size,
                          ])}{' '}
                        </span>
                      )}
                  </div>
                ) : (
                  <span className={styles.soldOff}>Esgotado</span>
                )}
                {labelImageType && product.imageType && (
                  <span className={styles.imageType}>
                    {product.imageType.label}
                  </span>
                )}
                {product?.tags &&
                  product?.tags?.map((item: any, index: number) => (
                    <span key={index} className={styles.tag}>
                      {item.label}
                    </span>
                  ))}
              </div>
            )} */}

                    {color && size && (
                        <button onClick={() => handleAddToCart()}>
                            {loadingAddToCart ? (
                                <p>
                                    <Image
                                        width={20}
                                        height={20}
                                        src="/assets/icons/BsCheck.svg"
                                        loader={cloudflareLoader}
                                        alt=""
                                    />
                                    <span>Adicionado</span>
                                </p>
                            ) : (
                                <p>
                                    <Image
                                        className={styles.pagarme} 
                                        width={20}
                                        height={20}
                                        src="/assets/icons/BsBag.svg"
                                        loader={cloudflareLoader}
                                        alt=""
                                    />
                                    Comprar
                                </p>
                            )}
                        </button>
                    )}
                </div>

                <p className={styles.productName + " productName"}> 
                    {nameWithVariant(product?.name, [ color ])}
                </p>

                {productPromotionalPrice(product, [ color, size ], false) > 0 ? (
                    <div className={styles.prices}>
                        <p className={styles.productPriceWithPromocional}>
                            {productPrice(product, [ color, size ])}
                        </p>
                        <p className={styles.productPricePromocional}>
                            {productPromotionalPrice(product, [ color, size ])}
                        </p>
                    </div>
                ) : (
                    <p className={styles.productPrice}>
                        {productPrice(product, [ color, size ])}
                    </p>
                )}
                {/* <p className={styles.productPrice}>
                  {productPrice(product)}
              </p> */}

                <p className={styles.productInstallments}>
                    {productInstallments(
                        product,
                        installmentRuleData?.max,
                        installmentRuleData?.minValue,
                        [ color, size ]
                    )}
                </p>

                {percentageByPaymentMethod("pix", installmentRuleData) > 0 && (
                    <p className={styles.productPIXDiscount}> ou{" "}
                        {productDiscount(
                            product,
                            percentageByPaymentMethod("pix", installmentRuleData),
                            [ color, size ]
                        )}{" "} no PIX
                    </p>
                )}

                {!THEME_SETTING.disabledDisplayColorVatiant && product?.variant && hasStockByListVariant(product, product.imagesListVariant) && (
                    <div className={styles.ballsColor}>
                        {product?.variant[0]?.items?.map((item: any, index: number) =>
                        // <p key={index} data-tip={item.label} onClick={() => setColor(item)}>
                        // 	<ImageSet width={480} image={item.image} aspectRatio={1 / 1} />
                        // 	{item.value == color && <BsCheck />}
                        // </p>
                            colorTable && colorTable?.status && colorTable?.table?.length > 0 ? (
                                <p
                                    key={index}
                                    className={styles.colorVariantImage}
                                    data-tip={item.label}
                                    onClick={() => setColor(item)}
                                >
                                    <ImageSet
                                        width={480}
                                        image={imageColorTableByVariant(colorTable, item)}
                                        aspectRatio={1 / 1}
                                    />
                                    {item.value == color?.value && <ImageSet
                                        width={20}
                                        height={20}
                                        src="/assets/icons/BsCheck.svg"
                                        alt=""
                                    />}
                                </p>
                            ) : (
                                hasStockByVariant(product, item) && firstProductImage(product, [ item ]) && (
                                    <p
                                        key={index}
                                        className={
                                            styles.colorVariantText + " " + (item.value == color?.value ? styles["selected"] : "")
                                        }
                                        //data-tip={item.label}
                                        onClick={() => setColor(item)}
                                    >
                                        {item && item.value && item?.label}
                                    </p>
                                )
                            )
                        )}
                    </div>
                )}

                {product?.variant && ( 
                    <div className={styles.ballsColor}>
                        {sortArrayByProp(product?.variant[1]?.items, "label", THEME_SETTING.variantAsc, THEME_SETTING.noSortVariant)?.map((item: any, index: number) => hasStockByListVariant(product, [ color, item ]) 
                            ? (
                                <p
                                    key={index}
                                    className={styles.colorVariantText + " " + (item.value == size?.value ? styles["selected"] : "")}
                                    onClick={() => setSize(item)}
                                >
                                    {item && item.value !== undefined && parseSizeVariant(item?.label)}
                                </p>
                            )
                            :
                            THEME_SETTING?.variantListProductNotStock && <p className={styles.colorVariantText + " " + styles.disabled} key={index}>
                                {item.label.replace(/\s/g, "")}
                            </p>
                        )}
                    </div>
                )}
            </div>
        )}
    </>);
}
