"use client";

import styles from "./styles.module.scss";
import Select from "react-select";
import { useEffect, useState } from "react";
import { customSelectStyles } from "../../core-nextv3/util/util.style";
import { productVariantByValue, sortArray } from "../../core-nextv3/product/product.util";
import dynamic from "next/dynamic";
import Image from "next/image";
import { cloudflareLoader } from "@/core-nextv3/util/util";
import { useRouter, useSearchParams } from "next/navigation";

const ModalFiltersCollection = dynamic(() => import("../ModalFiltersCollection").then((mod) => ({
    default : mod.ModalFiltersCollection
})), 
{
	 ssr : false,
}); 

export const FilterFrontCollection = ({ allProducts, updateFilters }: any) => 
{
    const router  						                        = useRouter();
    const searchParams  						                        = useSearchParams();
    const [ sizes,    		      setSizes ]    	    		= useState<any>([]);
    const [ colors,    		  	  setColors ]    	    	= useState<any>([]);
    const [ openModal,             setOpenModal ]             = useState(false);
    const [ categories,    		  setCategories ]    	    = useState<any>([]);
    const [ defaultOrderFilter,    setDefaultOrderFilter ]    = useState<any>();
    const [ size,     setSize ]     	  = useState<any>();
    const [ color,    setColor ]    	  = useState<any>();
    const [ category, setCategory ]	  = useState<any>();

    // console.log('aaaaaaaaa', defaultCategoryFilter)

    const orderOptions = [
	  { value : "indexes.price|false", label : "Maior Preço" },
	  { value : "indexes.price|true",  label : "Menor Preço" }
    ];

    // const onCategory = (event:any) =>
    // {
    //     if (event) 
    //     {
    //         push({ query : { ...query, category : event.label } }, undefined, { scroll : false });			
    //     } 
    //     else 
    //     {
    //         delete query.category;
    //         push({ query : { ...query } }, undefined, { scroll : false });
    //     }		
    // }

    // const onColor = (event:any) =>
    // {
    // 	if(event) 
    // 	{
    // 		push({ query: { ...query, color: event.value } }, undefined, {scroll: false});			
    // 	} 
    // 	else 
    // 	{
    // 		delete query.color;
    // 		push({ query: { ...query } }, undefined, {scroll: false});		
    // 	}		
    // }

    // const onSize = (event:any) =>
    // {
    // 	if(event) 
    // 	{
    // 		push({ query: { ...query, size: event.value } }, undefined, {scroll: false});
    // 	} 
    // 	else 
    // 	{
    // 		delete query.size;
    // 		push({ query: { ...query } }, undefined, {scroll: false});		
    // 	}		
    // }

    const onOrderBy = (event:any) =>
    {
        const params = new URLSearchParams(searchParams);

        if (event)
        {
            const values = event.value.split("|");

            params.set("orderBy", values[0]);
            params.set("asc", 	  values[1]);						
        }
        else 
        {
            params.delete("orderBy");
            params.delete("asc");
        }		

        router.push(`?${params.toString()}`);	
    }

    useEffect(() => 
    {
        const categoriesMap : any = {};
        const colorsMap     : any = {};
        const sizesMap      : any = {};
        const items         : any = getItems();
        const colorQuery   = searchParams.get("color");
        const sizeQuery    = searchParams.get("size");
        const orderByQuery = searchParams.get("orderBy");
        const ascQuery     = searchParams.get("asc");
        const categoryQuery = searchParams.get("category");

        // CATEGORIAS DE TODOS OS ITENS
        if (allProducts)
        {
            for (const product of allProducts)
            {
                if (product.categories instanceof Array)
                {
                    for (const item of product.categories)
                    {
                        categoriesMap[item.id] = {
                            id    : item.id,
                            label : item.name,
                            value : item.id,
                        };
                    }	
                }
            }	
        }	
                
        // VARIANTES DO PRODUTOS DA CATEGORIA
        for (const product of items)
        {
            if (product.skuVariant && product.skuVariant.length == 2)
            {
                for (const item of product.skuVariant[0].items)
                {
                    colorsMap[item.id] = item;
                }

                for (const item of product.skuVariant[1].items)
                {
                    sizesMap[item.id] = item;
                }
            }
        }

        const categories = [];
        const colors     = [];
        const sizes      = [];

        for (const key in categoriesMap)
        {
            categories.push(categoriesMap[key]);
        }

        for (const key in colorsMap)
        {
            colors.push(colorsMap[key]);
        }

        for (const key in sizesMap)
        {
            sizes.push(sizesMap[key]);
        }

        setCategories(categories);
        setColors(colors);
        setSizes(sizes);
        
        // DEFAULT CATEGORY
        if (categoryQuery) 
        {
            const category2 = categories.filter((item: any) => item.label == categoryQuery);

            if (category2.length > 0)
            {
                setCategory(category2[0]);
            }
        } 
        else
        {
            setCategory(null);
        }	

        // COLOR
        if (colorQuery) 
        {
            const color2 = colors.filter((color: any) => color.value == colorQuery)

            if (color2.length > 0)
            {
                setColor(color2[0]);
            }
        } 
        else 
        {
            setColor(null);
        }

        // SIZE
        if (sizeQuery)
        {
            const size2 = sizes.filter((size: any) => size.value == sizeQuery);

            if (size2.length > 0)
            {
                setSize(size2[0]);
            }				
        } 
        else 
        {
            setSize(null);
        }

        // ORDER BY
        if (orderByQuery && ascQuery !== undefined) 
        {
            const value  = orderByQuery + "|" + ascQuery;
            setDefaultOrderFilter(orderOptions.filter((order: any) => order.value == value));
        }
        else
        {
            setDefaultOrderFilter(null);
        }

        updateFilters(items);		

    }, [ allProducts ])

    const getItems = () =>
    {
        let items = [];
        const colorQuery   = searchParams.get("color");
        const sizeQuery    = searchParams.get("size");
        const orderByQuery = searchParams.get("orderBy");
        const ascQuery     = searchParams.get("asc");
        const categoryQuery = searchParams.get("category");

        if (allProducts)
        {
            items = allProducts.filter((item:any) => 
            {
                return item;
            });
        }

        // CATEGORY
        if (categoryQuery) 
        {
            items = items.filter((item:any) => 
            {
                for (const item2 of item.categories)
                {	
                    if (item2.name == categoryQuery)
                    {
                        return item;
                    }
                }					
            });			
        }

        // COLOR
        if (colorQuery) 
        {
            items = items.filter((item:any) => 
            {
                return productVariantByValue(item, colorQuery, "skuVariant");
            });			
        } 

        // SIZE
        if (sizeQuery)
        {
            items = items.filter((item:any) => 
            {
                return productVariantByValue(item, sizeQuery, "skuVariant");
            });
        } 

        // ORDER BY
        if (orderByQuery && ascQuery !== undefined) 
        {
            items = sortArray(items, orderByQuery, ascQuery == "true");
        }

        return items;
    }

    function cleanFilter() 
    {
        const params = new URLSearchParams(searchParams);

        params.delete("color");
        params.delete("size");
        params.delete("category");

        router.push(`?${params.toString()}`);       
    }

    return (
        <>
            <div className={styles.filtersFrontCollection}>
                {/*<Select
                    placeholder="Categoria"
                    options={sortArray(categories)}
                    styles={customSelectStyles}
                    isClearable
                    isSearchable={false}
                    // value={defaultCategoryFilter || null}
                    onChange={(e:any) => onCategory(e)}
                />

                 <Select
                    placeholder="Cores"
                    options={sortArray(colors)}
                    styles={customSelectStyles}
                    isClearable
                    isSearchable={false}
                    value={defaultColorFilter || null}
                    onChange={(e:any) => onColor(e)}
                />

                <Select
                    placeholder="Tamanhos"
                    options={sortArray(sizes)}
                    styles={customSelectStyles}
                    isClearable
                    isSearchable={false}
                    value={defaultSizeFilter || null}
                    onChange={(e:any) => onSize(e)}
                /> */}

                <div className={styles.modalFilters + " " + styles[(color || size ? "selected" : "")]}>					
                    {category && <small>{category.label}</small>}
                    {color && <small>{color.label}</small>}
                    {size && <small>{size.label}</small>}					
                    <span onClick={() => setOpenModal(true)}>
									Filtro
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/BiFilter.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />           
                    </span>
                    {(color || size || category) && <p onClick={cleanFilter}>
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/GrFormClose.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                    </p>}
                </div>

                <Select
                    placeholder="Ordenar Por"
                    options={orderOptions}
                    styles={customSelectStyles}
                    isClearable
                    isSearchable={false}
                    value={defaultOrderFilter || null}
                    onChange={(e:any) => onOrderBy(e)}
                />
            </div>

            {openModal &&
                <ModalFiltersCollection
                    closeModal={setOpenModal}
                    allProducts={allProducts}
                    updateFilters={updateFilters}
                    categories={categories}
                    colors={colors}
                    sizes={sizes}
                    color={color}
                    setColor={setColor}
                    size={color}
                    setSize={setSize}
                    category={category}
                    setCategory={setCategory}
                />
            }
        </>
    );
}