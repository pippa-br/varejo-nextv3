"use client"

import styles from "./styles.module.scss";
import Drawer from "react-modern-drawer";
import { MenuTypeMobile } from "../MenuTypeMobile";
import { MENU_FOOTER_STATIC } from "../../setting/setting";
import { AccordionSubMenuItem } from "../AccordionSubMenuItem";
import Link from "next/link"; 
import { useCore } from "@/core-nextv3/core/core";

export const MobileMenu = ({ menu }: any) => 
{
    const { isOpenMenu, setIsOpenMenu } = useCore();
   
    return (
        isOpenMenu && <Drawer
            open={isOpenMenu}
            onClose={setIsOpenMenu}
            size="100%"
            direction="left"
            duration={300}
            enableOverlay={false}
            className={styles.container + " menuMobile"}
        >
            <div className={styles.content} onClick={(e) => e.stopPropagation()}>

                <div className={styles.mobileMenu}>
                    <div className={styles.mobileItem} >
                        <p onClick={() => 
                        {
                            setIsOpenMenu(false) 
                        }}>
                            <Link href="/" prefetch={true}>
                                Home
                            </Link>
                        </p>
                    </div>
                    <div className={styles.mobileItem}>
                        <div className={styles.menuTypeMobile}>
                            {menu?.map((item: any, index:any) => (
                                item.status && <MenuTypeMobile
                                    key={index}
                                    className={styles.menuTypeItemMobile}
                                    item={item}
                                />
                            ))}
                        </div>
                    </div>

                    <div className={styles.otherPages + " otherPages"}>
                        {MENU_FOOTER_STATIC && (MENU_FOOTER_STATIC.map((item: any, index:any) =>
                            (
                                <AccordionSubMenuItem
                                    key={index}
                                    title={item?.label}
                                >
                                    {item.children.map((item2: any, index:any) => (
                                        <div
                                            key={index}
                                            className={styles.answer}
                                        >
                                            <p
                                                onClick={() => 
                                                {
                                                    setIsOpenMenu(false) 
                                                }}
                                                className={styles.answerText}
                                            >
                                                <Link href={item2.url || ""} prefetch={true}>
                                                    {item2.label}
                                                </Link>                        
                                            </p>
                                        </div>
                                    ))}
                                </AccordionSubMenuItem>
                            )))}
                    </div>

                </div>
            </div>
        </Drawer>
    );
};
