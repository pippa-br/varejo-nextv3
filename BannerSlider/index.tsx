"use client"

import styles from "./styles.module.scss";
import { Options } from "@splidejs/splide";
import { Splide } from "@splidejs/react-splide";
//import { Splide } from '@splidejs/react-splide';
import { THEME_SETTING } from "../../setting/setting";
import { ImageSet } from "../../component-nextv3/ImageSet";
//import { Desktop, Mobile } from "../../core-nextv3/util/useResponsive";
import { VideosSet } from "../../component-nextv3/VideoSet/indx";
//import { useEffect, useState } from "react";
import Link from "next/link";

export const BannerSlider = ({ images }: any) => 
{
    //const [loadingSlide, setLoadingSlide] = useState(false);
    const widthDesktop = THEME_SETTING.widthBannerMain || 1920;
    const heighDesktop = THEME_SETTING.heightBannerMain || 813;

    const widthMobile = THEME_SETTING.widthBannerMainMobile || 1000;
    const heighMobile = THEME_SETTING.heightBannerMainMobile || 1500;

    const mainOptions : Options = {
        role         : "banner",
        type         : "loop",
        rewind       : true,
        perPage      : 1,
        perMove      : 1,
        pagination   : false,
        slideFocus   : true,
        lazyLoad     : "nearby",
        preloadPages : 1,
        start        : 0,
        arrows       : true,
        autoplay     : true,
        interval     : 3000,
    }

    // useEffect(() => 
    // {
    //     const onLoad = () => {
    //         setLoadingSlide(true);
    //     };

    //     if (document.readyState === 'complete') {
    //         setLoadingSlide(true);
    //     } else {
    //         window.addEventListener('load', onLoad);
    //     }

    //     // Fallback: Garante que o estado seja atualizado após um pequeno atraso
    //     const timeout = setTimeout(() => {
    //         setLoadingSlide(true);
    //     }, 3000); // 3 segundos de espera

    //     return () => {
    //         window.removeEventListener('load', onLoad);
    //         clearTimeout(timeout);
    //     };
    // }, []);

    // const getStaticImage = (images:any) => 
    // {
    //     for (const item of images)
    //     {
    //         if (item.status)
    //         {
    //             return [ item ];
    //         }
    //     }

    //     return [];
    // }

    return (
        <>
            {/* <Head>
                {!loadingSlide && getStaticImage(images)?.map((item:any, index:any) => (
                    <>
                        <link rel="preload" href={cloudflareLoader({src:item?.desktop._url, width : widthDesktop})} as="image" />
                        <link rel="preload" href={cloudflareLoader({src:item?.mobile._url, width : widthDesktop})} as="image" />
                    </>
                ))}
            </Head> */}

            <div className={styles.bannerSlider}>
                <div className={styles.content}>

                    {<Splide
                        options={mainOptions}
                        className={styles.slider}
                    >
                        {images && images?.length > 0 && images?.map((item:any, index:any) => (
                            item.status && <div key={index} className="splide__slide">
                                <div className={styles.imageDesktop}>
                                    {/* <Desktop> */}                                
                                    <Link href={item?.url || "/"} prefetch={true} aria-label="Banner">
                                        {item?.type?.value == "video" ? 
                                            <VideosSet
                                            // aspectRatio={THEME_SETTING.bannerAspectRatio || 1920/813} 
                                                video={item?.desktopVideo}
                                                width={widthDesktop} 
                                                height={heighDesktop} /> 
                                            :
                                            <ImageSet 
                                            // aspectRatio={THEME_SETTING.bannerAspectRatio || 1920/813} 
                                            //   domainFrom={THEME_SETTING.domainFrom} 
                                            //   domainTo={THEME_SETTING.domainTo}
                                                image={item?.desktop} 
                                                width={widthDesktop} 
                                                height={heighDesktop} 
                                                priority={index === 0}/>}
                                    </Link>                                
                                    {/* </Desktop> */}
                                </div>
                                <div className={styles.imageMobile}>
                                    {/* <Mobile> */}
                                    <Link href={item?.url || "/"} prefetch={true} aria-label="Banner"> 
                                        {item?.type?.value == "video" ? 
                                            <VideosSet 
                                                // aspectRatio={THEME_SETTING.bannerMobileAspectRatio || 1000/1500}
                                                video={item?.mobileVideo}
                                                width={widthMobile} 
                                                height={heighMobile}/> 
                                            : 
                                            <ImageSet 
                                                //aspectRatio={THEME_SETTING.bannerMobileAspectRatio || 1000/1500} 
                                                // domainFrom={THEME_SETTING.domainFrom} 
                                                // domainTo={THEME_SETTING.domainTo}
                                                image={item?.mobile} 
                                                width={widthMobile} 
                                                height={heighMobile} 
                                                priority={index === 0}/> 
                                        }
                                    </Link>
                                    {/* </Mobile> */}
                                </div>
                            </div>
                        ))}
                    </Splide>}
                </div>
            </div>
        </>
    );
};
