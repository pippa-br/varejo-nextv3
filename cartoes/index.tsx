import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import { useState } from "react";
import { toast } from "react-hot-toast";
import { AddCreditCardModal } from "../AddCreditCardModal";
import { PageTitle } from "../../component-nextv3/PageTitle";
import Image from "next/image";
import { cloudflareLoader } from "@/core-nextv3/util/util";
import Link from "next/link";
declare const window: any;

function CreditCardsPage({ userCreditCards, userData }: any) 
{
    const [ openModal, setOpenModal ] = useState(false);
    const router = useRouter();

    async function handleLogout() 
    {
    //await authPlugin.logoutFront();
        toast.success("Logout feito com sucesso!");
        window.location.reload();
    }

    const deleteCreditCard = async (creditCardReference: any) => 
    {
        creditCardReference;
        
        toast.success("Cartão deletado com sucesso!");
        // const newData = {
        //     document : {
        //         referencePath : creditCardReference,
        //     },
        // };
        //await creditCardsPlugin.cancelDocumentFront(newData);
        router.reload();
    };

    return (
        <>
            <div className={styles.creditCardsPage}>
                <div className={styles.content}>
                    <PageTitle name="Endereços" />
                    <div className={styles.perfilData}>
                        <div className={styles.perfilMenu}>
                            <Link href="/perfil/" prefetch={true}>Meus Pedidos</Link>
                            <Link href="/perfil/detalhes-da-conta/" prefetch={true}>Detalhes da conta</Link>
                            <Link href="/perfil/enderecos/" prefetch={true}>Endereços</Link>
                            <Link href="/perfil/cartoes/" prefetch={true} className={styles.active}>
                Cartões
                            </Link>
                            <a onClick={() => handleLogout()}>Logout</a>
                        </div>
                        <div className={styles.perfilContent}>
                            {userCreditCards?.length >= 1 ? (
                                <div className={styles.addressGrid}>
                                    {userCreditCards.map((creditcard: any) => (
                                        <div
                                            key={creditcard.referencePath}
                                            className={styles.addressItem}
                                        >
                                            <div className={styles.addressItemActions}>
                                                <Image
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/BsTrash.svg"
                                                    loader={cloudflareLoader}
                                                    alt=""
                                                    onClick={() =>
                                                        deleteCreditCard(creditcard.referencePath)
                                                    }
                                                    title="Deletar cartão"
                                                />
                                            </div>

                                            <span>{creditcard.cardnumber}</span>
                                            <span>{creditcard.owner}</span>
                                            <span>{creditcard.expirydate}</span>
                                        </div>
                                    ))}
                                </div>
                            ) : null}
                            <span className={styles.noCreditCards}>
                Você não possui cartões cadastrados.
                            </span>
                            <button
                                type="button"
                                className={styles.addAddressButton}
                                onClick={() => setOpenModal(true)}
                            >
                Cadastrar cartão
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            {openModal && (
                <AddCreditCardModal userData={userData} setOpenModal={setOpenModal} />
            )}
        </>
    );
}

export default CreditCardsPage;
