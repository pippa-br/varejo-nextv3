import { FavoriteDisplay } from "./favorite.display";

const GenerateMetadata = async () =>
{
    return {
        title : "Favoritos"
    }
}

const FavoritePage = () =>
{
    return (
        <FavoriteDisplay/>            
    );
}

export { 
    FavoritePage,
    GenerateMetadata
};