"use client";

import styles from "./styles.module.scss";
import { useEffect, useState } from "react";
import { ProductItem } from "../ProductItem";
import { AUTH_SETTING, CART_SETTING, FAVORITE_SETTING, THEME_SETTING, } from "../../setting/setting";
import { collectionProductVariantFavorite } from "../../core-nextv3/favorite/favorite.api";
import { PageTitle } from "../../component-nextv3/PageTitle";
import { LoadingPage } from "../../component-nextv3/LoadingPage";
import { useCore } from "../../core-nextv3/core/core";
import { useProtectedAuth } from "@/core-nextv3/auth/user.auth.api";
import Link from "next/link";
import { logoutAuth } from "@/core-nextv3/auth/auth.api";
import { mergeAllCart } from "@/core-nextv3/cart/cart.api";
//import { useRouter } from "next/navigation";
import toast from "react-hot-toast";
import { useTranslations } from "next-intl";
declare const window: any;

const FavoriteDisplay = () =>
{
    //const router                    = useRouter()
    const { user, cart }            = useCore();  
    const [ favorites, setFavorites ] = useState<any>([]);
    const t = useTranslations();

    useEffect(() => 
    {
        const getDocumments = async () =>
        {
            if (user)
            {
                const result = await collectionProductVariantFavorite(FAVORITE_SETTING.merge({
                    client : {
                        referencePath : user.referencePath
                    }
                }));

                setFavorites(result.collection);
            }
        }

        getDocumments();

    }, [ user ])

    const handleLogout = async () => 
    {
        await logoutAuth(AUTH_SETTING)

        // MERGE CART SESSION
        await mergeAllCart(
            CART_SETTING.merge({
                document : {
                    referencePath : cart?.referencePath,
                },
            })
        )

        toast.success(`${t("Logout feito com sucesso")}!`)
        window.location.reload();
    }

    const loadProtectedAuth = useProtectedAuth(AUTH_SETTING, "", "/login/");

    if (!loadProtectedAuth) 
    {
        return (<LoadingPage />)
    }  

    return (
        <>
            <div className={styles.wishlistPage + " page"}>
                <div className={styles.content}>
                    <PageTitle name="Lista de desejos" />

                    <div className={styles.perfilData}>

                        <div className={styles.perfilMenu}>
                            <Link href="/perfil/" prefetch={true}>
                                {t("Meus Pedidos")}
                            </Link>
                            {!THEME_SETTING.disabledExpress && (
                                <Link href="/pedidos-express/" prefetch={true}>
                                    {t("Pedidos Express")}
                                </Link>
                            )}
                            <Link href="/perfil/detalhes-da-conta/" prefetch={true}>
                                {t("Detalhes da conta")}
                            </Link>
                            {!THEME_SETTING?.disabledFavorite &&
                      <Link href="/lista-de-desejos/" prefetch={true} className={styles.active}>
                          {t("Lista de Desejos")}
                      </Link>
                            }
                            <a
                                onClick={() => 
                                {
                                    handleLogout()
                                }}
                            >
                                {t("Sair")}
                            </a>
                        </div>

                        {favorites && favorites.length > 0 ? (
                            <div className={styles.favoriteProducts}>
                                {favorites.map((product: any) => (
                                    <ProductItem
                                        key={product.id}
                                        product={product}
                                        disabledQuery={true}
                                    />
                                ))}
                            </div>
                        ) : (
                            <p className={styles.noFavorite}>Não há produtos em seu favorito.</p>
                        )}
                  
                    </div>

                </div>
            </div>
        </>      
    );
}

// const getStaticProps : GetStaticProps = () => withHeader(async (pros:any) => 
// {
//     return {
//         revalidate : THEME_SETTING.revalidate,
//         props: {
//            seo : pros.seo.merge({ title : 'Favoritos'})
//         },
//     };
// });

export { FavoriteDisplay };