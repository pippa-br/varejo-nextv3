import styles from "./styles.module.scss";
import { usePathname } from "next/navigation";
import { ProductDetails } from "../ProductDetails";
import React, { useEffect, useState } from "react";
import IUser from "../../core-nextv3/interface/i.user";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { getProduct } from "../../core-nextv3/product/product.api";
import { FAVORITE_SETTING } from "../../setting/setting";
import { useOnEvent } from "../../core-nextv3/util/use.event";
import { useGetFavorite } from "@/core-nextv3/favorite/favorite.use";
import { useProductClickAnalytics } from "@/core-nextv3/analytics/analytics.use";

export const ModalProduct = ({ setOpenModalProduct, product }: any) => 
{
    const pathname = usePathname();
    const [ user, setUser ] = useState<IUser>();
    const [ hasFavorite, setHasFavorite ] = useState<any>(false);

    // console.log(product);

    useOnEvent("changeUser", (event: any) => 
    {
        setUser(event);
    });

    // FAVORITE
    useGetFavorite(FAVORITE_SETTING.merge({
        document : {
            referencePath : product?.referencePath,
        }
    }),
    (status: boolean) => 
    {
        setHasFavorite(status);
    });

    // PRODUCTS ANALYTICS
    useProductClickAnalytics(product);

    useEffect(() => 
    {
        tagManager4.addProduct(pathname, product, user);
        getProduct(product)

    }, [ pathname ]);

    return (
        <>
            <div className={styles.modalProduct} onClick={() => setOpenModalProduct(false)}>
                <div className={styles.content} onClick={(e) => e.stopPropagation()}>
                    <div className={styles.top}>
                        <span onClick={() => setOpenModalProduct(false)}>X</span>
                    </div>

                    <ProductDetails
                        user={user}
                        hasFavorite={hasFavorite}
                        setHasFavorite={setHasFavorite}
                        product={product}
                    />

                    {/* {product?.relatedProducts?.length > 0 &&
                        <>
                            <p className={styles.title}>Produtos Relacionados</p>
                            <div className={styles.relatedProducts}>
                                {product?.relatedProducts?.map((product: any) => (
                                    (product.slug && <ProductItem key={product.id} product={product} disabledQuery={true} />)
                                ))}
                            </div>
                        </>
                    } */}
                </div>
            </div>
        </>
    );
}