import { CheckoutFormTwo } from "../../component-nextv3/CheckoutFormTwo";
import { Suspense } from "react";
import { calls } from "@/core-nextv3/util/call.api";
import { ACCOUNT_SETTING, GATEWAY_SETTING } from "@/setting/setting";
import { getAccount } from "@/core-nextv3/account/account.api";
import { getDocument } from "@/core-nextv3/document/document.api";

const getCheckoutPageServer = async () => 
{
    const [ accountResult, installmentRuleResult ] = await calls(
        getAccount(ACCOUNT_SETTING.merge({ cache : "force-cache" })), 
        getDocument(GATEWAY_SETTING.merge({ cache : "force-cache" })
        ));

    return {
        accountData         : accountResult?.data || {},
        installmentRuleData : installmentRuleResult?.data?.installmentRule || {}        
    };
}

const GenerateMetadata = async () =>
{
    return {
        title : "Checkout"
    }
}

const CheckoutPageTwo = async () =>
{
    const { accountData, installmentRuleData } = await getCheckoutPageServer();

    return (
        <Suspense>
            <CheckoutFormTwo
                account={accountData}
                redirectUrl="/pedido"
                installmentRuleData={installmentRuleData}
            />			            
        </Suspense>
    );
}

export { 
    CheckoutPageTwo,
    GenerateMetadata
}