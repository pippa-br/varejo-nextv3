import styles from "./styles.module.scss";
import { useState } from "react";
import { GetStaticProps } from "next";
import { ProductDetails } from "../ProductDetails";
import { ProductItem } from "../ProductItem";
import { COLOR_TABLE_SETTING, COMMET_SETTING, FAVORITE_SETTING, FULL_PRODUCT_SETTING, SLUG_PRODUCT_SETTING, THEME_SETTING, TROCA_PAGE_SETTING } from "../../setting/setting";
import withHeader from "../../utils/withHeader";
import { getProduct } from "../../core-nextv3/product/product.api";
import { calls } from "../../core-nextv3/util/call.api";
import { collectionDocument, getDocument } from "../../core-nextv3/document/document.api";
import { useCore } from "../../core-nextv3/core/core";
import { CommentBox } from "../../component-nextv3/CommentBox";
import { useProductClickAnalytics } from "@/core-nextv3/analytics/analytics.use";
import { useGetFavorite } from "@/core-nextv3/favorite/favorite.use";

const ProductPage = ({ product, colorTable, page, comments }: any) =>
{
    //console.error('aaa', product);
    // console.log('xxx', comments);
	
    const { user } 						            = useCore();
    const [ hasFavorite, setHasFavorite ]             = useState<any>(false);

    // FAVORITE
    useGetFavorite(FAVORITE_SETTING.merge({
        document : {
            referencePath : product?.referencePath,
        }
    }),
    (status:boolean) => 
    {
        setHasFavorite(status);
    });

    // PRODUCTS ANALYTICS
    useProductClickAnalytics(product);

    return (
        <>			
            <div className={styles.productPage}>
                <div className={styles.content}>

                    <ProductDetails
                        user={user}
                        page={page}
                        product={product}
                        colorTable={colorTable}
                        hasFavorite={hasFavorite}
                        setHasFavorite={setHasFavorite}						
                    />

                    <CommentBox
                        product={product}
                        comments={comments}
                    />

                    {product?.relatedProducts?.length > 0 && <>
                        <p className={styles.title}>Produtos Relacionados </p>
                        <div className={styles.relatedProducts}>
                            {product?.relatedProducts?.map((product: any) => (
                                (product.published == true && product.slug && <ProductItem key={product.id} product={product} disabledQuery={true}/>)
                            ))}
                        </div>
                    </>}

                </div>
            </div>
        </>
    );
}

const getStaticProps : GetStaticProps = ({ params }) => withHeader(async (props:any) => 
{
    const [ product, colorTable ] = await calls(
        getProduct(FULL_PRODUCT_SETTING.merge({
            slug : params?.slug,
        })),
        getDocument(COLOR_TABLE_SETTING),
    );

    if (!product.status)
    {
        return {
            notFound   : !product.serverError,
            revalidate : true,
        };
    }

    if (!product.data.published)
    {
        return {
            notFound   : true,
            revalidate : THEME_SETTING.revalidate
        }
    }

    const pageResult = await getDocument(TROCA_PAGE_SETTING)

    const commentsResult = await collectionDocument(COMMET_SETTING.merge(
        {
            where : [ {
                field    : "product",
                operator : "==",
                value    : {
                    referencePath : product.data.referencePath
                }
            },
            {
                field    : "status",
                operator : "==",
                value    : true
            } ]
        }));	

    return {
        revalidate : THEME_SETTING.revalidate,
        props      : {
            seo        : props.seo.mergeProduct(props.account, product.data),
            product    : product.data,
            colorTable : colorTable?.data || {},
            page       : pageResult.data,
            comments   : commentsResult.collection || [],
        },
    };
});

const getStaticPaths = async () =>
{
    const categories = await collectionDocument(SLUG_PRODUCT_SETTING.merge({
        perPage : 1000,
        where   : [ {
            field  	 : "published",
            operator : "==",
            value  	 : true,
        } ]
    }));
  
    const paths = categories.collection.map((item:any) => 
        ({
	  	params : { slug : item.slug },
        }));
  
    return { paths, fallback : "blocking" }
}

export { getStaticProps as GetStaticProps, getStaticPaths as GetStaticPaths, ProductPage }