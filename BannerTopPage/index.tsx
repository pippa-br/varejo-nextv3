import styles from "./styles.module.scss";
import { THEME_SETTING } from "../../setting/setting";
import { ImageSet } from "@/component-nextv3/ImageSet";

const BannerTopPage = ({ page, category }:any) =>
{
    const getImage = () => 
    {        
        if (page)
        {
            if (category && page.categories) 
            {				
                for (const item of page.categories)
                {
                    if (item.status && item.category && category.id == item.category.id)
                    { 
                        return (
                            <>
                                <div className={styles.desktop}>
                                    <ImageSet
                                        image={item?.categoryDesktop}
                                        width={THEME_SETTING.widthBannerCategory || 1920}
                                        height={THEME_SETTING.heightBannerCategory || 400}
                                        alt=""
                                    />
                                </div>
                                {!THEME_SETTING.disabledFotoCategoryMobile &&
                                    <div className={styles.mobile}>
                                        <ImageSet
                                            image={item?.categoryMobile}
                                            width={THEME_SETTING.widthBannerCategoryMobile || 1000}
                                            height={THEME_SETTING.heightBannerCategoryMobile || 1500}
                                            alt=""
                                        />
                                    </div>
                                }
                            </>
                        )
                    }
                }
            }        
        }
    }

    return (
        <>
            <div className={styles.bannerTopPage}>
                {getImage()}
            </div>
        </>      
    );
}

export { BannerTopPage };