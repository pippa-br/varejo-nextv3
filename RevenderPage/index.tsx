import styles from "./styles.module.scss";
import { useState } from "react";
import { ProductItem } from "../ProductItem";
import { GetStaticProps } from "next";
import { AUTH_SETTING, FAVORITE_SETTING } from "../../setting/setting";
import { PageTitle } from "../../component-nextv3/PageTitle";
import withHeader from "../../utils/withHeader";
import { LoadingPage } from "../../component-nextv3/LoadingPage";
import { useProtectedAuth } from "@/core-nextv3/auth/user.auth.api";
import { useCollectionProductVariantFavorite } from "@/core-nextv3/favorite/favorite.use";

const RevenderPage = () =>
{
    const [ favorites, setFavorites ] = useState<any>();
  
    useCollectionProductVariantFavorite(FAVORITE_SETTING, (collection:any) => 
    {
        setFavorites(collection); 
    });

    const loadProtectedAuth = useProtectedAuth(AUTH_SETTING, "", "/login");

    if (!loadProtectedAuth) 
    {
        return (<LoadingPage />)
    }  

    return (
        <>
            <div className={styles.wishlistPage}>
                <div className={styles.content}>
                    <PageTitle name="Produtos para Revendas" />

                    {favorites && favorites.length >= 1 ? (
                        <div className={styles.favoriteProducts}>
                            {favorites.map((product: any) => (
                                <ProductItem
                                    key={product.id}
                                    product={product}
                                    disabledQuery={true}
                                />
                            ))}
                        </div>
                    ) : (
                        <p className={styles.noFavorite}>Não há produtos em seu favorito.</p>
                    )}
                </div>
            </div>
        </>      
    );
}

const getStaticProps : GetStaticProps = () => withHeader(async (pros:any) => 
{
    return {
        props : {
            seo : pros.seo.merge({ title : "Revendas" })
        },
    };
});

export { RevenderPage, getStaticProps as GetStaticProps };