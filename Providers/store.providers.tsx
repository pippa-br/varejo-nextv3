"use client";

import React, { useContext } from "react"
import { createContext } from "react";

type StoreContextType = {
    colorTableData:any,
    installmentRuleData:any,
    colorVariantsData: any, 
    sizeVariantsData:any
};

const StoreContext = createContext<StoreContextType | undefined>(undefined);

const useStoreContext = () => 
{
    const context = useContext(StoreContext);

    if (!context) 
    {
        throw new Error("useAppContext must be used within an AppProvider");
    }

    return context;
};

const StoreProvider = ({ 
    colorTableData,
    installmentRuleData,
    colorVariantsData, 
    sizeVariantsData,
    children
}: any) => 
{
    return (<>
        <StoreContext.Provider value={{ colorVariantsData, sizeVariantsData, colorTableData, installmentRuleData }}>
            {children}
        </StoreContext.Provider>		
    </>);
}

export { 
    StoreProvider,
    useStoreContext
}