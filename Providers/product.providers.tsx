"use client";

import React, { useContext } from "react"
import { createContext } from "react";

type ProductContextType = {
    exchangeData: any, 
    featuresProductData:any,
    measurementTableData:any
};

const ProductContext = createContext<ProductContextType | undefined>(undefined);

const useProductContext = () => 
{
    const context = useContext(ProductContext);

    if (!context) 
    {
        throw new Error("useAppContext must be used within an AppProvider");
    }

    return context;
};

const ProductProvider = ({ 
    exchangeData, 
    featuresProductData,
    measurementTableData,
    children
}: any) => 
{    
    return (<>
        <ProductContext.Provider value={{ exchangeData, featuresProductData, measurementTableData }}>
            {children}
        </ProductContext.Provider>		
    </>);
}

export { 
    ProductProvider,
    useProductContext
}