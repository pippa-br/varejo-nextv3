"use client";

import * as CookieConsent from "vanilla-cookieconsent";
import { tagManager4 } from "@/core-nextv3/util/TagManager4"
import { disableReactDevTools } from "@/core-nextv3/util/disableReactDevTools"
import React, { useContext, useEffect } from "react"
import { THEME_SETTING } from "@/setting/setting";
import { AppProgressBar } from "next-nprogress-bar";
import { ChakraProvider, defaultSystem } from "@chakra-ui/react";
import { Toaster } from "react-hot-toast";
import { NextIntlClientProvider } from "next-intl";
import { usePathname } from "next/navigation";
import { WhatsAppIcon } from "@/component-nextv3/WhatsAppIcon";
import { createContext } from "react";
import { Header } from "../Header";
import { Footer } from "../Footer";
import { ProductModalHome } from "../ProductModallHome";

//declare const window: any;
declare const document: any;

type AppContextType = {
    accountData: any;
    menuData:any;
    topHeaderData:any;
    popupData:any;
    giftData:any;
};

const AppContext = createContext<AppContextType | undefined>(undefined);

const useAppContext = () => 
{
    const context = useContext(AppContext);

    if (!context) 
    {
        throw new Error("useAppContext must be used within an AppProvider");
    }

    return context;
};

const AppProvider = ({ accountData, children, topHeaderData, menuData, popupData, giftData }: any) => 
{
    const pathname = usePathname();

    const onlyPathsFull : boolean = [ "/checkout/" ].includes(
        pathname
    );
		
    useEffect(() => 
    {
        if (accountData && accountData.gtm) 
        {
            tagManager4.initialize(accountData.gtm);
            //tagManager4.pageView(pathname, globalData?.seo?.title, 'b2b', 'ecommerce', window.orientation > -1 ? 'mobile' : 'desktop', 'Pippa', user);
        }

        //pageViewsAnalytics(globalData.seo);

        CookieConsent.run({

            categories : {
                necessary : {
                    enabled  : true,  // this category is enabled by default
                    readOnly : true  // this category cannot be disabled
                },
            },

            guiOptions : {
                consentModal : {
                    layout             : "bar inline",
        						position           : "bottom center",
                    flipButtons        : false,
    								equalWeightButtons : true
                }
            },						

            language : {
                default      : "en",
                translations : {
                    en : {
                        consentModal : {
                            title              : "",
                            description        : THEME_SETTING.cookieConsent,
                            acceptAllBtn       : "Aceitar",
                            acceptNecessaryBtn : "Rejeitar",
                            showPreferencesBtn : ""
                        },
                        preferencesModal : {
                            title              : "Manage cookie preferences",
                            acceptAllBtn       : "Accept all",
                            acceptNecessaryBtn : "Reject all",
                            savePreferencesBtn : "Accept current selection",
                            closeIconLabel     : "Close modal",
                            sections           : [
                                {
                                    title       : "Somebody said ... cookies?",
                                    description : "I want one!"
                                },																
                            ]
                        }
                    }
                }
            }
        });

        disableReactDevTools();

    }, []);

    // useUniqueUserAnalytics()
    // usePeriodsDaysAnalytics()
    // useWeatherAnalytics()
    // useDeviceAnalytics()
    // useReferrerAnalytics()
    // useDocumentSocket(
    // 	'default/admin/documents/uPSh5s2Ub0U1EQAKXQ0i',
    // 	(data: any) => {
    // 		//console.error('xxx', data)
    // 	}
    // )

    useEffect(() => 
    {
        const script = document.createElement("script");
        script.src = "https://www.google.com/recaptcha/enterprise.js?render=" + process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY;
        script.async = true;
        document.head.appendChild(script);
    }, []);

    return (<>
        <AppProgressBar
            height="4px"
            color="var(--theme-color);"
            options={{ showSpinner : false }}
            shallowRouting 
        />				
        <NextIntlClientProvider
            locale={"pt-BR"}
            messages={{}}
            onError={(error) => 
            {
                if (error.code === "MISSING_MESSAGE") 
                {
                    //
                }
            }}>
            <ChakraProvider value={defaultSystem}>
                <div className={(onlyPathsFull ? "fullPage" : "") + " " + (topHeaderData?.published ? "hasTopHeader" : "noTopHeader")}>
                    <AppContext.Provider value={{ accountData, menuData, topHeaderData, giftData, popupData }}>
                        
                        <Header accountData={accountData} topHeaderData={topHeaderData} menuData={menuData} giftData={giftData}/>

                        {children}

                        <Footer accountData={accountData}/>

                        {popupData && popupData?.published && <ProductModalHome popupData={popupData} />}

                    </AppContext.Provider>
                </div>							
                <Toaster
                    position="top-center"
                    reverseOrder={true}
                    containerStyle={{
                        fontSize : 16,
                        height   : "85px"
                    }}
                    toastOptions={{
                        duration : 2500,
                    }}
                />

                {!THEME_SETTING.disabledWhatsapp && <WhatsAppIcon account={accountData} />}
            </ChakraProvider>
        </NextIntlClientProvider>				
    </>);
}

export { 
    AppProvider,
    useAppContext
}