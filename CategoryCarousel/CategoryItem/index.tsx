import { THEME_SETTING } from "@/setting/setting";
import styles from "./styles.module.scss";
import { ImageSet } from "@/component-nextv3/ImageSet";
import Link from "next/link";

export const CategoryItem = ({ category, banners }: any) => 
{
    const getIamge = () =>
    {
        if (banners)
        {
            if (category && banners)
            {
                for (const item of banners)
                {
                    if (item.status && item.category && category.id == item.category.id)
                    {
                        return (
                            <Link href={`/categoria/${category?.slug}/`} prefetch={true}>
                                <ImageSet
                                    alt=""
                                    className={styles.mobile}
                                    image={item?.categoryMobile}
                                    width={THEME_SETTING.widthBannerCategoryMobile}
                                    height={THEME_SETTING.heightBannerCategoryMobile}
                                />
                                <p className={styles.name}>{category.name}</p>
                            </Link>
                        )
                    }
                }
            }
        }
    }

    return (
        <div className={styles.slideItem + " splide__slide"}>
            {banners ?
                <>
                    {getIamge()}
                </>
                :
                <Link href={`/categoria/${category?.slug}/`} prefetch={true}>
                    <ImageSet image={category?.mobile}
                        width={THEME_SETTING.widthBannerCategoryMobile}
                        height={THEME_SETTING.heightBannerCategoryMobile}/>
                    <p className={styles.name}>{category.name}</p>
                </Link>
            }
        </div>
    );
}